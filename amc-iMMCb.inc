! ----- f90 -----

! ----- this is a template for model code simulated with aMC -------------------

! - iMMCb parameters -----------------------------------------------------------
!define iMMCb_no_Cl_sink
!define iMMCb_no_hv_sink
!define iMMCb_no_soil_sink
!define iMMCb_probe_air_exchange_rates
!define iMMCb_probe_unif_reactivities
!define iMMCb_triangular_emissions
!define iMMCb_do_90s
!define iMMCb_larger_tau_range
#define iMMCb_true_unif_glob_emis
#define iMMCb_true_unif_glob_tau
!define iMMCb_one_tau_and_KIE
!define iMMCb_Fischer_sources
!define iMMCb_hit_imr_only

! ----- global imports ---------------------------------------------------------
#ifdef    aMC__uses
#endif /* aMC__uses */

! ----- global definitions -----------------------------------------------------
#ifdef    aMC__defs
#endif /* aMC__defs */

! ----- custom routines --------------------------------------------------------
#ifdef    aMC__contains
#endif /* aMC__contains */

! ----- custom [de]init --------------------------------------------------------
#ifdef    aMC__init__defs
#endif /* aMC__init__defs */
#ifdef    aMC__init__code
#endif /* aMC__init__code */

#ifdef    aMC__deinit__defs
#endif /* aMC__deinit__defs */
#ifdef    aMC__deinit__code
#endif /* aMC__deinit__code */

! ----- MC parameter setup -----------------------------------------------------
#ifdef    aMC__parameter_setup__defs
#endif /* aMC__parameter_setup__defs */
#ifdef    aMC__parameter_setup__code
#endif /* aMC__parameter_setup__code */

! ----- new initial state ------------------------------------------------------
#ifdef    aMC__new_x0__defs
#endif /* aMC__new_x0__defs */
#ifdef    aMC__new_x0__code
#endif /* aMC__new_x0__code */

! ----- prepare output ---------------------------------------------------------
#ifdef    aMC__output_prepare__defs
#endif /* aMC__output_prepare__defs */
#ifdef    aMC__output_prepare__code
#endif /* aMC__output_prepare__code */

! ----- find matching cases ----------------------------------------------------
#ifdef    aMC__match__defs
#endif /* aMC__match__defs */
#ifdef    aMC__match__cases__code
! inside the loop over cases: case = aMC_match_case_from, aMC_match_case_upto
#endif /* aMC__match__cases__code */

! ----- aMC_physc --------------------------------------------------------------
#ifdef    aMC__physc__defs
#endif /* aMC__physc__defs */
#ifdef    aMC__physc__code
#endif /* aMC__physc__code */
! ----- aMC_physc:check_req_hits -----
! inside the loop over cases: cn = aMC_match_case_from, aMC_match_case_upto
#ifdef    aMC__physc__check_req_hits__code
#endif /* aMC__physc__check_req_hits__code */

! ----- info output ------------------------------------------------------------
#ifdef    aMC__info__code
    _info_ '| '//aMC_setup//' options:'
#ifdef iMMCb_one_spheric_tau_and_KIE
    _info_ '| - one sink+KIE probed per sphere'
#else
#ifndef iMMCb_no_Cl_sink
    _info_ '| - Cl sink'
#endif
#ifndef iMMCb_no_hv_sink
    _info_ '| - hv sink'
#endif
#ifndef iMMCb_no_soil_sink
    _info_ '| - soil sink'
#endif
#endif /* ! iMMCb_one_spheric_tau_and_KIE */

#ifdef iMMCb_do_90s
    _info_ '| - simulating 90s conditions'
#endif
#ifdef iMMCb_probe_air_exchange_rates
    _info_ '| - probing air exchange rates'
#endif
#ifdef iMMCb_probe_unif_reactivities
    _info_ '| - probing uniform reactivities instead of lifetimes'
#endif
#ifdef iMMCb_triangular_emissions
    _info_ '| - triangular category total emission distr.'
#endif
#ifdef iMMCb_true_unif_glob_emis
    _info_ '| - true uniform global emission distr.'
#endif
#ifdef iMMCb_true_unif_spheric_tau
    _info_ '| - true uniform trop/stratospheric lifetimes'
#endif
#ifdef iMMCb_larger_tau_range
    _info_ '| - iMMCb_larger_tau_range'
#endif
#endif /* aMC__info__code */

! --- XXX ---
#ifdef    aMC__XXX__defs
#endif /* aMC__XXX__defs */
#ifdef    aMC__XXX__code
#endif /* aMC__XXX__code */
