export SHELL=/bin/sh

### build options ############################################################
# define additional options here
  ADDEFS = TAG_OPT_SKIP_XTRABOXPPROC #E4CHEM
# select either ASCII or NETCDF output (may affect CAABA)
 #OUTPUT = ASCII
  OUTPUT = NETCDF NETCDF4
# installation directory
  INSTALLDIR = .

### sources & dependencies ###################################################
# sources (default: standalone master, MESSy/CAABA distros detected)
  DISTR = master
  ifneq ("$(wildcard ./messy/)", "")
    DISTR := messy
   #INSTALLDIR := ../../../bin
  else ifneq ("$(wildcard ./caaba/)", "")
    DISTR := caaba
  endif

# sources & paths
  VPATH = .:$(DISTR)
  SRCS = $(filter-out F%.f90,$(wildcard *.f90 $(DISTR)/*.f90))
  MODS = $(notdir $(SRCS:.f90=.mod))
  OBJS = $(notdir $(SRCS:.f90=.o))
  I90S = $(notdir $(SRCS:.f90=.i90))

# path to the executable that will be produced
  PROG = $(INSTALLDIR)/$(EXE)

# dependencies maker
# if you don't have sfmakedepend.pl, get it from http://people.arsc.edu/~kate/Perl
  DEPEND_INC = depend.mk
  F_makedepend = @$(DISTR)/sfmakedepend --file=$(DEPEND_INC) 2>$(DEPEND_INC).log

# build configurator
  BUILD_INC = build.mk
  F_cfgbuild = @./sfcfgbuild
  include $(BUILD_INC)

# check if MECCA is configured without KP4
  ifneq ("$(wildcard $(CAABA_SRC)/mecca/messy_mecca_kpp_parameters.f90)","")
    ADDEFS += NO_KP4
  endif
# additional defs for MECCA-TAG to F90DEFS0
  -include $(CAABA_SRC)/mecca/tag/f90defs0_mecca_tag.mk

# allows picking-up include files from current dir specified as <> in code
  F90FLAGS += -I.

# final adjustments
# add output/add.defines to common F90 build defines
  F90DEFS0 += $(OUTPUT) $(ADDEFS) $(AMC_DEFS)
# form F90DEFS
  DEFOPT = -D
  F90DEFS = $(addprefix $(DEFOPT), $(F90DEFS0))

### versioning ###############################################################
# system / environment
  USER := $(shell whoami)
  SYSTEM := $(shell uname)
  HOST := $(shell dnsdomainname -f)

# git-based revision
  GIT = $(shell $(W) git || echo "<none>" | tail -n 1)
ifneq ($(GIT),<none>)
  DISTRO := $(shell git config --get remote.origin.url | sed -e 's/^git@//g' -e 's/\.git//g')
  BRANCH := $(shell git branch | grep \* | cut -d ' ' -f2)
  STATE  := $(shell git --no-pager describe --tags --always --dirty --broken)@$(firstword $(shell git --no-pager show --date=iso-strict --format="%ad" --name-only))
# COMMIT := $(shell git rev-parse --verify --quiet HEAD)
  BUILDREV := $(BRANCH)@$(DISTRO) $(STATE)
else
  BUILDREV := "n/a"
endif
  DATE := built@$(shell date --iso=seconds)
  BUILDREV += $(DATE) $(USER)@$(HOST)

### targets ##################################################################
# targets as aMC setups

# setup picked up from amc.inc
 #AMC_SETUPS := $(shell grep 'aMC_setup' amc.inc | awk '{print $$3}')
# and from model include files available
 #AMC_SETUPS += $(filter-out template,$(subst amc-,,$(subst .inc,,$(wildcard amc-*.inc))))
# sorted by last-access time
  AMC_SETUPS += $(shell ls --color=never -tu amc-*.inc | sed -e 's/^amc-//g' -e 's/\.inc$$//g' | grep -v template)

define make-setup-target
.PHONY: $(1)
$(1): AMC_SETUP = $(1)
$(1): AMC_DEFS = aMC=aMC_ aMC_setup=$(1) _aMC_setup_=\"$(1)\" aMC_build_rev=\"$(subst $() $(),__,$(BUILDREV))\"
$(1):      INC = amc-$(1).inc
$(1):      EXE = amc-$(1).exe
$(1): touch-inc amc-core
endef
$(foreach ss,$(AMC_SETUPS),$(eval $(call make-setup-target,$(ss))))

ifeq ($(MAKECMDGOALS),$(filter $(MAKECMDGOALS),$(AMC_SETUPS)))
# remove aMC object files to force updates in *.inc compiled
  $(shell rm -f amc*.o)
endif

.PHONY: list-setups
list-setups:
	@echo "$(AMC_SETUPS)"

#.PHONY: all
#all: $(AMC_SETUPS)
#all: $(PROG)

.DEFAULT_GOAL := $(word 1,$(AMC_SETUPS))

touch-inc:
	@touch -c -a $(INC)    # update access time to make def. target for next build
	@echo "##### building $(DISTR)::$(AMC_SETUP) #####" | grep -E --color "|$(DISTR)|$(AMC_SETUP)"

amc-core: depend $(OBJS) cfg-build
	$(F90) $(F90FLAGS) $(OBJS) $(NETCDF_LIB) -o $(EXE)

.PHONY: mecca-relink
mecca-relink:
# cleanup/relink auto-generated MECCA sources, if present
ifeq ($(DISTR),caaba)
#	@echo "mecca-relink"
	@-rm $(DISTR)/messy_mecca_kpp*.f90 $(DISTR)/messy_mecca_tag*.f90 &>/dev/null || true
	@-if [ -d $(DISTR) ]; then ( cd $(DISTR); ln -s --target-directory=. `ls -1 caaba/messy_mecca_kpp*.f90 caaba/messy_mecca_tag*.f90 2>/dev/null` || true ) fi
endif

# update file dependencies
depend $(DEPEND_INC): $(BUILD_INC) | mecca-relink
	$(F_makedepend) $(SRCS)

### update build cfg at every build ##########################################
.PHONY: cfg-build
cfg-build $(BUILD_INC):
# update TAGS
	@-ctags --language-force=fortran --fortran-kinds=+p --fields=+iaS --extra=+q -e *.f90 *.inc 2>ctags.log
# include (or obtain) build configuration
	$(F_cfgbuild)

### force build configuration refresh ########################################
.PHONY: cfg-rebuild-force
cfg-rebuild-force:
	$(F_cfgbuild) force

### examine the configuration ################################################
.PHONY: info
info: cfg-build
	@echo "------------------------------------------------"
	@echo "SRCS       = $(SRCS)"
	@echo "------------------------------------------------"
	@echo "OBJS       = $(OBJS)"
	@echo "------------------------------------------------"
	@echo "OUTPUT     = $(OUTPUT)"
	@echo "SYSTEM     = $(SYSTEM)"
	@echo "HOST       = $(HOST)"
	@echo "COMPILER   = $(COMPILER)"
	@echo "F90        = $(F90)"
	@echo "F90FLAGS   = $(F90FLAGS)"
	@echo "F90DEFS    = $(F90DEFS)"
	@echo "NETCDF_INC = $(NETCDF_INC)"
	@echo "NETCDF_LIB = $(NETCDF_LIB)"
	@echo "------------------------------------------------"
	@echo "ADDEFS     = $(ADDEFS)"
	@echo "BUILDREV   = $(BUILDREV)"
	@echo "AMC_SETUPS = $(AMC_SETUPS)"
	@echo "------------------------------------------------"

# forcheck:
.PHONY: forcheck
forcheck:
	@forchk -rigor -cond -f95 -obs -ff -decl -ext -intr -spec -ancmpl -anprg -anref -shcom -shinc -shmod -shprg -shref -shsrc -shsub -inf -plen 25 -pwid 132 *.f90 /soft/ECHAM5/lib/netcdf90.flb > forcheck.log 2>&1
	@echo "(forcheck found: 8=errors, 4=warnings, 2=infos)"

.PHONY: clean
clean:
	@-rm -f *.mk.old $(MODS) amc_.mod $(OBJS) $(I90S) *.log *.s *.optrpt *.pyc *~

.PHONY: distclean
distclean: clean
	@-rm -f $(DEPEND_INC)* $(BUILD_INC)*
	@-rm -i *.nc *.dat || true
	@-rm -f *.exe

.PHONY: fix-cpp-eol-tokens
fix-cpp-eol-tokens:
	sed -i~ 's/\(^#endif\s*\)\/\/\(.*$$\)/\1\/\*\2 \*\//g' amc*.f90 amc*.inc

# prevent Makefile.in from remaking this Makefile
Makefile: ;

# one rule for all F90 files
%.o: %.f90
	$(GRC) $(F90) $(F90DEFS) $(F90FLAGS) $(NETCDF_INC) -c $<

### list of dependencies #####################################################
include depend.mk
