! -*- f90 -*- ! mz_sg_20160914(@tonnerre)


! === aMC parameters ===========================================================

! - general --------------------------------------------------------------------
#define aMC_purge_rnd_output      // suppress abundant info output by RND (for MESSy distros before 2.54 dev)
!define aMC_show_match_progress   // output matching progress (realisation-wise)
!define aMC_show_slave_stats      // output hit statistics on slaves
#define aMC_info_to_stderr        // output run info to stderr instead of '*.info'
!define aMC_use_LHS               // enable to use latin hypercube sampling instead of randoms

! - HPC ------------------------------------------------------------------------
#define aMC_no_sim_on_master      // master only serves run control & I/O
#define aMC_independent_harvest   // randoms are harvested independently at PEs, otherwise sent by master
#define aMC_cached_send           // successful hits are sent cached (prevents MPI clogging)
#define aMC_cached_write          // results are output cached (prevents netCDF output clogging)

! - output control -------------------------------------------------------------
#define aMC_netcdf                // enable netCDF output
#define aMC_output_non_real       // output also uncuccessful realisations
#define aMC_output_single_case    // enable output containers for single cases
#define aMC_output_common_set     // enable output containers for sets of cases
!define aMC_output_raw_parallel   // each PE outputs its results to a separate file
!define aMC_output_rq_on_master   // replace slaves' output request no. with that of @master
#define aMC_output_probe_stat     // output MC probe statistic (use to check for skewed input)
#define aMC_output_spare_extra_rq // skip PE output requests if the local amnt. of hits is greater than ~ max_req / (#PEs)
#define aMC_output_time_sort      // sort output by time (@T-axis of netCDF, e.g. for ferret)
#define aMC_output_time_gaps      // add 1ms gaps to output (@T-axis of netCDF, e.g. for ferret)

! - tuning ---------------------------------------------------------------------
!define DEBUG                     // useful runtime info
!define DEEPDEBUG                 // a lot of runtime info
!define aMC_run_single_sim        // enable to run a single try simulation
!define aMC_benchmark_run 50      // set to the number of runs to benchmark


! === simulated model parameters ===============================================
! simulated model include file name
#define  aMC__model(x) <amc-x.inc> /* */
! includes parameters from model overriding the above ones
#include aMC__model(aMC_setup)


! === important definitions ====================================================
! type aliases & symbols
#define int8    integer(kind=8)
#define real8   real(dp)
#define string  character(:),allocatable
#define strings type(t_strings),allocatable
#define _tab_   char(9)
#define _cr_    char(10)
! logging macros
#define _log_   print *,
#define _log__  write(*,'(A)',advance="no")               /* print without CR */
#define _info_  write(aMC_iounit_info,'(A)')
#define _info__ write(aMC_iounit_info,'(A)',advance="no") /* output to info without CR */
#define _pe_    '#'//num2str(i=p_pe)
! diagnostic memory (de)allocation wrappers
#if defined(__INTEL_v11) || defined(__INTEL_COMPILER)
#define actsize(var) product(shape(var))*kind(var)
#define allocate(var) allocate(var,stat=aMC_mstat);call aMC_err_check(mstat=aMC_mstat);aMC_mem=aMC_mem+actsize(var);
#define deallocate(var) aMC_mem=aMC_mem-actsize(var);if(allocated(var))deallocate(var,stat=aMC_mstat);
#endif
