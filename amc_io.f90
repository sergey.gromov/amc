! mz_sg_20170124+ | f90
!===============================================================================
!
! this unit implements I/O routines for aMC // some prototype code is from CAABA
!
!===============================================================================

#include "amc.inc"

module aMC_io

  use netcdf, only: nf90_clobber, nf90_close, nf90_create, nf90_def_dim, &
    nf90_def_var, nf90_double, nf90_enddef, nf90_float, nf90_get_att, &
    nf90_get_var, nf90_global, nf90_inq_dimid, nf90_inq_varid, nf90_inquire, &
    nf90_inquire_attribute, nf90_inquire_dimension, nf90_inquire_variable, &
    nf90_noerr, nf90_nowrite, nf90_open, nf90_put_att, nf90_put_var, &
#if defined(NETCDF4)
    nf90_netcdf4, nf90_classic_model, &
    nf90_set_fill, nf90_nofill, &
#endif
    nf90_strerror, nf90_sync, nf90_unlimited, nf90_share, nf90_write

  use messy_main_constants_mem, only: sp, dp, i4, i8, FLAGGED_BAD
  use aMC_util

  implicit none
  save

! choose a default floating-point precision:
  integer, parameter  :: FP_PREC = nf90_float
  real(sp), parameter :: FP_NODATA = real(aMC_NODATA,sp)
 !integer, parameter  :: FP_PREC = nf90_double
 !real(dp), parameter :: FP_NODATA = real(aMC_NODATA,dp)

! netCDF time axis & output handling
! forward time origin for netcdf files to monitor real time w/suff. precision
! origin gets updated by aMC_init()
  real(dp)          :: aMC_io_time_origin = 2457754.5_dp       ! 2017 ! 2457023.5_dp for 2015
  real(dp)          :: aMC_io_time_unit_fac = (60._dp*60._dp*24._dp)  ! days -> seconds
  character(len=64) :: aMC_io_time_origin_string = trim('seconds since 2017-01-01 00:00:00')

! no of opened output files
  integer           :: aMC_io_output_opened = 0

  integer :: dimids2d(3), dimids3d(4)
  integer :: londimid, latdimid, levdimid, tdimid
  integer :: lonid, latid, levid, tid
  integer :: start3d(4), cnt3d(4)
  integer :: dimids1d(1), start1d(1), cnt1d(1)
  integer :: nlon, ngl, nlev
  integer, allocatable :: varid(:)
  real :: nlondata(1), ngldata(1), nlevdata(1)

contains

! ------------------------------------------------------------------------------

  subroutine nf(status) ! turns nf90_* function into subroutine + checks status
    integer :: status
    if (status /= nf90_noerr) then
      write (*,*) 'netcdf error: ', nf90_strerror(status)
      stop
    endif
  end subroutine nf

! ------------------------------------------------------------------------------

  subroutine aMC_open_input(ncid, filename)

    implicit none
    intrinsic :: trim

    integer, intent(out) :: ncid
    character(len=*), intent(in) :: filename

    call nf(nf90_open(trim(filename), nf90_nowrite, ncid))

  end subroutine aMC_open_input

! ------------------------------------------------------------------------------

  subroutine aMC_output_open(ncid, filename, output_info, append, &
                             title, setup, set, case)

    implicit none

    integer, intent(out)                   :: ncid
    character(len=*), intent(in)           :: filename
    type(t_aMC_output_info)                :: output_info(:)  ! list of aMC outputs
    logical, optional, intent(in)          :: append          ! specifies whether the file is open for appending (in case of restart of aMC)
    character(len=*), optional, intent(in) :: title, setup, set, case
    logical                                :: p_append = .false.
    integer                                :: cmode, v
    string                                 :: s, filename_

  if (present(append)) then
    p_append = append
  else
    p_append = .false.
  endif
! netcdf interface
  filename_=trim(filename)//'.nc'

! initialise variables IDs array
  if (.not.allocated(varid)) allocate(varid(size(output_info)))

  if (.not.p_append) then

  ! define the grid size (1 box)
    nlon = 1
    ngl  = 1
    nlev = 1

  ! define the grid (pseudo, since only box)
    nlondata = (/ 0. /)
    ngldata  = (/ 0. /)
    nlevdata = (/ 0. /)

  ! open new netCDF container
    cmode = nf90_clobber
#if defined(NETCDF4)
    cmode = cmode+nf90_netcdf4+nf90_classic_model
#endif
    call nf(nf90_create(filename_, cmode, ncid))

  ! global attributes
    if (present(title)) then
      s = str_field(title,'|',1); if (s.ne.'') call nf(nf90_put_att(ncid, nf90_global, 'title', s))
      s = str_field(title,'|',2); if (s.ne.'') call nf(nf90_put_att(ncid, nf90_global, 'title_mod', s))
    else
      call nf(nf90_put_att(ncid, nf90_global, 'title', 'aMC')) !trim(modstr)))
    endif
    call nf(nf90_put_att(ncid, nf90_global, 'model',   trim(modstr)))
    call nf(nf90_put_att(ncid, nf90_global, 'version', trim(modver)))
#ifdef aMC_build_rev
    call nf(nf90_put_att(ncid, nf90_global, 'build',   trim(modrev)))
#endif
    if (present(setup)) call nf(nf90_put_att(ncid, nf90_global, 'setup', trim(setup)))
    if (present(set))   call nf(nf90_put_att(ncid, nf90_global, 'set',   trim(set)))
    if (present(case))  call nf(nf90_put_att(ncid, nf90_global, 'case',  trim(case)))

  ! definition of the dimensions
  ! syntax: nf90_def_dim(in:ncid, in:name, in:len, out:dimid)
!*  call nf(nf90_def_dim(ncid, 'lon',  nlon,           londimid))
!*  call nf(nf90_def_dim(ncid, 'lat',  ngl,            latdimid))
!*  call nf(nf90_def_dim(ncid, 'lev',  nlev,           levdimid))
    call nf(nf90_def_dim(ncid, 'time', nf90_unlimited, tdimid))

!*  dimids2d(:) = (/ londimid, latdimid,           tdimid /) ! 2d (3rd dim=t)
!*  dimids3d(:) = (/ londimid, latdimid, levdimid, tdimid /) ! 3d (4th dim=t)
!*  cnt3d(:)    = (/ nlon,     ngl,      nlev,     1 /)
    dimids1d(:) = (/ tdimid /) ! 1d (dim=t)
    cnt1d(:)    = (/ 1 /)

  ! coordinate variables & attributes
  ! longitude
!*  call nf(nf90_def_var(ncid, 'lon',   FP_PREC,        londimid,  lonid))
!*  call nf(nf90_put_att(ncid, lonid,  'long_name', 'longitude'))
!   call nf(nf90_put_att(ncid, lonid,  'units',     'degrees_east'))
  ! latitude
!*  call nf(nf90_def_var(ncid, 'lat',   FP_PREC,        latdimid,  latid))
!*  call nf(nf90_put_att(ncid, latid,  'long_name', 'latitude'))
!*  call nf(nf90_put_att(ncid, latid,  'units',     'degrees_north'))
  ! levels
!*  call nf(nf90_def_var(ncid, 'lev',   FP_PREC,        levdimid,  levid))
!*  call nf(nf90_put_att(ncid, levid,  'long_name', 'level index'))
!*  call nf(nf90_put_att(ncid, levid,  'units',     'level'))
!*  call nf(nf90_put_att(ncid, levid,  'positive',  'down'))
  ! time
    call nf(nf90_def_var(ncid, 'time',  nf90_double, tdimid,    tid))
    call nf(nf90_put_att(ncid, tid,    'long_name', 'time'))
    call nf(nf90_put_att(ncid, tid,    'units', trim(aMC_io_time_origin_string)))

    do v = 1, size(output_info)

    ! if this is a variable with *time* in name, use double prec.
#ifdef DEEPDEBUG
      print *,'aMC_output_open(',ncid,'): adding '//output_info(v)%name
#endif
      if ( index(trim(output_info(v)%name),'time') .gt. 0 ) then
      ! double for time(s)
        call nf(nf90_def_var(ncid, trim(output_info(v)%name), nf90_double, tdimid, varid(v)))
        call nf(nf90_put_att(ncid, varid(v), '_FillValue',    real(aMC_NODATA,dp)))
        call nf(nf90_put_att(ncid, varid(v), 'missing_value', real(aMC_NODATA,dp)))
      else
      ! selected default
        call nf(nf90_def_var(ncid, trim(output_info(v)%name), FP_PREC, tdimid, varid(v)))
        call nf(nf90_put_att(ncid, varid(v), '_FillValue',    FP_NODATA))
        call nf(nf90_put_att(ncid, varid(v), 'missing_value', FP_NODATA))
      endif
    ! attributes
      if (trim(output_info(v)%unit).ne.''   ) call nf(nf90_put_att(ncid, varid(v), 'units',     trim(output_info(v)%unit)))
      if (trim(output_info(v)%caption).ne.'') call nf(nf90_put_att(ncid, varid(v), 'long_name', trim(output_info(v)%caption)))
      if (trim(output_info(v)%info).ne.''   ) call nf(nf90_put_att(ncid, varid(v), 'info',      trim(output_info(v)%info)))
    end do

#if defined(NETCDF4)
  ! swtiching off fill mode (def. for netcdf-4)
    call nf(nf90_set_fill(ncid, nf90_nofill, v))
#endif

    ! end of the definitions, switch to data mode
    call nf(nf90_enddef(ncid))

    ! syntax: nf90_put_var(in:ncid, in:varid, in:values)
    ! write the data of the grid
!*  call nf(nf90_put_var(ncid, lonid,   nlondata))
!*  call nf(nf90_put_var(ncid, latid,   ngldata))
!*  call nf(nf90_put_var(ncid, levid,   nlevdata))

  else   ! if (.not.p_append) then

  ! check if output exists
    if ( .not.file_exists(filename_) ) then
      _log_ 'aMC_output_open(filename=<'//filename_//'>): file not found, stop.'
      _log_ '[ check if you try to continue from .save without available output files ]'
      stop 132
    endif
  ! open already present netCDF container, get variables IDs
    cmode = nf90_write
    call nf(nf90_open(filename_, cmode, ncid))
    call nf(nf90_inq_dimid(ncid, 'time', tid))
    cnt1d(:)    = (/ 1 /)
    do v = 1, size(output_info)
      call nf(nf90_inq_varid(ncid, trim(output_info(v)%name), varid(v)))
    enddo

  endif

! update no. of opened outputs
  aMC_io_output_opened = aMC_io_output_opened + 1

#ifdef DEBUG
    print *,'aMC_output_open(): ncid=',ncid,'filename=',filename_
#endif

  end subroutine aMC_output_open

! ------------------------------------------------------------------------------

  subroutine aMC_output_write(ncid, time, x, recno)

    implicit none

    integer, intent(in)           :: ncid
    real8, intent(in)             :: time
    real8, intent(in)             :: x(:)
    integer, intent(in), optional :: recno  ! used in sorting routine below

    integer  :: v
    integer  :: timestep


    if (.not.present(recno)) then
    ! write timestep
      call nf(nf90_inquire_dimension(ncid, tid, len=timestep))
      timestep = timestep + 1
    else
      timestep = recno
    endif

  ! syntax: nf90_put_var(ncid, varid, values, start, cnt)
  ! start:  start in netcdf variable
  ! cnt:    number of netcdf variable points
  ! values: starting point of the fortran variable
!*  start3d = (/ 1,    1,   1,    timestep /)
    start1d = (/ timestep /)

    call nf(nf90_put_var(ncid, tid, time, (/ timestep /) ))
    do v = 1, size(x)
      call nf(nf90_put_var(ncid, varid(v), (/ x(v) /), start1d, cnt1d)) !start3d, cnt3d))
    enddo

#ifdef DEEPDEBUG
    print *,'aMC_output_write(): ncid=',ncid,'ts=',timestep,'time=',time
#endif

    call nf(nf90_sync(ncid)) ! write buffer to file

  end subroutine aMC_output_write

  !----------------------------------------------------------------------------*

  subroutine aMC_write_output_buf(ncid, time_idx, x)

    implicit none

    integer, intent(in) :: ncid
    integer, intent(in) :: time_idx  ! index of time variable to use as a coordinate along T
    real8, intent(in)   :: x(:,:)    ! (rec,1:var)

    integer  :: vi, bufsize
    integer  :: timestep

  ! get last record no.
    call nf(nf90_inquire_dimension(ncid, tid, len=timestep))
    timestep = timestep + 1

    start1d = (/ timestep /)
  ! get the length of buffer output
    bufsize = size(x,dim=1)
    cnt1d = (/ bufsize /)

#ifdef DEEPDEBUG
    print *,'aMC_write_output_buf: ncid=',ncid,'ts=',timestep,'bufsize=',bufsize
#endif

  ! writeout
  ! dimension
    call nf(nf90_put_var(ncid, tid, x(:,time_idx), start1d, cnt1d))
  ! variables
    do vi = 1,size(x,dim=2)
#ifdef DEEPDEBUG
      print *,'> var.#',vi,'data:',x(:,vi)
#endif
      call nf(nf90_put_var(ncid, varid(vi), x(:,vi), start1d, cnt1d))
    enddo

    call nf(nf90_sync(ncid)) ! write buffer to file

  end subroutine aMC_write_output_buf

! ------------------------------------------------------------------------------

  subroutine aMC_output_sort(ncid)

    implicit none

    integer,  intent(in) :: ncid

  ! all data
    real8, allocatable   :: t(:), x(:)
  ! sorting apparatus
    integer, allocatable :: si(:)
    integer              :: recno, vi, ri, re, rt
    logical              :: ex

  ! get timesteps no.
    call nf(nf90_inquire_dimension(ncid, tid, len=recno))
  ! no steps to sort? return
    if ( recno.lt.2 ) return

  ! we read time and variables one-by-one in again, write out sorted
    allocate(t(recno))
    allocate(x(recno))
#ifdef DEEPDEBUG
    _log_ 'aMC_sort_output('//num2str(ncid)//'):'
    _log_ '| recno:',recno,'size(varid): ',size(varid)
    _log_ '| size(t(:)) =',size(t(:))
#endif
    start1d = (/ 1 /)
    cnt1d = (/ recno /)
  ! times from axis
    call nf(nf90_get_var(ncid, tid, t(:), start1d, cnt1d))

  ! get the sort sequence
    call idxsort(t(:),si)
#ifdef aMC_output_time_gaps
  ! adding 1us gaps for ferret
    do ri=1,recno-1
      if ( (t(si(ri+1))-t(si(ri))).lt.1e-6_dp ) t(si(ri+1)) = t(si(ri))+1e-6_dp
    enddo
#endif

#ifdef DEEPDEBUG
    _log_ '| ncid / tid / start1d / cnt1d =',ncid, tid, start1d, cnt1d
    _log_ '|    si =',si(1:5),    '...', si(size(si)-4:size(si))
    _log_ '| t(si) =',t(si(1:5)), '...', t(si(size(si)-4:size(si)))
#endif

  ! write out in a new sequece
  ! time dimension
    t(:) = t(si(:)) ! not thread-safe netCDF lib. can't use t(si(:)) indexing, need to reorder
    call nf(nf90_put_var(ncid, tid, t(:), start1d, cnt1d))
  ! variables
    do vi = 1,size(varid)
      call nf(nf90_get_var(ncid, varid(vi), x(:), start1d, cnt1d)) ! read in
      x(:) = x(si(:)) ! not thread-safe netCDF lib. can't use x(si(:)) indexing, need to reorder
      call nf(nf90_put_var(ncid, varid(vi), x(:), start1d, cnt1d)) ! write out
    enddo

  ! cleanup
    deallocate(t)
    deallocate(x)
    deallocate(si)

#ifdef DEBUG
    print *,'aMC_sort_output: ncid=',ncid
#endif

  end subroutine aMC_output_sort

! ------------------------------------------------------------------------------

  subroutine aMC_output_close(ncid)

    implicit none

    integer, intent(in) :: ncid

#ifdef DEBUG
    print *,'aMC_close_output: ncid=',ncid
#endif

    call nf(nf90_close(ncid))

  ! update no. of opened outputs
    aMC_io_output_opened = aMC_io_output_opened - 1

  ! free variables indices array -- only when the last file is closing
#ifndef aMC_output_time_sort
  ! and when no sorting is done -- otherwise we need varid later
    if (aMC_io_output_opened.eq.0) deallocate(varid)
#endif

  end subroutine aMC_output_close

! ------------------------------------------------------------------------------


! - read column from tabulated data file identified by vname -------------------
  function aMC_read_tab_data_colvar(fname, vname, r,re, i,ie, i8,i8e, s, sf, fsep, esep, na, ftrim, colno) result(recno)

    use, intrinsic  :: iso_fortran_env

    implicit none

    character(len=*), intent(in)    :: fname, vname !> input file/variabe name
    character, intent(in), optional :: fsep, esep   !> optional field/error part separators
    character(len=*), &
      intent(in), optional          :: na           !> optional flag for not available value
    logical, intent(in), optional   :: ftrim        !> optional flag for field trimming

    integer                         :: recno        !> returned no. of records read
    integer, intent(out), optional  :: colno        !> returned no. of column to read

    real8, allocatable, &
      intent(out), optional, target :: r(:), re(:)  !> arrays with data/errors
    integer, allocatable, &
      intent(out), optional, target :: i(:), ie(:)
    int8, allocatable, &
      intent(out), optional, target :: i8(:),i8e(:)
    strings, &
      intent(out), optional, target :: s(:)

    real8, optional                 :: sf           !> optional scaling factor (applied to available data only)

    integer                         :: colno_ = 0, s_maxlen = 1, ll, jj,  iou, rc
    string                          :: fn, vn, si, sd, sv, se
    character(len=16384)            :: sbuf
    string                          :: fsep_, esep_, na_ !> field and error part separators, n/a flag
    logical                         :: ftrim_, eof, eor
    real8                           :: sf_

  ! info
    fn = trim(fname); vn = trim(vname)
    si = 'aMC_read_tab_data_var('//fn//':'//vn//'): '

  ! separators, trim/na flags, scaling: defaults and overrides
     fsep_ = _tab_;   if ( present(fsep)  )  fsep_ = fsep
     esep_ = '~';     if ( present(esep)  )  esep_ = esep
       na_ = aMC_NADATA; if ( present(na) )    na_ = na
    ftrim_ = .false.; if ( present(ftrim) ) ftrim_ = ftrim
       sf_ = 1.0_dp;  if ( present(sf)    )    sf_ = sf

#ifdef DEBUG
    _log_ si
    _log_ '  na=<'//na_//'> esep=<'//esep_//'> fsep=<'//fsep_//'> ftrim='//num2str(l=ftrim_)
#endif

  ! check whether only one variable/error is provided
    jj = 0;
    if ( present(r)  ) jj = jj + 1;
    if ( present(i)  ) jj = jj + 1;
    if ( present(i8) ) jj = jj + 1;
    if ( present(s)  ) jj = jj + 1;
    if (jj.ne.1) then
      _log_ si//'none or more than one variable (r|i[8]|s) specified, stop.'
      stop 141
    endif
    jj = 0;
    if ( present(re) .and.(.not.present(r))  ) jj = jj + 1;
    if ( present(ie) .and.(.not.present(i))  ) jj = jj + 1;
    if ( present(i8e).and.(.not.present(i8)) ) jj = jj + 1;
    if ( jj.ne.0) then
      _log_ si//'wrong variable error (re|i[8]e) specified, stop.'
      stop 141
    endif

  ! check if file exists
    if ( .not.file_exists(fname) ) then
      _log_ si//'file not found, stop.'
      stop 142
    endif

  ! file handle
    iou = aMC_iou_find()

  ! get the var. name & column no. from header (1st line) and no. of records
    open(unit=iou, file=trim(fname), status='old')
    recno = -1 ! expecting header
    eof = .false.
    do while ( .not.eof )
      ll = 0; sbuf = ''
      read(iou, '(a)', advance='no', iostat=rc, size=ll) sbuf
#ifdef DEEPDEBUG
      print *,'read sbuf, ll=',ll,' rc=',rc,' eof=',eof,' sbuf=<'//sbuf(1:ll)//'>'
#endif
      eof = IS_IOSTAT_END(rc)
    ! skipping eof/comment lines
      if ( eof.or.(sbuf(1:1).eq.'#') ) cycle
      if ( ll.gt.0 ) recno = recno + 1
#ifdef DEEPDEBUG
      print *,' recno=',recno
#endif

    ! scan variable names in header line
      if ( recno.eq.0 ) then
        colno_ = 0 ! column no. sought
        sv = '*'; jj = 0
        do while ( sv.ne.'' )
          jj = jj + 1
          sv = str_field(sbuf(1:ll), fsep_, jj, notrim=.not.ftrim_)
#ifdef DEEPDEBUG
          _log_ '  colname=<'//sv//'>'
#endif
          if (sv.eq.vn) exit
        enddo
        if (sv.eq.vn) then
          colno_ = jj
        else
          _log_ si//'could not find variable <'//trim(vname)//'>, check the input file. stop.'
          stop 143
        endif
      else
      ! do other pre-scanning
      ! max. length of values in strings column
        if (present(s)) then
          sv = str_field(sbuf(1:ll), fsep_, colno_, notrim=.not.ftrim_)
          s_maxlen=max(s_maxlen,len(sv))
        endif
      endif
    enddo

    if (recno.lt.1) then
      _log_ si//'no records read, check the input file. stop.'
      stop 144
    endif

    if (present(colno)) colno = colno_  ! update output value
#ifdef DEBUG
    _log_ '  colno=',colno_,'>, recno='//num2str(i=recno)
    sd = ''
#endif

  ! prepare data arrays
    if ( present(r)   ) call resize(r,   recno, init=real(0., kind(r))  )
    if ( present(re)  ) call resize(re,  recno, init=real(0., kind(re)) )
    if ( present(i)   ) call resize(i,   recno, init= int(0 , kind(i))  )
    if ( present(ie)  ) call resize(ie,  recno, init= int(0 , kind(ie)) )
    if ( present(i8)  ) call resize(i8,  recno, init= int(0 , kind(i8)) )
    if ( present(i8e) ) call resize(i8e, recno, init= int(0 , kind(i8e)))
    if ( present(s) ) then
     !allocate(character(len=s_maxlen) :: s(1:recno))
      allocate(s(1:recno))
    endif

  ! read data in
    rewind(unit=iou, iostat=rc)
    jj = -1 ! expecting header
    eof = .false.
    do while (.not.eof)
      read(iou, '(a)', advance='no', iostat=rc, size=ll) sbuf
      eof = IS_IOSTAT_END(rc)
#ifdef DEEPDEBUG
      print *,'read sbuf, jj=',jj,' ll=',ll,' rc=',rc,' eof=',eof,' sbuf=<'//sbuf(1:ll)//'>'
#endif
      if (eof.or.(sbuf(1:1).eq.'#').or.(ll.lt.1)) cycle ! eof or comment/empty line

      jj = jj + 1        ! next record no.
      if (jj.lt.1) cycle ! skipping header

    ! get data as a string
      sv = str_field(sbuf(1:ll), fsep_, colno_, notrim=.not.ftrim_)
#ifdef DEEPDEBUG
      print *,'read in <'//sv//'>'
#endif

    ! read in string type
      if (present(s)) then
        s(jj)%s = sv
#ifdef DEEPDEBUG
       print *,'s(jj)=<'//trim(s(jj)%s)//'>'
#endif
#ifdef DEBUG
        sd = sd//s(jj)%s//':' ! store values read to output info later
#endif
        cycle
      endif

    ! numerical
      se = str_field(sv, esep_, 2, notrim=.not.ftrim_)  ! error (if present)
      sv = str_field(sv, esep_, 1, notrim=.not.ftrim_)  ! value
#ifdef DEEPDEBUG
      print *, 'recno='//num2str(jj)//' sv=<'//sv//'('//se//')>'
#endif
      if ( present(r)   ) call str2num(sv,r=r(jj),    na=na_, sf=sf_)
      if ( present(re)  ) call str2num(se,r=re(jj),   na=na_, sf=sf_)
      if ( present(i)   ) call str2num(sv,i=i(jj),    na=na_, sf=sf_)
      if ( present(ie)  ) call str2num(se,i=ie(jj),   na=na_, sf=sf_)
      if ( present(i8)  ) call str2num(sv,i8=i8(jj),  na=na_, sf=sf_)
      if ( present(i8e) ) call str2num(se,i8=i8e(jj), na=na_, sf=sf_)
#ifdef DEBUG
      if ( present(r)  ) sv = num2str(r=r(jj)); if ( present(re) )  se = num2str(r=re(jj))
      if ( present(i)  ) sv = num2str(i=i(jj));   if (present(ie))  se = num2str(i=ie(jj))
      if ( present(i8) ) sv = num2str(i8=i8(jj)); if (present(i8e)) se = num2str(i8=i8e(jj))
      sd = sd//sv//'('//se//')'//':' ! store values read to output info later
#endif
    enddo

#ifdef DEBUG
    print *,'  data=<'//sd//'>'
#endif

    close(iou)

  ! info
    _log_ si//'column #'//num2str(i=colno_)//', records read: '//num2str(i=recno)

    end function aMC_read_tab_data_colvar


end module aMC_io
