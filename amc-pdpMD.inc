! - f90 -

! ----- pdpMD: pore distribution from particle mob. eq. diameter ---------------

! - parameters -----------------------------------------------------------------
! - aMC parameters overrides ---------------------------------------------------
!undef  aMC_no_sim_on_master      // master only serves run control & I/O
!undef  aMC_output_non_real       // output also uncuccessful realisations
!undef  aMC_output_single_case    // enable output containers for single cases
!undef  aMC_output_common_set     // enable output containers for sets of cases
!undef  aMC_output_probe_stat     // output MC probe statistic (use to check for skewed input)
!undef  aMC_output_spare_extra_rq // skip PE output requests if the local amnt. of hits is greater than ~ max_req / (#PEs)
! - tuning/debugging -----------------------------------------------------------
!define aMC_run_single_sim        // enable to run a single try simulation
!define DEBUG                     // useful runtime info
!define DEEPDEBUG                 // a lot of runtime info

! ----- global imports ---------------------------------------------------------
#ifdef aMC__uses
#endif

! ----- global definitions -----------------------------------------------------
#ifdef aMC__defs
! lab data
  integer              :: pdpMD_smpQ              !> no of lab samples
  integer, parameter   :: pdpMD_smp_max = 100     !> - maximum
  integer, parameter   :: pdpMD_idx_sn  = 0, &    !> indices of variables in data array: sample no.
                          pdpMD_idx_s   = 1, &    !> - saturation ratio (rh/100)
                          pdpMD_idx_ds  = 2, &    !>   - its change
                          pdpMD_idx_Db  = 3, &    !> - mobility equiv. diameter
                          pdpMD_idx_dDb = 4       !> its change
  real8                :: pdpMD_lab(pdpMD_idx_sn:pdpMD_idx_dDb,1:pdpMD_smp_max)  !> data array
! parameters/variables
! fixed (so far)
  real8                :: pdpMD_R = 8.3144626181532              !> [J/K/mol] ugc
  real8                :: pdpMD_T = 298.                         !> [K] temperature
  real8                :: pdpMD_Pi_6 = 4._dp*atan(1.0_dp)/6._dp  !> Pi/6
! MCed Kelvin eq. fitting parameters, etc.
  real8                :: pdpMD_aw,  &         !> solution saturation in pores
                          pdpMD_st,  &         !> surface tension of solution
                          pdpMD_vw,  &         !> molar volume of water in solution
                          pdpMD_phi, &         !> contact angle (0=completely wet)
                          pdpMD_Dpc            !> collapsed pores diameter
  integer              :: pdpMD_Np             !> number of (collapsed) pores
! derived parameters
  real8, dimension(1:pdpMD_smp_max) &          !> derived values for each sample
                       :: pdpMD_dVb, &         !> change in volume (from bulk)
                          pdpMD_dVc, &         !> change in volume (from pores)
                          pdpMD_Dp             !> pore diameter
! hit sets/cases
  integer              :: pdpMD_set_fit,  &    !> set fitting lab data
                          pdpMD_set_Dpc1, &    !> collapsed pores size < 1nm')
                          pdpMD_set_aw1,  &    !> aw~1
                          pdpMD_set_phi0, &    !> phi~0
                          pdpMD_set_pwater, &  !> pure water (surface tension, aw)
                          pdpMD_set_starch     !> assumed parameters for starch (aw~1, phi~0, vw, st of pure water)
  integer              :: pdpMD_case_smp(1:pdpMD_smp_max)  !> case nos. for samples
#endif


! ----- custom routines --------------------------------------------------------
#ifdef aMC__contains
! ----- read lab data for pdpMD -----
  subroutine pdpMD_read_lab_data()

    use messy_main_tools, only: find_next_free_unit
    use, intrinsic  :: iso_fortran_env

    implicit none

    character(len=*), parameter :: ifname = 'starch.dat'
    real8                       :: rh, drh, Db, dDb
    integer                     :: i, iou, rc, sn

#ifdef DEBUG
    print *,'pdpMD_read_lab_data(): reading from '//ifname
#endif

    iou = find_next_free_unit(100,200)
    open(unit=iou, file=trim(ifname), status='old')

  ! read no. of samples
    read(iou, *, iostat=rc) pdpMD_smpQ

#ifdef DEBUG
    print *,'pdpMD_smpQ = '//num2str(i=pdpMD_smpQ)
    print *,'no. s ds Db dDb'
#endif

  ! skip header lines (2)
    do i = 1,2
      read(iou, *)
    enddo

    readloop: do i=1, pdpMD_smpQ
    ! read line of data
      read(iou, *, iostat=rc) sn, rh, drh, Db, dDb
      if ( rc /= 0 ) then
        if ( rc == iostat_end ) then
          exit readloop
        else
          print *, 'pdpMD_read_lab_data(): error reading "'//trim(ifname)//'", code: ', rc
          stop 101
        endif
      else
      ! put into data array
        pdpMD_lab(pdpMD_idx_sn,i)  = sn
        pdpMD_lab(pdpMD_idx_s,i)   = rh/1e2
        pdpMD_lab(pdpMD_idx_ds,i)  = drh/1e2
        pdpMD_lab(pdpMD_idx_Db,i)  = Db
        pdpMD_lab(pdpMD_idx_dDb,i) = dDb
      endif
#ifdef DEBUG
      print *,pdpMD_lab(:,i)
#endif
    enddo readloop
    close(iou)

    if ( p_parallel_io ) print *, 'pdpMD_read_lab_data()@master: read ',sn,' sample record(s)'

#ifdef DEBUG
    print *,'pdpMD_read_lab_data(): finish'
#endif

  end subroutine pdpMD_read_lab_data
#endif /* aMC__contains */

! ----- custom [de]init --------------------------------------------------------
#ifdef    aMC__init__defs
#endif /* aMC__init__defs */
#ifdef    aMC__init__code
    call pdpMD_read_lab_data()
#endif /* aMC__init__code */

#ifdef    aMC__deinit__defs
#endif /* aMC__deinit__defs */
#ifdef    aMC__deinit__code
#endif /* aMC__deinit__code */

! ----- MC parameter setup -----------------------------------------------------
#ifdef    aMC__parameter_setup__defs
#endif /* aMC__parameter_setup__defs */
#ifdef    aMC__parameter_setup__code
  ! MC parameters
  ! pore size <-> rh correspondence via mod. Kelvin eq.: fitting parameters
    res = aMC_mcpar_register(aMC_kind_unif, r=pdpMD_aw,  range=(/  0.9 ,   1.0  /), info='aw  |          | solution saturation in pores' )
#ifdef pdpMD_purewater
    res = aMC_mcpar_register(aMC_kind_unif, r=pdpMD_st,  range=(/ 72.0 ,  72.0  /), info='st  | mN/m     | surface tension of solution' )   ! 72 for water @298K
    res = aMC_mcpar_register(aMC_kind_unif, r=pdpMD_vw,  range=(/ 18.04,  18.04 /), info='vw  | cm^3/mol | molar volume of water in solution' )
#else
    res = aMC_mcpar_register(aMC_kind_unif, r=pdpMD_st,  range=(/ 70.0 ,  90.0  /), info='st  | mN/m     | surface tension of solution' )
    res = aMC_mcpar_register(aMC_kind_unif, r=pdpMD_vw,  range=(/ 17.0 ,  19.0  /), info='vw  | cm^3/mol | molar volume of water in solution' )
#endif
    res = aMC_mcpar_register(aMC_kind_unif, r=pdpMD_phi, range=(/  0.0 ,  90.0  /), info='phi | degrees  | contact angle' )
  ! probed collapsed pores diameter and number
    res = aMC_mcpar_register(aMC_kind_unif, r=pdpMD_Dpc, range=(/  0.0 ,  10.0  /), info='Dpc | nm       | collapsed pores diameter' )
    res = aMC_mcpar_register(aMC_kind_unif, i=pdpMD_Np,  range=(/  0.  ,   5.e3 /), info='Np  | count    | number of (collapsed) pores' )

  ! output
  ! derived parameters -> sample-dependent, need update (array support, ar= ai= ) of the output facility
    res = aMC_output_add('Dp  | nm   | pore diameter', ra=pdpMD_Dp)
    res = aMC_output_add('dVb | nm^3 | change in bulk volume from Db+dDb (current-previous)', ra=pdpMD_dVb)
    res = aMC_output_add('dVc | nm^3 | change in bulk volume from collapsed pores volume (Dp,Npc)', ra=pdpMD_dVc)
  ! lab data
    res = aMC_output_add('smp | no.   | sample                                 ', ra=pdpMD_lab(pdpMD_idx_sn,:) )
    res = aMC_output_add('s   | pa/pa | saturation ratio (rh/100)              ', ra=pdpMD_lab(pdpMD_idx_s,:)  )
    res = aMC_output_add('ds  | pa/pa | change in s (w.r.t. preceding s value) ', ra=pdpMD_lab(pdpMD_idx_ds,:) )
    res = aMC_output_add('Db  | nm    | bulk diameter                          ', ra=pdpMD_lab(pdpMD_idx_Db,:) )
    res = aMC_output_add('dDb | nm    | change in Db (w.r.t. preceding s value)', ra=pdpMD_lab(pdpMD_idx_dDb,:))

  ! hit sets/cases
    pdpMD_set_fit    = aMC_hit_set_add('fit    | fitting changes in bulk volume')
    pdpMD_set_Dpc1   = aMC_hit_set_add('Dpc1   | collapsed pores size < 1nm')
    pdpMD_set_aw1    = aMC_hit_set_add('aw1    | full pore saturation (aw>0.99)')
    pdpMD_set_phi0   = aMC_hit_set_add('phi0   | full pore wettening (phi<1)')
    pdpMD_set_pwater = aMC_hit_set_add('pwater | pure water (st=72+-1, vw=18.04+-0.1)')
    pdpMD_set_starch = aMC_hit_set_add('starch | starch (aw>0.99, phi<1, pure water)')
    do ci = 1, pdpMD_smpQ
      res = int(pdpMD_lab(pdpMD_idx_sn,ci)) ! sample no.
      pdpMD_case_smp(ci) = aMC_hit_case_add(num2str(i=res,fmt='(I2.2)')//' | sample #'//num2str(i=res)//' at'// &
                      ' s(ds)='//num2str(r=pdpMD_lab(pdpMD_idx_s,ci))//'('//num2str(r=pdpMD_lab(pdpMD_idx_ds,ci))//')'// &
                    ' Db(dDb)='//num2str(r=pdpMD_lab(pdpMD_idx_Db,ci))//'('//num2str(r=pdpMD_lab(pdpMD_idx_dDb,ci))//')')
    enddo
#endif /* aMC__parameter_setup__code */


! ----- new initial state ------------------------------------------------------
#ifdef    aMC__new_x0__defs
#endif /* aMC__new_x0__defs */
#ifdef    aMC__new_x0__code
#endif /* aMC__new_x0__code */

! ----- prepare output ---------------------------------------------------------
#ifdef    aMC__output_prepare__defs
#endif /* aMC__output_prepare__defs */
#ifdef    aMC__output_prepare__code
#endif /* aMC__output_prepare__code */

! ----- find matching cases ----------------------------------------------------
#ifdef    aMC__match__defs
    logical   :: fit, pure_water
#endif /* aMC__match__defs */
#ifdef    aMC__match__cases__code
    ! case number is the sample no.
    ! hit if dDb for a given sample fits the observed value within +-1%
      fit = dabs( (pdpMD_dVb(case)/pdpMD_dVc(case)-1.0) ) .le. 1e-2
      aMC_hit_set(pdpMD_set_fit)%is_hit = fit
    ! subsets
      aMC_hit_set(pdpMD_set_Dpc1)%is_hit = fit .and. ( pdpMD_Dpc .lt. 1.0  )  ! collapsed pores diameter < 1nm
      aMC_hit_set(pdpMD_set_aw1)%is_hit  = fit .and. ( pdpMD_aw  .gt. 0.99 )  ! aw~1 (saturated solution)
      aMC_hit_set(pdpMD_set_phi0)%is_hit = fit .and. ( pdpMD_phi .lt. 1.0  )  ! phi~0 (full wettening)
      pure_water = aMC_check_pair( pdpMD_vw, 18.04_dp, 0.1_dp ) &             ! pure water
             .and. aMC_check_pair( pdpMD_st,   72._dp,  1._dp )
      aMC_hit_set(pdpMD_set_pwater)%is_hit = fit .and. pure_water
      aMC_hit_set(pdpMD_set_starch)%is_hit = fit &
                                     .and. aMC_hit_set(pdpMD_set_aw1)%is_hit &
                                     .and. aMC_hit_set(pdpMD_set_phi0)%is_hit &
                                     .and. pure_water
    ! probe statistic (all)
      aMC_hit_set(aMC_hit_set_probestat)%is_hit = .true.
#endif /* aMC__match__cases__code */


! ----- aMC_physc --------------------------------------------------------------
#ifdef    aMC__physc__defs
#endif /* aMC__physc__defs */
#ifdef    aMC__physc__code
#define __ 1:pdpMD_smpQ
    ! calculating decrease in Db based on Kelvin f-la and guessed diameter of collapsed pores

    ! pore size as a f-n of s & modified Kelvin f-la
      pdpMD_Dp(__) = -4.*pdpMD_st*pdpMD_vw*cos(pdpMD_phi/180.*pdpMD_Pi_6*6.)/(pdpMD_R*pdpMD_T*log(pdpMD_lab(pdpMD_idx_s,__)/pdpMD_aw))

    ! change in bulk volume from change in bulk diameter (collapsed - previous)
      pdpMD_dVb(__) = pdpMD_Pi_6*( (pdpMD_lab(pdpMD_idx_Db,__))**3 - (pdpMD_lab(pdpMD_idx_Db,:)-pdpMD_lab(pdpMD_idx_dDb,__))**3 )

    ! change in bulk volume from collapsed pores
      pdpMD_dVc(__) = pdpMD_Pi_6*pdpMD_Np*( pdpMD_Dpc - pdpMD_Dp(__) )

#ifdef DEBUG
      print *,'pdpMD_physc():'
      print *,' Dp:',pdpMD_Dp(__)
      print *,'dVb:',pdpMD_dVb(__)
      print *,'dVc:',pdpMD_dVc(__)
#endif

    ! shot is done when simulation end is reached or error occured
      aMC_shot_done = .true.
      aMC_shot_real = .true.
#endif /* aMC__physc__code */
! ----- aMC_physc:check_req_hits -----
! inside the loop over cases: cn = aMC_match_case_from, aMC_match_case_upto
#ifdef    aMC__physc__check_req_hits__code
        ! stop when hit statistic is complete for all samples
          do sn = aMC_match_set_from, aMC_match_set_upto
            if ( aMC_hits(cn,sn,00).lt.aMC_req_hits ) then
              req_hits_found4all = .false.
              exit ! do-loop
            endif
          enddo
#endif /* aMC__physc__check_req_hits__code */


! ----- info output ------------------------------------------------------------
#ifdef    aMC__info__code
#ifdef pdpMD_purewater
    _info_ '| - assuming pure water'
#endif
#endif /* aMC__info__code */

! --- XXX ---
#ifdef    aMC__XXX__defs
#endif /* aMC__XXX__defs */
#ifdef    aMC__XXX__code
#endif /* aMC__XXX__code */
