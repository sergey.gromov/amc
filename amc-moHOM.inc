! ----- f90 -----

! ----- moHOM: mainz organics mechanism (MOM) experiments on HOM chemistry -----

! - parameters -----------------------------------------------------------------
#define USE_CAABA
!define moHOM_oz
#define moHOM_dark

! - aMC parameters overrides ---------------------------------------------------
!undef  aMC_no_sim_on_master      // master only serves run control & I/O
!undef  aMC_output_non_real       // output also uncuccessful realisations
!undef  aMC_output_single_case    // enable output containers for single cases
!undef  aMC_output_common_set     // enable output containers for sets of cases
#undef  aMC_output_probe_stat     // output MC probe statistic (use to check for skewed input)
!undef  aMC_output_spare_extra_rq // skip PE output requests if the local amnt. of hits is greater than ~ max_req / (#PEs)
! - tuning/debugging -----------------------------------------------------------
!define aMC_run_single_sim        // enable to run a single try simulation
!define DEBUG                     // useful runtime info
!define DEEPDEBUG                 // a lot of runtime info

! ----- global imports ---------------------------------------------------------
#ifdef    aMC__uses
    use messy_main_constants_mem, only: OneDay, R_gas, N_A
    use messy_main_tools, only: psat_mk
#endif /* aMC__uses */

! ----- global definitions -----------------------------------------------------
#ifdef    aMC__defs
! experiment data
  integer              :: expQ      = 0, &    !> no of experiments
                          exp_no    = 1       !> current exp. (if variable conditions are used)
  integer, parameter   :: exp_max   = 30      !> - maximum

  integer, parameter   :: idx_rn    = 0,  &   !> indices of variables in data array: run no.
                          idx_AP    = 1,  &   !> - alpha-pinene MR
                          idx_IP    = 2,  &   !> - isoprene
                          idx_C10   = 3,  &   !> - C10
                          idx_C15   = 4,  &   !> - C15
                          idx_C20   = 5,  &   !> - C20
                          idx_C20lo = 6,  &   !> - C20 lower CI
                          idx_C20hi = 7,  &   !> - C20 higher CI
                          idx_O3    = 8,  &   !> - ozone
                          idx_NO2   = 9,  &   !> - NO2
                          idx_NO    = 10, &   !> - NO
                          idx_T     = 11, &   !> - mean T
                          idx_rh    = 12, &   !> - rel. humidity
                          idx_BCY   = 13, &   !> - BCY (?)
                          idx_HOMs  = 14      !> - HOMs MR

  character(len=64)    :: exp_runID(1:exp_max)  !> run ID
  real8                :: exp_data(idx_rn:idx_HOMs,1:exp_max)  !> data array
! simulation parameters/variables
  real8                :: APem, IPem, & !> alpha-pinene & isoprene emission rates
                          kdil, &           !> chamber air dilution rate
                          HOMs(1:exp_max),        &  !> simulated HOMs: mixing ratio
                          HOMs_rOC(1:exp_max),    &  !>               - O:C ratio
                          HOMs_rHC(1:exp_max),    &  !>               - H:C ratio
                          HOMs_lr(1:exp_max),     &  !>               - loss rate
                          HOMs_lr_rOC(1:exp_max), &  !>               - loss O:C ratio
                          HOMs_lr_rHC(1:exp_max), &  !>               - loss H:C ratio
                          eqt(1:exp_max), & !> simulated equilibration time
                          SS_threshold, &   !> rel. change threshold (+-)
                          SS_period         !> minimum period required to conclude SS
!!                        AP, IP,  &  !> simulated a-pinene & isoprene MRs
  integer, allocatable :: SS_inds(:),   &   !> indices of simulated speciesmonitored for steady-state (SS)
                          out_inds(:),  &   !>            species MRs to be output
                          HOMs_inds(:), &   !>            HOM species indices
                          HOMs_C(:),    &   !>                        C count
                          HOMs_O(:),    &   !>                        O count
                          HOMs_H(:)         !>                        H count
  real8, allocatable   :: C_SS(:,:), &      !> steady-state monitored species concentrations
                          SS_start(:),  &   !> guessed time of SS start
                          C_synair(:), &    !> synthetic air species concentrations
                          C_out(:,:)        !> output species concentrations [idx,exp]
! hit sets/cases
  integer              :: moHOM_case_serv, &      !> case needed for tests/probe stat/etc.
                          moHOM_case_exp(1:exp_max)  !> case nos. corresponding to experiments
  integer              :: moHOM_set_eq,   &       !> set with successful equilibrations
                          moHOM_set_neq,  &       !> - not successful equilibrations
                          moHOM_set_AP,   &       !> - fitting alpha-pinene MR
                          moHOM_set_IP,   &       !> - fitting isoprene MR
                          moHOM_set_C10,  &       !> - fitting C10 MR
                          moHOM_set_C15,  &       !> - fitting C15 MR
                          moHOM_set_C20,  &       !> - fitting C20 MR
                          moHOM_set_APIP, &       !> - fitting AP & IP
                          moHOM_set_HOMs, &       !> - fitting total HOMs MR
                          moHOM_set_A12,  &       !> set fitting AP+C10+C20
                          moHOM_set_AI5,  &       !> set fitting AP+IP+C15
                          moHOM_set_C1020, &      !> set fitting C10+C20
                          moHOM_set_C1015, &      !> set fitting C10+C15
                          moHOM_set_C1520, &      !> set fitting C15+C20
                          moHOM_set_fit           !> set fitting all data

  real8, parameter     :: ppb = 1e-9_dp, ppt = 1e-12_dp
#endif /* aMC__defs */

! ----- custom routines --------------------------------------------------------
#ifdef    aMC__contains
! ----- read experiment data for moHOM -----
  subroutine moHOM_read_exp_data()

    use messy_main_tools, only: find_next_free_unit
    use, intrinsic  :: iso_fortran_env

    implicit none

#ifdef moHOM_oz
    character(len=*), parameter :: ifname = 'CLOUD-oz.dat'
#endif
#ifdef moHOM_dark
    character(len=*), parameter :: ifname = 'CLOUD-dark.dat'
#endif
    integer                     :: i, z, iou, rc, en
    string                      :: s, v
    character(len=4096)         :: sbuf
    real8                       :: skip
    logical                     :: eor, eof

    expQ = 0; i = 0
    exp_data(:,:) = 0.

    _log__ 'moHOM_read_exp_data('//ifname//'): reading runs:'

    iou = find_next_free_unit(100,200)
    open(unit=iou, file=trim(ifname), status='old', iostat=rc)
    readloop: do while (.not.eof)

    ! read line
      read(iou, '(a)', advance='no', iostat=rc, size=z) sbuf
      eof = IS_IOSTAT_END(rc)
      eor = IS_IOSTAT_EOR(rc)
      if ((rc/=0).and.(.not.(eof.or.eor))) then
        print *, 'soHOM_read_exp_data(): error reading "'//trim(ifname)//'", code: ', rc
        stop 101
      endif

      if ( eof ) exit readloop

    ! skip comments
      if (sbuf(1:1).eq.'#') cycle

    ! id 1st col + numeric data
      s = str_field(sbuf(1:z),_tab_,1,notrim=.true.)
      v = sbuf(len(s)+2:z)

    ! next sample
      i = i + 1
    ! run ID
      exp_runID(i) = trim(s)
      _log__ ' '//str_field(trim(exp_runID(i)),'|',1)
    ! convert values
#ifdef DEEPDEBUG
        print *,'moHOM_read_exp_data(): s='//s
        print *,'moHOM_read_exp_data(): trying to convert v='//v
#endif
      exp_data(idx_rn,i) = i  ! record no.
#ifdef moHOM_oz
      read(v,*,iostat=rc) skip, &  ! oz, ap, hom
                          exp_data(idx_AP,i), &
                          exp_data(idx_HOMs,i)
#ifdef DEEPDEBUG
      _log_ exp_data(idx_AP:idx_HOMs,i)
#endif
#endif
#ifdef moHOM_dark
      read(v,*,iostat=rc) exp_data(idx_AP,i),  &
                          exp_data(idx_IP,i),  &
                          exp_data(idx_C10,i), &
                          exp_data(idx_C15,i), &
                          exp_data(idx_C20,i), &
                          exp_data(idx_C20lo,i), &
                          exp_data(idx_C20hi,i), &
                          exp_data(idx_O3,i),  &
                          exp_data(idx_NO2,i), &
                          exp_data(idx_NO,i),  &
                          exp_data(idx_T,i),   &
                          exp_data(idx_rh,i)

#ifdef DEEPDEBUG
      _log_ exp_data(idx_AP:idx_rh,i)
#endif
#endif

    ! update exp. qty.
      expQ = i

    enddo readloop
    close(iou)

    if ( p_parallel_io ) &
      _log_ _cr_//'moHOM_read_exp_data()@master: read ',expQ,' record(s)'

  ! post-process (units, etc.)
#ifdef moHOM_oz
    exp_data(idx_AP,:)   = exp_data(idx_AP,:)   * ppt
    exp_data(idx_HOMs,:) = exp_data(idx_HOMs,:) * ppt
#endif
#ifdef moHOM_dark
    exp_data(idx_T,:)     = exp_data(idx_T,:) + 273.15  ! C -> K
    exp_data(idx_rh,:)    = exp_data(idx_rh,:) * 1e-2   ! % -> frac
  ! species MRs
    exp_data(idx_AP,:)    = exp_data(idx_AP,:)  * ppt
    exp_data(idx_IP,:)    = exp_data(idx_IP,:)  * ppt
    exp_data(idx_O3,:)    = exp_data(idx_O3,:)  * ppb
    exp_data(idx_NO2,:)   = exp_data(idx_NO2,:) * ppb
    exp_data(idx_NO,:)    = exp_data(idx_NO,:)  * ppb
  ! HOMs are as concentrations
  ! exp_data(idx_C10,:)   = exp_data(idx_C10,:)
  ! exp_data(idx_C15,:)   = exp_data(idx_C15,:)
  ! exp_data(idx_C20,:)   = exp_data(idx_C20,:)
  ! exp_data(idx_C20lo,:) = exp_data(idx_C20lo,:)
  ! exp_data(idx_C20hi,:) = exp_data(idx_C20hi,:)
#endif

    print *,'moHOM_read_exp_data(): done'

  end subroutine moHOM_read_exp_data


! maintain experiment conditions (outside CAABA's integration)
  subroutine moHOM_main_exp_cond(l_reset, l_dilute, exp_no)

    implicit none

    logical, intent(in), optional :: l_reset, &  !> flag to reset the box
                                     l_dilute    !> flag to perform dilution
    integer, intent(in), optional :: exp_no      !> experiment no.
    real8                         :: rh
    integer                       :: en

  ! experiment no.
    en = 1; if (present(exp_no)) en = exp_no

  ! setup boxmodel according to the experiment conditions
    if ( present(l_reset) ) then; if ( l_reset ) then

    ! adjust pressure/temperature, cair according to exp.

#ifdef moHOM_oz
    ! permanent conditions [acp-18-845-2018]
      temp = 278.15
        rh = 0.38     ! humidified (38%)
#endif

#ifdef moHOM_dark
    ! experiment-wise
      temp = exp_data(idx_T,en)
        rh = exp_data(idx_rh,en)
#endif

    ! atmosphere
      press = 970e2 + 5e3 ! 970 mbar @Geneva + 5mbar overpressure [SE]
      cair = (N_A/1e6)*press/(R_gas*temp)
    ! synthetic air
      C_synair(:)       = 0._dp
      C_synair(ind_O2)  = 21e-2 * cair
      C_synair(ind_N2)  = 79e-2 * cair
    ! humidity
      C_synair(ind_H2O) = cair * rh * psat_mk(temp) / press

    ! chamber air dilution rate [1/s]
     !kdil = 1./(3.*60.*60.)  ! = 9.26e-5 <= 3h residence time [acp-18-2363-2018]
      kdil = 9.6e-5           ! [acp-18-845-2018]

    ! reinitialise all species' abundances
      C(:) = C_synair(:)

    ! TODO: photolysis

    endif; endif

  ! chamber dilution with syn. air
    if (present(l_dilute)) then
      if (l_dilute) C(:) = C(:) + (C_synair(:)-C(:)) * kdil * timesteplen
    endif

  ! set alpha-pinene / isoprene emission rates [mlc/cm3/s]
    if (ind_APEM /= 0) C(ind_APEM) = APem * ppt * cair  ! APem is in [ppt/s]
    if (ind_IPEM /= 0) C(ind_IPEM) = IPem * ppt * cair  ! IPem is in [ppt/s]

  ! experiment-wise maintained conditions
#ifdef moHOM_oz
  ! a-pinene ozonolysis -> sCI and HOM production experiments [acp-18-2363-2018]
  ! keep O3, H2, SO2 at desired levels
    if (ind_O3  /= 0) C(ind_O3)  = ( 22e-9 + max(0.,(22e-9-C(ind_O3)/cair)/10.) ) * cair  ! 22ppb O3
    if (ind_H2  /= 0) C(ind_H2)  = 0.1e-2 * cair  ! 0.1% H2 // was used to scavenge OH
    if (ind_SO2 /= 0) C(ind_SO2) =  70e-9 * cair  ! 70ppb SO2
#endif
#ifdef moHOM_dark
  ! new part. form. from a-pinene oxidation [acpd-2019-1058]
    if (ind_APINENE /= 0) C(ind_APINENE) = exp_data(idx_AP,en)  * cair
    if (ind_C5H8    /= 0) C(ind_C5H8)    = exp_data(idx_IP,en)  * cair
    if (ind_O3      /= 0) C(ind_O3)      = exp_data(idx_O3,en)  * cair
 !! if (ind_NO      /= 0) C(ind_NO)      = exp_data(idx_NO,en)  * cair
    if (ind_NO2     /= 0) C(ind_NO2)     = exp_data(idx_NO2,en) * cair
#endif

  ! reset loss tracers
    C(ind_LHOM)  = 0.
    C(ind_LHOMC) = 0.
    C(ind_LHOMO) = 0.
    C(ind_LHOMH) = 0.

  end subroutine moHOM_main_exp_cond
#endif /* aMC__contains */

! ----- custom [de]init --------------------------------------------------------
#ifdef    aMC__init__defs
#endif /* aMC__init__defs */
#ifdef    aMC__init__code
  ! init/read experiment data
    call moHOM_read_exp_data()

  ! synthetic air array
    call resize(C_synair,size(C))

  ! HOMs - adjust definitions here
    call resize(HOMs_inds,8)                   !>  HOM species indices
    HOMs_inds(:) = (/ ind_C10, ind_C10OOH, ind_C10OH, ind_C10dO, ind_C10O, ind_C10NO3, ind_C15, ind_C20 /)
    call resize(HOMs_C,size(HOMs_inds))  !>  C count
    HOMs_C(:)    = (/      10,         10,        10,        10,       10,         10,      15,      20 /)
    call resize(HOMs_O,size(HOMs_inds))  !>  O count
    HOMs_O(:)    = (/       4,          4,         3,         3,        3,          5,       7,       6 /)
    call resize(HOMs_H,size(HOMs_inds))  !>  H count
    HOMs_H(:)    = (/      15,         16,        16,        14,       15,         15,      24,      30 /)

  ! species being monitored for steady-state
    call resize(SS_inds,3)
    SS_inds(:) = (/ ind_C10, ind_C15, ind_C20 /) !! ind_APINENE, ind_C5H8
#ifdef DEBUG
    print *,'SS_inds(:) =',SS_inds(:)
#endif
    call resize(C_SS,(/ size(SS_inds),expQ /))
    call resize(SS_start,size(SS_inds))

  ! species to be output
    call resize(out_inds,14)
    out_inds = (/ ind_APINENE, ind_C5H8, ind_C10, ind_C15, ind_C20, ind_C10NO3, ind_OH, ind_HO2, ind_NO, ind_NO2, ind_NO3, ind_O3, ind_H2, ind_H2O /)
    call resize(C_out,(/ size(out_inds),expQ /))
#endif /* aMC__init__code */

#ifdef    aMC__deinit__defs
#endif /* aMC__deinit__defs */
#ifdef    aMC__deinit__code
  ! synthetic air
    deallocate(C_synair)
  ! HOMs
    deallocate(HOMs_inds)
    deallocate(HOMs_C)
    deallocate(HOMs_O)
    deallocate(HOMs_H)
  ! species being monitored for steady-state
    deallocate(C_SS)
    deallocate(SS_inds)
    deallocate(SS_start)
  ! species output
    deallocate(C_out)
    deallocate(out_inds)
#endif /* aMC__deinit__code */

! ----- MC parameter setup -----------------------------------------------------
#ifdef    aMC__parameter_setup__defs
    string :: sn
#endif /* aMC__parameter_setup__defs */
#ifdef    aMC__parameter_setup__code
  ! MC parameters
    res = aMC_mcpar_register(aMC_kind_unif, r=yAPIoC10, range=(/         0.01,        0.99 /), info='yAPIoC10 | C/C            | @SR C_1_0 yield from @SGa@SR-pinene oxidation' )  !0.5
    res = aMC_mcpar_register(aMC_kind_unif, r=yC10oNO2, range=(/         0.77,        0.77 /), info='yC10oNO2 | C/C            | @SR NO_2 yield from C_1_0 oxidation by NO' )
    res = aMC_mcpar_register(aMC_kind_unif, r=kC10C20 , range=(/        1e-12,       1e-10 /), info='kC10C20  | (mlc/cm^3)^2/s | @SR C_1_0 to C_2_0 polymerisation rate' ) !2e-11
    res = aMC_mcpar_register(aMC_kind_unif, r=kC10C15,  range=(/        1e-12,       1e-10 /), info='kC10C15  | (mlc/cm^3)^2/s | @SR C_1_0 to C_1_5 polymerisation rate' ) !2e-11
    res = aMC_mcpar_register(aMC_kind_unif, r=kC10a  ,  range=(/ 6.7e-15*0.01, 6.7e-15*2.0 /), info='kC10a    | (mlc/cm^3)^2/s | @SR C_1_0 to alcohol rate' )  !*0.3
    res = aMC_mcpar_register(aMC_kind_unif, r=kC10c  ,  range=(/ 8.8e-13*0.01, 8.8e-13*2.0 /), info='kC10c    | (mlc/cm^3)^2/s | @SR C_1_0 to carbonyl rate' ) !*0.2
  ! n.b. wall loss can be zero, i.e. net in equilibrium with surface stuff
    res = aMC_mcpar_register(aMC_kind_unif, r=kWall  ,  range=(/           0.,      1.2e-3 /), info='kWall    | mlc/cm^3/s     | wall loss rate' )
#ifdef moHOM_oz
    res = aMC_mcpar_register(aMC_kind_unif, r=APem, range=(/        0.1,         1.0 /), info='APem     | ppt/s          | @SGa@SR-pinene emission rate' )
#endif
#ifdef moHOM_dark
!!  res = aMC_mcpar_register(aMC_kind_unif, r=APem, range=(/        0.1,         1.0 /), info='APem     | ppt/s          | @SGa@SR-pinene emission rate' )
!!  res = aMC_mcpar_register(aMC_kind_unif, r=IPem, range=(/        0.1,       150.0 /), info='IPem     | ppt/s          | isoprene emission rate' )
#endif
  ! output
  ! derived parameters -> sample-dependent, need update (array support, ar= ai= ) of the output facility
    res = aMC_output_add('eqt      | s     | equilibration time',  ra=eqt)
!!  res = aMC_output_add('AP       | ppt   | @SGa@SR-pinene',      r=AP, sf=1e12_dp )
!!  res = aMC_output_add('IP       | ppt   | isoprene',            r=IP, sf=1e12_dp )
    res = aMC_output_add('HOMs     | ppt   | HOMs mixing ratio',   ra=HOMs, sf=1e12_dp )
    res = aMC_output_add('HOMs_rOC | O:C   | HOMs O:C ratio',      ra=HOMs_rOC)
    res = aMC_output_add('HOMs_rHC | H:C   | HOMs H:C ratio',      ra=HOMs_rHC)
    res = aMC_output_add('lr       | ppt/s | HOMs loss rate',      ra=HOMs_lr, sf=1e12_dp )
    res = aMC_output_add('lr_rOC   | O:C   | HOMs loss O:C ratio', ra=HOMs_lr_rOC)
    res = aMC_output_add('lr_rHC   | H:C   | HOMs loss H:C ratio', ra=HOMs_lr_rHC)
!  ! species from CAABA
     do ci = 1, ubound(C_out,dim=1)
       sn = trim(SPC_NAMES(out_inds(ci)))
       res = aMC_output_add(sn//' | mol/mol | '//sn//' mixing ratio', ra=C_out(ci,:))
     enddo
  ! hit sets/cases
    moHOM_set_eq     = aMC_hit_set_add('eq   | successful equilibrations'  , fill=.false., out_single=.true., out_common=.false.)
    moHOM_set_neq    = aMC_hit_set_add('neq  | unsuccessful equilibrations', fill=.false., out_single=.false.)
#ifdef moHOM_oz
    moHOM_set_AP     = aMC_hit_set_add('AP   | fitting alpha-pinene MRs'   , fill=.false., out_single=.false.)
    moHOM_set_HOMs   = aMC_hit_set_add('HOMs | fitting HOMs MRs'           , fill=.false., out_single=.false.)
#endif
#ifdef moHOM_dark
!!  moHOM_set_AP     = aMC_hit_set_add('AP   | fitting alpha-pinene MRs'   , fill=.false., out_single=.false.)
!!  moHOM_set_IP     = aMC_hit_set_add('IP   | fitting isoprene MRs'       , fill=.false., out_single=.false.)
    moHOM_set_C10    = aMC_hit_set_add('C10  | fitting C10 MRs'            , fill=.false., out_single=.false.)
    moHOM_set_C15    = aMC_hit_set_add('C15  | fitting C15 MRs'            , fill=.false., out_single=.false.)
    moHOM_set_C20    = aMC_hit_set_add('C20  | fitting C20 MRs'            , fill=.false., out_single=.false.)
!!  moHOM_set_APIP   = aMC_hit_set_add('APIP | fitting alpha pinene and isoprene MRs')
!!  moHOM_set_A12    = aMC_hit_set_add('A12  | fitting AP+C10+C20')
!!  moHOM_set_AI5    = aMC_hit_set_add('AI5  | fitting AP+IP+C15')
    moHOM_set_C1020  = aMC_hit_set_add('C1020 | fitting C10+C20 MRs'                     , out_single=.false.)
    moHOM_set_C1015  = aMC_hit_set_add('C1015 | fitting C10+C15 MRs'                     , out_single=.false.)
    moHOM_set_C1520  = aMC_hit_set_add('C1520 | fitting C15+C20 MRs'                     , out_single=.false.)
#endif
    moHOM_set_fit    = aMC_hit_set_add('fit  | fit all data (rare)', fill=.false., out_single=.false.)
  ! first cases as experiments
    do ci = 1, expQ
      sn = str_field(trim(exp_runID(ci)),'|',1)  ! run ID
      moHOM_case_exp(ci) = aMC_hit_case_add(sn//' | run ID: '//sn//' @ '//str_field(trim(exp_runID(ci)),'|',2)// &  ! runID & datetime(duration)
                                                        ' at'// &  ! parameters
#ifdef moHOM_oz
                     ' AP='//num2str(r=exp_data(idx_AP,ci),fmt='(ES7.1)')  // &
                   ' HOMs='//num2str(r=exp_data(idx_HOMs,ci),fmt='(ES7.1)')   &
#endif
#ifdef moHOM_dark
                     ' T='//num2str(r=exp_data(idx_T,ci))//'K'// &
                   '  rh='//num2str(r=exp_data(idx_rh,ci)*1e2)//'%' &
#endif
                           )
    enddo
  ! then the service case
!!  moHOM_case_serv = aMC_hit_case_add('serv | service (probe/test/eq) case')
#endif /* aMC__parameter_setup__code */

! ----- new initial state ------------------------------------------------------
#ifdef    aMC__new_x0__defs
#endif /* aMC__new_x0__defs */
#ifdef    aMC__new_x0__code

  ! reset CAABA and set experiment conditions
    call moHOM_main_exp_cond(l_reset=.true.)

#ifdef DEBUG
    print *,aMC_setup//'_x0: temp='//num2str(r=temp)//' psat_mk='//num2str(r=psat_mk(temp))//' cair='//num2str(r=cair)//' => C(H2O)='//num2str(r=C(ind_H2O))
#endif

  ! reset try variables
    SS_period    = 10.*60.    !> minimum period required to conclude SS [s]
    SS_threshold = 1e-2       !> rel. change threshold (+-)
    eqt(:)       = aMC_NODATA !> shortest equilibration time
    C_SS(:,:)    = aMC_NODATA !> simulated MRs being monitored for steady-state (SS)
    SS_start(:)  = aMC_NODATA !> guessed start of SS

!!  AP           = aMC_NODATA
!!  IP           = aMC_NODATA
    HOMs(:)      = aMC_NODATA
    HOMs_rOC(:)  = aMC_NODATA
    HOMs_lr(:)   = aMC_NODATA
    HOMs_lr_rOC(:) = aMC_NODATA
    exp_no       = 1  !> reset exp. no.

#ifdef DEBUG
    print *,'moHOM_x0: tries =',aMC_tries, &
            '| APem/IPem/kWall:',APem, IPem, kWall, &
            '| yAPIoC10/yC10oNO2:',yAPIoC10, yC10oNO2, &
            '| kC10C20/kC10C15:', kC10C20, kC10C15, &
            '| kC10a/kC10c:', kC10a, kC10c
#endif
#endif /* aMC__new_x0__code */

! ----- prepare output ---------------------------------------------------------
#ifdef    aMC__output_prepare__defs
#endif /* aMC__output_prepare__defs */
#ifdef    aMC__output_prepare__code
#endif /* aMC__output_prepare__code */

! ----- find matching cases ----------------------------------------------------
#ifdef    aMC__match__defs
#endif /* aMC__match__defs */
! inside the loop over cases: case = aMC_match_case_from, aMC_match_case_upto
#ifdef    aMC__match__cases__code

    ! service case
      if ( case .eq. moHOM_case_serv ) then

      ! [not]successful equilibration
        aMC_hit_set(moHOM_set_eq)%is_hit  = aMC_shot_real
        aMC_hit_set(moHOM_set_neq)%is_hit = .not.aMC_shot_real
      ! probe statistic (all)
        aMC_hit_set(aMC_hit_set_probestat)%is_hit = .true.

      else
    ! experiments, case number is the experiment no.
        exp_no = case

#ifdef moHOM_dark
      ! [not]successful equilibration
        aMC_hit_set(moHOM_set_eq)%is_hit  = ( eqt(exp_no) .gt. 0. )
        aMC_hit_set(moHOM_set_neq)%is_hit = ( eqt(exp_no) .le. 0. )  ! not equilibrated cases set negative eqt
#endif

      ! partial matches, within +-1%
!!      aMC_hit_set(moHOM_set_AP)%is_hit   = dabs(            AP/exp_data(idx_AP,exp_no)-1.   ) .lt. 1e-2
!!      aMC_hit_set(moHOM_set_IP)%is_hit   = dabs(            IP/exp_data(idx_IP,exp_no)-1.   ) .lt. 1e-2
!!      aMC_hit_set(moHOM_set_HOMs)%is_hit = dabs(         HOMs(exp_no)/exp_data(idx_HOMs,exp_no)-1. ) .lt. 1e-2
        aMC_hit_set(moHOM_set_C10)%is_hit  = dabs( C_SS(1,exp_no)/exp_data(idx_C10,exp_no)-1.  ) .lt. 1e-2
        aMC_hit_set(moHOM_set_C15)%is_hit  = dabs( C_SS(2,exp_no)/exp_data(idx_C15,exp_no)-1.  ) .lt. 1e-2
        aMC_hit_set(moHOM_set_C20)%is_hit  =     ( C_SS(3,exp_no) .ge. exp_data(idx_C20lo,exp_no) ) .and. &
                                                 ( C_SS(3,exp_no) .le. exp_data(idx_C20hi,exp_no) )
      ! combined matches
!!      aMC_hit_set(moHOM_set_APIP)%is_hit = aMC_shot_real .and. &
!!                                           aMC_hit_set(moHOM_set_AP)%is_hit .and. aMC_hit_set(moHOM_set_IP)%is_hit
!!      aMC_hit_set(moHOM_set_A12)%is_hit  = aMC_shot_real .and. &
!!                                           aMC_hit_set(moHOM_set_AP)%is_hit  .and. &
!!                                           aMC_hit_set(moHOM_set_C10)%is_hit .and. aMC_hit_set(moHOM_set_C20)%is_hit
!!      aMC_hit_set(moHOM_set_AI5)%is_hit  = aMC_shot_real .and. &
!!                                           aMC_hit_set(moHOM_set_APIP)%is_hit .and. aMC_hit_set(moHOM_set_C15)%is_hit
!!      aMC_hit_set(moHOM_set_fit)%is_hit  = aMC_hit_set(moHOM_set_A12)%is_hit .and. aMC_hit_set(moHOM_set_AI5)%is_hit

        aMC_hit_set(moHOM_set_C1020)%is_hit = aMC_hit_set(moHOM_set_eq)%is_hit  .and. &
                                              aMC_hit_set(moHOM_set_C10)%is_hit .and. &
                                              aMC_hit_set(moHOM_set_C20)%is_hit
        aMC_hit_set(moHOM_set_C1015)%is_hit = aMC_hit_set(moHOM_set_eq)%is_hit  .and. &
                                              aMC_hit_set(moHOM_set_C10)%is_hit .and. &
                                              aMC_hit_set(moHOM_set_C15)%is_hit
        aMC_hit_set(moHOM_set_C1520)%is_hit = aMC_hit_set(moHOM_set_eq)%is_hit  .and. &
                                              aMC_hit_set(moHOM_set_C15)%is_hit .and. &
                                              aMC_hit_set(moHOM_set_C20)%is_hit
        aMC_hit_set(moHOM_set_fit)%is_hit   = aMC_hit_set(moHOM_set_C1020)%is_hit .and. &
                                              aMC_hit_set(moHOM_set_C1015)%is_hit
      endif
#endif /* aMC__match__cases__code */

! ----- aMC_physc --------------------------------------------------------------
#ifdef    aMC__physc__defs
#endif /* aMC__physc__defs */
#ifdef    aMC__physc__code

    ! adjust conditions for next experiment
      if ( model_time.le.(model_start+timesteplen) ) then
        call moHOM_main_exp_cond(l_reset=.true., exp_no=exp_no)
        C_SS(:,exp_no) = aMC_NODATA
        SS_start(:) = model_time
#ifdef DEBUG
        print *,'moHOM_physc(): exp_no =',exp_no,' start'
#endif
      endif

    ! check for steady-state conditions
    ! storing previous value, if there's none
      where (C_SS(:,exp_no).le.0.)  !
        C_SS(:,exp_no) = C(SS_inds(:))
        SS_start(:) = model_time
      endwhere
    ! reset value if there's change > threshold
      where ( (C_SS(:,exp_no).gt.0.) .and. (dabs( C(SS_inds(:))/C_SS(:,exp_no)-1. ).gt.SS_threshold) )
        C_SS(:,exp_no) = C(SS_inds(:))
        SS_start(:) = model_time
      endwhere

    ! evaluating shortest guessed SS period
      eqt(exp_no) = minval(model_time-SS_start(:))

    ! check if curr. exp. simulation is done/successful
      aMC_shot_real = eqt(exp_no).gt.SS_period
      aMC_shot_done = aMC_shot_real.or.(model_time>model_end)  ! using max. sim. length from CAABA

    ! continue simulation
      if (.not.aMC_shot_done) then
        call moHOM_main_exp_cond(l_dilute=.true., exp_no=exp_no)
      else
    ! post-process simulation results
      ! AP & HOMs
!!      AP = C(ind_APINENE)/cair
!!      IP = C(ind_C5H8)/cair
        HOMs(exp_no)        = sum(C(HOMs_inds(:)))/cair
        HOMs_rOC(exp_no)    = dot_product(C(HOMs_inds(:)),HOMs_O(:)) / &
                              dot_product(C(HOMs_inds(:)),HOMs_C(:))
        HOMs_rHC(exp_no)    = dot_product(C(HOMs_inds(:)),HOMs_H(:)) / &
                              dot_product(C(HOMs_inds(:)),HOMs_C(:))
        HOMs_lr(exp_no)     = C(ind_LHOM)/timesteplen/cair
        HOMs_lr_rOC(exp_no) = C(ind_LHOMO)/C(ind_LHOMC)
        HOMs_lr_rHC(exp_no) = C(ind_LHOMH)/C(ind_LHOMC)
      ! equilibration time since start
        eqt(exp_no) = model_time - model_start
        if ( .not.aMC_shot_done ) eqt(exp_no) = -eqt(exp_no)  ! not eq. cases have negative eq. time
      ! output species
        C_out(:,exp_no) = C(out_inds(:))/cair

#ifdef DEBUG
        _log_ 'moHOM_physc(): exp_no =',exp_no,' done, eqt = ',eqt(exp_no)
#endif

#ifdef moHOM_dark
      ! all experiments done
        if (exp_no.eq.expQ) then
        ! signal aMC to search for hits
          aMC_shot_done = .true.
          aMC_shot_real = .true.
        else
        ! continue with the next exp.
          aMC_shot_done = .false.
          aMC_shot_real = .false.
          exp_no = exp_no + 1
          model_time = model_start
        endif
#endif
      endif

#ifdef DEBUG
      if (.not.aMC_shot_done) then
        _log_ 'moHOM_physc(): exp_no =',exp_no,'SS_MRs =',C_SS(:,exp_no),', eq.time =',eqt(exp_no)
      endif
#endif
#endif /* aMC__physc__code */
! ----- aMC_physc:check_req_hits -----
! inside the loop over cases: cn = aMC_match_case_from, aMC_match_case_upto
#ifdef    aMC__physc__check_req_hits__code
        ! get req no. of equilibrated runs
!         req_hits_found4all = aMC_stats(aMC_stats_reals,00).gt.aMC_req_hits

        ! stop when hit statistic is complete for all sets (except neq)
          do sn = aMC_match_set_from, aMC_match_set_upto
            if ( aMC_hits(cn,sn,00).lt.aMC_req_hits ) then
              if ( sn .ne. moHOM_set_neq ) then ! exclude non-euilibrated integrations
                req_hits_found4all = .false.
                exit ! do-loop
              endif
            endif
          enddo
#endif /* aMC__physc__check_req_hits__code */

! ----- info output ------------------------------------------------------------
#ifdef    aMC__info__code
#endif /* aMC__info__code */

! --- XXX ---
#ifdef    aMC__XXX__defs
#endif /* aMC__XXX__defs */
#ifdef    aMC__XXX__code
#endif /* aMC__XXX__code */
