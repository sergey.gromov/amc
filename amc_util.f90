! -*- f90 -*-

! mz_sg_20190728(@tonnerre)

! this unit contains netcdf output routines for aMC,
! based on the similar unit developed for CAABA (R. Sander)
! with modifications for aMC output

! this unit contains shared types/data/routines
! and utilities for aMC

!*****************************************************************************


#include "amc.inc"

module aMC_util

  use messy_main_constants_mem, only: mmcm_sp=>sp, dp, i4, mmcm_i8=>i8, FLAGGED_BAD
#ifdef __INTEL_COMPILER
  use ifport  ! for system signals handling
#endif

  implicit none
  save

! unit-shared types and variables
  character(len=*), parameter :: modstr = 'amc'                !> model id string
  character(len=*), parameter :: modver = '1.9'                !> model version
#ifdef aMC_build_rev
  character(len=*), parameter :: modrev = aMC_build_rev        !> build revision (picked up during compilation)
#endif

! no data value
  real8, parameter            :: aMC_NODATA = FLAGGED_BAD      !> used to flag bad/missing data (real)
  integer, parameter          :: aMC_NIDATA = -999999999       !> used to flag bad/missing data (integer)
  character(len=*), parameter :: aMC_NADATA = 'n/a'            !> used to flag bad/missing data (text)
! end-of-data value
  integer, parameter          :: aMC_EODATA = -42              !> flags end-of-data coming from slave

! kinds of generated randoms
  integer, parameter          :: aMC_kind_unif = 1, &          !> uniform
                                 aMC_kind_norm = 2, &          !> normally distributed
                                 aMC_kind_lognorm = 3          !> log-normally distributed (TODO)

  type t_strings; string      :: s                             !> atomic strings type
  end type t_strings                                           !> for use in arrays

  type t_aMC_mcpar_info                                        !> parameter info for automatic factor->value translation & LHS
    integer                   :: kind = 0, &                   !>   0: not initialised, 1/2:uniform/normally-distributed
                                 mcfc_no                       !>   entry no. in the aMC_mcfc_* arrays
    real8                     :: range(2) = (/ 0._dp,1._dp /)  !>   [a;b)/a±b ranges for unif./norm. distributed parameters
    real8                     :: delta = 0._dp                 !>   delta or
    integer                   :: strata = 0, stratum           !>     qty. of strata and current stratum no. for LHS
    real8, allocatable        :: LHS_mcfc(:)                   !>   stratified mcfc values on LH axis
    real8, pointer            :: r => null()                   !>   pointers to defined variables to calculate
    integer, pointer          :: i => null()
    int8, pointer             :: i8 => null()
    logical, pointer          :: l => null()
    string                    :: name, unit, caption, info     !> variable information
    logical                   :: output = .true.               !> output switch
  end type t_aMC_mcpar_info

  type t_aMC_hit_info                                          !> hit case/set info forfacilitated dynamic management
    logical                   :: is_hit, skip, fill            !> flags for hit checking, skipping, required statistic fill-up
    logical                   :: out_common, out_single        !> switches for common set / single case output
    integer, pointer          :: hit_delta                     !> pointer to the entry in hits_delta(:,:) array
    string                    :: name, info                    !> optional information
  end type t_aMC_hit_info

  type t_aMC_output_info                                       !> info for facilitated output management
    string                    :: name, unit, caption, info     !> variable information
    real8, pointer            :: r => null(), ra(:) => null()  !> pointers to output variables (if arrays, following cases)
    integer, pointer          :: i => null(), ia(:) => null()
    int8, pointer             :: i8 => null(), i8a(:) => null()
    logical, pointer          :: l => null(), la(:) => null()
    real8, pointer            :: sf => null()                  !> variable scaling factor
  end type t_aMC_output_info

! resizable arrays
  interface resize
    module procedure resize_1d_i
    module procedure resize_1d_i8
    module procedure resize_1d_r
    module procedure resize_1d_r8

    module procedure resize_1d_t_aMC_mcpar_info
    module procedure resize_1d_t_aMC_hit_info
    module procedure resize_1d_t_aMC_output_info

    module procedure resize_2d_i
    module procedure resize_2d_i8
    module procedure resize_2d_r
    module procedure resize_2d_r8
  end interface

! system-related enhancements
  integer                     :: aMC_mstat, aMC_iostat  !> memory/io operation check flags
  int8                        :: aMC_mem = 0            !> memory used
  integer                     :: aMC_signal = 0         !> equals non-zero signal no. (if received)

! system signals trapping (implementation differs btw. gcc & intel (also xcf))
#ifdef __GFORTRAN__
  integer, parameter :: SIGINT = 2, SIGQUIT = 3, SIGTERM = 15
  external aMC_trap_signal
  intrinsic signal
#endif
#ifdef __INTEL_COMPILER
  integer, parameter :: SIGQUIT = 3
  interface
    function aMC_trap_signal(signal_no)
     !DIR$ ATTRIBUTES DEFAULT :: aMC_trap_signal
      integer(4) aMC_trap_signal
      integer(4) signal_no
    end function
  end interface
#endif


contains


! - check for runtime memory/io/etc. errors -----------------------------------
    subroutine aMC_err_check(mstat, istat)
      implicit none
      integer, intent(in), optional :: mstat, istat
      if (present(mstat)) then
        if (mstat.ne.0) then
          print *,'aMC: memory allocation error, mstat =',aMC_mstat
          stop 80
        endif
      endif
      if (present(istat)) then
        if (istat.ne.0) then
          print *,'aMC: i/o error, istat =',aMC_iostat
          stop 10
        endif
      endif
    end subroutine aMC_err_check


! - install signal trap handlers -----------------------------------------------
    subroutine aMC_install_signal_handler()
      logical :: ii = .false.
#ifdef __GFORTRAN__
      call signal(SIGINT,aMC_trap_signal)
      call signal(SIGQUIT,aMC_trap_signal)
      ii = .true.
#endif
#ifdef __INTEL_COMPILER
      integer(4) :: ierr
      ierr = signal(SIGINT,aMC_trap_signal,-1)
      ierr = signal(SIGQUIT,aMC_trap_signal,-1)
      ii = .true.
#endif
#ifdef DEBUG
      if (ii) _log_ 'aMC_install_signal_handler(): installed'
#endif
    end subroutine aMC_install_signal_handler


! - find next free i/o unit ----------------------------------------------------
    function aMC_iou_find() result(iou)
      use messy_main_tools, only: find_next_free_unit
      implicit none
      integer :: iou
      iou = -1; iou = find_next_free_unit(100,200)
      if ( iou.eq.-1 ) call aMC_err_check(istat=iou)
#ifdef DEEPDEBUG
      print *,'aMC_iou_find() =',iou
#endif
    end function aMC_iou_find


! - resize(): dynamic arrays resizing ------------------------------------------
#define RESIZE_1D_BODY(atype) \
      implicit none; \
      type(atype), intent(inout), allocatable :: arr(:); \
      integer, intent(in)                     :: ub; \
      integer, intent(in), optional           :: lb; \
      type(atype), intent(in), optional       :: init; \
      type(atype),                allocatable :: temp(:); \
      integer :: aub, alb, nlb, cub, clb; \
      nlb = 1; if (present(lb)) nlb = lb; \
      if (present(init)) then; \
        if (allocated(arr)) deallocate(arr); \
        allocate(arr(nlb:ub)); \
        arr(nlb:ub) = init; return; \
      endif; \
      if (allocated(arr)) then; \
        aub = ubound(arr,dim=1); alb = lbound(arr,dim=1); \
        cub = min(ub,aub); clb = max(nlb,alb); \
        allocate(temp(clb:cub)); temp(clb:cub) = arr(clb:cub); \
        deallocate(arr); allocate(arr(nlb:ub)); \
        arr(clb:cub) = temp(clb:cub); deallocate(temp); \
      else; \
        allocate(arr(nlb:ub)); \
      endif

    subroutine resize_1d_i(arr,ub,lb,init)
      RESIZE_1D_BODY(integer)
    end subroutine resize_1d_i
    subroutine resize_1d_i8(arr,ub,lb,init)
      RESIZE_1D_BODY(int8)
    end subroutine resize_1d_i8
    subroutine resize_1d_r(arr,ub,lb,init)
      RESIZE_1D_BODY(real)
    end subroutine resize_1d_r
    subroutine resize_1d_r8(arr,ub,lb,init)
      RESIZE_1D_BODY(real8)
    end subroutine resize_1d_r8

    subroutine resize_1d_t_aMC_mcpar_info(arr,ub,lb,init)
      RESIZE_1D_BODY(t_aMC_mcpar_info)
    end subroutine resize_1d_t_amc_mcpar_info
    subroutine resize_1d_t_aMC_hit_info(arr,ub,lb,init)
      RESIZE_1D_BODY(t_aMC_hit_info)
    end subroutine resize_1d_t_amc_hit_info
    subroutine resize_1d_t_aMC_output_info(arr,ub,lb,init)
      RESIZE_1D_BODY(t_aMC_output_info)
    end subroutine resize_1d_t_amc_output_info

#define RESIZE_2D_BODY(atype) \
      implicit none; \
      type(atype), intent(inout), allocatable :: arr(:,:); \
      integer, intent(in)                     :: ub(1:2); \
      integer, intent(in), optional           :: lb(1:2); \
      type(atype), intent(in), optional       :: init; \
      type(atype),                allocatable :: temp(:,:); \
      integer, dimension(1:2) :: aub, alb, nlb, cub, clb; \
      nlb(:) = 1; if (present(lb)) nlb(:) = lb(:); \
      if (present(init)) then; \
        if (allocated(arr)) deallocate(arr); \
        allocate(arr(nlb(1):ub(1),nlb(2):ub(2))); \
        arr(nlb(1):ub(1),nlb(2):ub(2)) = init; return; \
      endif; \
      if (allocated(arr)) then; \
        aub(1) = ubound(arr,dim=1); alb(1) = lbound(arr,dim=1); \
        aub(2) = ubound(arr,dim=2); alb(2) = lbound(arr,dim=2); \
        cub(1) = min(ub(1),aub(1)); clb(1) = max(nlb(1),alb(1)); \
        cub(2) = min(ub(2),aub(2)); clb(2) = max(nlb(2),alb(2)); \
        allocate(temp(clb(1):cub(1),clb(2):cub(2))); \
        temp(clb(1):cub(1),clb(2):cub(2)) = arr(clb(1):cub(1),clb(2):cub(2)); \
        deallocate(arr); allocate(arr(nlb(1):ub(1),nlb(2):ub(2))); \
        arr(clb(1):cub(1),clb(2):cub(2)) = temp(clb(1):cub(1),clb(2):cub(2)); \
        deallocate(temp); \
      else; \
        allocate(arr(nlb(1):ub(1),nlb(2):ub(2))); \
      endif;

    subroutine resize_2d_i(arr,ub,lb,init)
      RESIZE_2D_BODY(integer)
    end subroutine resize_2d_i
    subroutine resize_2d_i8(arr,ub,lb,init)
      RESIZE_2D_BODY(int8)
    end subroutine resize_2d_i8
    subroutine resize_2d_r(arr,ub,lb,init)
      RESIZE_2D_BODY(real)
    end subroutine resize_2d_r
    subroutine resize_2d_r8(arr,ub,lb,init)
      RESIZE_2D_BODY(real8)
    end subroutine resize_2d_r8


! - num2str(): number-to-string conversion -------------------------------------
    function num2str(i,r,l,i8,r4,fmt,na)
      integer,      intent(in), optional :: i
      int8,         intent(in), optional :: i8
      real8,        intent(in), optional :: r
      real,         intent(in), optional :: r4
      logical,      intent(in), optional :: l
      character(*), intent(in), optional :: fmt
      character(*), intent(in), optional :: na
      logical                            :: fnp
      character(len=256)                 :: cs
      string                             :: fmt_, na_
      string                             :: num2str
      fnp = .not.(present(fmt)); if ( .not.fnp ) fmt_ = trim(fmt);
      na_ = aMC_NADATA; if ( present(na) ) na_ = na
      cs = na_
      if ( present(i) ) then
        if ( fnp ) fmt_ = '(I0)'
        if ( i.ne.int(aMC_NIDATA,kind(i)) ) write(cs,trim(fmt_)) i
      endif
      if ( present(i8) ) then
        if ( fnp ) fmt_ = '(I0)'
        if ( i8.ne.int(aMC_NIDATA,kind(i8)) ) write(cs,trim(fmt_)) i8
      endif
      if ( present(r) ) then
        if ( fnp ) then
          if (     r.eq.real(0e+0,kind(r)) ) then; fmt_ = '(F0.0)'
          else if ( (abs(r).lt.real(1e+0,kind(r))).or. &
                    (abs(r).gt.real(1e+5,kind(r))) ) then; fmt_ = '(ES11.5)'
          else; fmt_ = '(F0.1)'; endif
        endif
        if ( r.ne.real(aMC_NODATA,kind(r)) ) write(cs,trim(fmt_)) r
      endif
      if ( present(r4) ) then
        if ( fnp ) then
          if (     r4.eq.real(0e+0,kind(r4)) ) then; fmt_ = '(F0.0)'
          else if ( (abs(r4).lt.real(1e+0,kind(r4))).or. &
                    (abs(r4).gt.real(1e+5,kind(r4))) ) then; fmt_ = '(ES11.5)'
          else; fmt_ = '(F0.1)'; endif
        endif
        if ( r4.ne.real(aMC_NODATA,kind(r4)) ) write(cs,trim(fmt_)) r4
      endif
      if ( present(l) ) then
        if ( fnp ) fmt_ = '(L1)'
        write(cs,trim(fmt_)) l
      endif
      num2str = trim(cs)
    end function num2str


! - str2num(): string-to-number conversion -------------------------------------
    subroutine str2num(s, i,i8,r,r4, na, sf)
      character(*), intent(in)            :: s   ! input string
      character(*), intent(in), optional  :: na  ! bad/no data flag
      real8,        intent(in), optional  :: sf  ! scaling factor
      integer,      intent(out), optional :: i   ! output number
      int8,         intent(out), optional :: i8
      real8,        intent(out), optional :: r
      real,         intent(out), optional :: r4
    ! check for bad/missing data/flag
      if ( present(na) ) then
        if ( s.eq.na ) then
          if ( present(i)  ) i  =  int(aMC_NIDATA,kind(i) )
          if ( present(i8) ) i8 =  int(aMC_NIDATA,kind(i8))
          if ( present(r)  ) r  = real(aMC_NODATA,kind(r) )
          if ( present(r4) ) r4 = real(aMC_NODATA,kind(r4))
          return
        endif
      endif
    ! convert
      if ( present(i)  ) read(s,*)  i
      if ( present(i8) ) read(s,*) i8
      if ( present(r)  ) read(s,*)  r
      if ( present(r4) ) read(s,*) r4
    ! scale
      if ( present(sf) ) then
        if ( present(i)  ) i  =  i*int(sf,kind(i))
        if ( present(i8) ) i8 = i8*int(sf,kind(i8))
        if ( present(r)  ) r  =  r*real(sf,kind(r))
        if ( present(r4) ) r4 = r4*real(sf,kind(r4))
      endif
    end subroutine str2num


! - check if a value is Inf or Nan ---------------------------------------------
    elemental logical function is_Inf_or_NaN(val)
      real(dp), intent(in) :: val
      is_Inf_or_NaN = (val.gt.HUGE(dp)).or.(val.ne.val)   ! by definition, NaN is not equal to anything, even itself
    end function is_Inf_or_NaN


! - check if file exists -------------------------------------------------------
    logical function file_exists(fname)
      character(*), intent(in) :: fname
      logical res
      inquire(file=fname, exist=res)
      file_exists = res
    end function file_exists


! - extract nth field from a string using given separator, trimmed by default --
    function str_field(str, fsep, fnum, notrim)
      implicit none
      intrinsic trim
      string                        :: str_field
      character(len=*),  intent(in) :: str     !> input string
      character,         intent(in) :: fsep    !> field separator
      integer,           intent(in) :: fnum    !> requested field no.
      logical, optional, intent(in) :: notrim  !> set to .true. to avoid trimming by default
      integer                       :: cn, fn
      string                        :: istr    !> working string
      logical                       :: notrim_
      if (fnum.lt.1) then
        _log_ 'aMC_util::str_field(): field no. (fnum) < 1, stop.'
        stop 176
      endif
      istr = str//fsep  ! add separator in case a string without separators is given
      notrim_ = .false.; if (present(notrim)) notrim_ = notrim
      str_field = ''; fn = 0; cn = 0
      do while ( cn.lt.len(istr) )
        cn = cn + 1
        if ( istr(cn:cn).eq.fsep ) then
          fn = fn + 1
          if ( fn.eq.fnum ) then
            if ( .not.notrim_ ) str_field = trim(adjustl(str_field))
            return
          endif
          str_field = ''
        else
          str_field = str_field//istr(cn:cn)
        endif
      enddo
    end function str_field


! - return sort indices for an array -------------------------------------------
    subroutine idxsort(array,si)
      implicit none
      real8, intent(in)              :: array(:)
      integer, allocatable, intent(out) :: si(:)
      integer :: lb, ub, ii, re, ri, rt
      logical :: ex
      lb=lbound(array,dim=1)
      ub=ubound(array,dim=1)
      call resize(si,lb=lb,ub=ub,init=int(0,kind(si)))
    ! init sort sequence
      si(lb:ub) = (/ (ii, ii=lb,ub,1) /)
    ! bubble-sort si(:) according to array(:)
      ex = .true.
      re = ub
      do while ( ex.and.(re.ge.lb+1) )
        ex = .false.
        do ri = lb,re-1
          if ( array(si(ri)) .gt. array(si(ri+1)) ) then
          ! swapping only si(:) indices, not data
            rt = si(ri)
            si(ri) = si(ri+1)
            si(ri+1) = rt
            ex = .true.
          endif
        enddo
        re = re-1
      enddo
    end subroutine idxsort


! - better randomisation of the seed -------------------------------------------
    subroutine init_random_seed()
      use iso_fortran_env, only: int64
#if defined(__INTEL_v11) || defined(__INTEL_COMPILER)
      use ifport, only: getpid
#endif
      implicit none
      integer, allocatable :: seed(:)
      integer :: i, n, un, istat, dt(8), pid
      integer(int64) :: t

      call random_seed(size = n)
      allocate(seed(n))
      ! First try if the OS provides a random number generator
      open(newunit=un, file="/dev/urandom", access="stream", &
           form="unformatted", action="read", status="old", iostat=istat)
      if (istat == 0) then
         read(un) seed
         close(un)
      else
         ! Fallback to XOR:ing the current time and pid. The PID is
         ! useful in case one launches multiple instances of the same
         ! program in parallel.
         call system_clock(t)
         if (t == 0) then
            call date_and_time(values=dt)
            t = (dt(1) - 1970) * 365_int64 * 24 * 60 * 60 * 1000 &
                 + dt(2) * 31_int64 * 24 * 60 * 60 * 1000 &
                 + dt(3) * 24_int64 * 60 * 60 * 1000 &
                 + dt(5) * 60 * 60 * 1000 &
                 + dt(6) * 60 * 1000 + dt(7) * 1000 &
                 + dt(8)
         end if
         pid = getpid()
         t = ieor(t, int(pid, kind(t)))
         do i = 1, n
            seed(i) = lcg(t)
         end do
      end if
      call random_seed(put=seed)
    contains
      ! This simple PRNG might not be good enough for real work, but is
      ! sufficient for seeding a better PRNG.
      function lcg(s)
        integer :: lcg
        integer(int64) :: s
        if (s == 0) then
           s = 104729
        else
           s = mod(s, 4294967296_int64)
        end if
        s = mod(s * 279470273_int64, 4294967291_int64)
        lcg = int(mod(s, int(huge(0), int64)), kind(0))
      end function lcg
    end subroutine init_random_seed


end module aMC_util

! - system signal handler ------------------------------------------------------
  integer(4) function aMC_trap_signal(signal_no)
    use aMC_util, only: aMC_signal, num2str
    integer(4) signal_no
   !DIR$ ATTRIBUTES DEFAULT :: aMC_trap_signal
    _log_ '>>> signal received (#'//num2str(signal_no)//')'
    aMC_signal = signal_no
    aMC_trap_signal = 1
  end
