! ----- f90 -----

! ----- kibME: kinetic integrators benchmark in MECCA --------------------------

! - parameters -----------------------------------------------------------------
#define USE_CAABA
!define kibME_int_rosenbrock_posdef_h211b_qssa

! - aMC parameters overrides ---------------------------------------------------
#define aMC_no_sim_on_master      // master only serves run control & I/O
#define aMC_output_non_real       // output also uncuccessful realisations
#undef  aMC_output_single_case    // enable output containers for single cases
#define aMC_output_common_set     // enable output containers for sets of cases
#define aMC_output_probe_stat     // output MC probe statistic (use to check for skewed input)
#define aMC_output_spare_extra_rq // skip PE output requests if the local amnt. of hits is greater than ~ max_req / (#PEs)
! - tuning/debugging -----------------------------------------------------------
!define aMC_run_single_sim        // enable to run a single try simulation
!define DEBUG                     // useful runtime info
!define DEEPDEBUG                 // a lot of runtime info

! ----- global imports ---------------------------------------------------------
#ifdef    aMC__uses
    use messy_main_constants_mem, only: OneDay, R_gas, N_A
    use messy_mecca_kpp, only: icntrl, rcntrl
#endif /* aMC__uses */

! ----- global definitions -----------------------------------------------------
#ifdef    aMC__defs
! coupling with aMC
  integer              :: kibME_set_bench,   &             !> hit sets
                          kibME_set_autonom, &
                          kibME_set_posdef,  &
                          kibME_set_qssa,    &
                          kibME_set_O3fit,   &
                          kibME_set_NOxfit,  &
                          kibME_set_OHfit,   &
                          kibME_set_Afit,    &
                          kibME_set_ierr,    &
                          kibME_set_succ
  integer              :: kibME_case_mbl,    &             !> - marine boundary layer box
                          kibME_case_ft,     &             !> - free troposphere
                          kibME_case_s20                   !> - stratosphere @20hPa
! parameters/variables in kibME
  integer              :: kibME_autonom, kibME_method, kibME_posdef, kibME_qssa    !> icntrl(:) parameters
  real8                :: kibME_facmin, kibME_facmax, kibME_facrej, kibME_facsafe  !> rcntrl(:) parameters
#ifdef kibME_int_rosenbrock_posdef_h211b_qssa
  real8                :: kibME_bb, kibME_kk, kibME_thres_tau, kibME_Iqssamax      !> for rosenbrock_posdef_h211b_qssa
#endif
  integer              :: kibME_scenario                   !> if different scenarios are probed (will be hit cases in MC)
  real8                :: kibME_timestep                   !> timestep (both if probed or not)
  integer              :: kibME_ierrf, kibME_icalls, &      !> integration error & no. of integrator calls (updated in outer loop)
                          kibME_nFC, kibME_nJC, &          !> accumulated integrator counters (ISTATUS(1-8))
                          kibME_nS, kibME_nSacc, kibME_nSrej, &
                          kibME_nLUD, kibME_nFBS, kibME_nSMD
  real8                :: kibME_aO3, kibME_aOH, kibME_aNOx !> simulated average MRs
  character(len=255)   :: kibME_oprstr                     !> used for text debug output
#endif /* aMC__defs */

! ----- custom routines --------------------------------------------------------
#ifdef    aMC__contains
#endif /* aMC__contains */

! ----- custom [de]init --------------------------------------------------------
#ifdef    aMC__init__defs
#endif /* aMC__init__defs */
#ifdef    aMC__init__code
#endif /* aMC__init__code */

#ifdef    aMC__deinit__defs
#endif /* aMC__deinit__defs */
#ifdef    aMC__deinit__code
#endif /* aMC__deinit__code */

! ----- MC parameter setup -----------------------------------------------------
#ifdef    aMC__parameter_setup__defs
#endif /* aMC__parameter_setup__defs */
#ifdef    aMC__parameter_setup__code
  ! integrator parameters - standard
  ! to rcntrl()
    res = aMC_mcpar_register(aMC_kind_unif, r=kibME_facmin,  range=(/  0.02,1.0  /), info='facmin ||minimum stepsize increase factor' )
    res = aMC_mcpar_register(aMC_kind_unif, r=kibME_facmax,  range=(/  2.0 ,50.0 /), info='facmax ||maximum stepsize increase factor' )
    res = aMC_mcpar_register(aMC_kind_unif, r=kibME_facrej,  range=(/  0.01,0.99 /), info='facrej ||rejected stepsize adj. factor'    )
    res = aMC_mcpar_register(aMC_kind_unif, r=kibME_facsafe, range=(/  0.05,0.99 /), info='facsafe||safe stepsize increase factor'    )
  ! to icntrl()
    res = aMC_mcpar_register(aMC_kind_unif, i=kibME_autonom, range=(/ 0.,1. /), info='autonom|flag|autonomos integration (0=none)' )
    res = aMC_mcpar_register(aMC_kind_unif, i=kibME_method,  range=(/ 0.,2. /), info='method |no. |integration method'             ) !5
    res = aMC_mcpar_register(aMC_kind_unif, i=kibME_posdef,  range=(/ 0.,2. /), info='posdef |flag|positive-definite (0=none)'     )
#ifdef kibME_int_rosenbrock_posdef_h211b_qssa
  ! integrator parameters - extended for rosenbrock_posdef_h211b_qssa
    res = aMC_mcpar_register(aMC_kind_unif, r=kibME_bb,      range=(/ 1.  ,20. /), info='bb       |-   |H211b step size controller (bb)' ) ! (bb=2.) advised by Söderlind, he tried 2,4,6,8
    res = aMC_mcpar_register(aMC_kind_unif, r=kibME_kk,      range=(/ 0.01,10. /), info='kk       |-   |H211b step size controller (kk)' ) ! best probing range not known yet
    res = aMC_mcpar_register(aMC_kind_unif,r=kibME_thres_tau,range=(/ 1e-4,20. /), info='thres_tau|-   |H211b threshold (tau)'           ) ! best probing range not known yet
    res = aMC_mcpar_register(aMC_kind_unif, r=kibME_Iqssamax,range=(/ 1e-3,20. /), info='Iqssamax |-   |H211b Iqssamax'                  ) ! best probing range not known yet
    res = aMC_mcpar_register(aMC_kind_unif, i=kibME_qssa,    range=(/ 0.,5. /)   , info='qssa     |flag|qssa mode (0=none)'              ) ! qssa is 0-5
#else
    res = aMC_mcpar_register(aMC_kind_unif, i=kibME_qssa,    range=(/ 0.,1. /),    info='qssa     |flag|qssa mode (0=none)')               ! qssa is 0-1
#endif

  ! scenario & timestep
    res = aMC_mcpar_register(aMC_kind_unif, i=kibME_scenario, range=(/ 1.,3. /), info='scenario | no | simulated scenario')
   !res = aMC_mcpar_register(aMC_kind_unif, r=kibME_timestep, range=(/1200.,4800./))

  ! output
    res = aMC_output_add('ierrf |flag|integration result (1=success)', i=kibME_ierrf)
    res = aMC_output_add('icalls|no.|integration calls',               i=kibME_icalls)
    res = aMC_output_add('nFC   |no.|function calls',                  i=kibME_nFC)
    res = aMC_output_add('nJC   |no.|jacobian calls',                  i=kibME_nJC)
    res = aMC_output_add('nS    |no.|total steps',                     i=kibME_nS)
    res = aMC_output_add('nSacc |no.|accepted steps',                  i=kibME_nSacc)
    res = aMC_output_add('nSrej |no.|rejected steps (except at the beginning)',i=kibME_nSrej)
    res = aMC_output_add('nLUD  |no.|LU decompositions',               i=kibME_nLUD)
    res = aMC_output_add('nFBS  |no.|forward/backward substitutions',  i=kibME_nFBS)
    res = aMC_output_add('nSMD  |no.|singular matrix decompositions',  i=kibME_nSMD)
    res = aMC_output_add('aO3   |ppb|ave. O3 mixing ratio',  r=kibME_aO3)
    res = aMC_output_add('aOH   |ppt|ave. OH mixing ratio',  r=kibME_aOH)
    res = aMC_output_add('aNOx  |ppb|ave. NOx mixing ratio', r=kibME_aNOx)

  ! hit sets/cases
    kibME_set_bench   = aMC_hit_set_add('bench   | benchmarking set, all output, scenario-wise')
    kibME_set_autonom = aMC_hit_set_add('autonom | autonomous integration')
    kibME_set_posdef  = aMC_hit_set_add('posdef  | positive-definite integration')
    kibME_set_qssa    = aMC_hit_set_add('qssa    | qssa enabled')
    kibME_set_O3fit   = aMC_hit_set_add('O3fit   | correct (+-1% vs. reference) simulated O3 MR')
    kibME_set_NOxfit  = aMC_hit_set_add('NOxfit  | correct (+-2% vs. reference) simulated NOx MR')
    kibME_set_OHfit   = aMC_hit_set_add('OHfit   | correct (+-5% vs. reference) simulated OH MR')
    kibME_set_Afit    = aMC_hit_set_add('Afit    | correct (vs. reference) simulated O3/NOx/OH MRs')
    kibME_set_ierr    = aMC_hit_set_add('ierr    | integrations ended with error')
    kibME_set_succ    = aMC_hit_set_add('succ    | successful integrations')
    kibME_case_mbl    = aMC_hit_case_add('mbl    | marine boundary layer box')
    kibME_case_ft     = aMC_hit_case_add('ft     | free troposphere box')
    kibME_case_s20    = aMC_hit_case_add('s20    | stratosphere @20hPa box')
#endif /* aMC__parameter_setup__code */

! ----- new initial state ------------------------------------------------------
#ifdef    aMC__new_x0__defs
   !use caaba_mem, only: istatus, ierrf
#endif /* aMC__new_x0__defs */
#ifdef    aMC__new_x0__code
  ! integrator parameters - basic
    icntrl((/1,3,5,6/)) = (/ kibME_autonom, kibME_method, kibME_posdef, kibME_qssa /)
    rcntrl(4:7)         = (/ kibME_facmin , kibME_facmax, kibME_facrej, kibME_facsafe /)
  ! integrator parameters - extended
#ifdef kibME_int_rosenbrock_posdef_h211b_qssa
    rcntrl(8:11) = (/ kibME_bb, kibME_kk, kibME_thres_tau, kibME_Iqssamax /)
#endif
  ! required for benchmarking / flagging
    istatus(:) = 0
    ierrf(1) = 0

  ! setup boxmodel depending on the scenario
  ! reinitialise all species' abundances
    C(:) = 0._dp
  ! adjust pressure/temperature -> cair -> species conc. via MRs from scenarios

  ! marine boundary layer @1013 hPa
    if (kibME_scenario.eq.kibME_case_mbl) then
      press = 1013e2
      temp = 298.
      cair = (N_A/1e6)*press/(R_gas*temp)
      if (ind_H2       /= 0) C(ind_H2)      =   1.E-06 * cair
      if (ind_O3       /= 0) C(ind_O3)      =  25.E-09 * cair
      if (ind_NO       /= 0) C(ind_NO)      =  10.E-12 * cair
      if (ind_NO2      /= 0) C(ind_NO2)     =  20.E-12 * cair
      if (ind_CH4      /= 0) C(ind_CH4)     =  1.8E-06 * cair
      if (ind_HCHO     /= 0) C(ind_HCHO)    = 300.E-12 * cair
      if (ind_CH3CHO   /= 0) C(ind_CH3CHO)  =  1.0E-10 * cair
      if (ind_CO       /= 0) C(ind_CO)      =  70.E-09 * cair
      if (ind_CO2      /= 0) C(ind_CO2)     = 350.E-06 * cair
      if (ind_DMS      /= 0) C(ind_DMS)     =  60.E-12 * cair
      if (ind_SO2      /= 0) C(ind_SO2)     =  90.E-12 * cair
      if (ind_CH3I     /= 0) C(ind_CH3I)    =  2.0E-12 * cair
      if (ind_H2O2     /= 0) C(ind_H2O2)    = 600.E-12 * cair
      if (ind_NH3      /= 0) C(ind_NH3)     = 200.E-12 * cair
      if (ind_HNO3     /= 0) C(ind_HNO3)    =  5.0E-12 * cair
      if (ind_HCl      /= 0) C(ind_HCl)     =  40.E-12 * cair
      if (ind_C3H7I    /= 0) C(ind_C3H7I)   =  1.0E-12 * cair
      if (ind_CH3OH    /= 0) C(ind_CH3OH)   = 300.E-12 * cair
      if (ind_C5H8     /= 0) C(ind_C5H8)    =  1.0E-09 * cair
      if (ind_Hg       /= 0) C(ind_Hg)      = 1.68E-13 * cair
    endif

  ! free troposphere @750 hPa
    if (kibME_scenario.eq.kibME_case_ft) then
      press = 750e2
      temp = 255.
      cair = (N_A/1e6)*press/(R_gas*temp)
    ! average init from CARIBIC2 trajectories 5-day back init
    ! flight: 20060706_CAN_FRA_157_EMAC
      if (ind_CH3OH       /= 0) C(ind_CH3OH)       =  7.23043E-10 * cair
      if (ind_HCHO        /= 0) C(ind_HCHO)        =  2.53104E-10 * cair
      if (ind_CO          /= 0) C(ind_CO)          =  7.29323E-08 * cair
      if (ind_HCOOH       /= 0) C(ind_HCOOH)       =  4.13365E-11 * cair
      if (ind_CH3CHO      /= 0) C(ind_CH3CHO)      =  2.00554E-11 * cair
      if (ind_CH3CO2H     /= 0) C(ind_CH3CO2H)     =  6.93619E-11 * cair
      if (ind_CH3COCH3    /= 0) C(ind_CH3COCH3)    =  3.34549E-10 * cair
      if (ind_ACETOL      /= 0) C(ind_ACETOL)      =  5.64005E-11 * cair
      if (ind_MGLYOX      /= 0) C(ind_MGLYOX)      =  2.03489E-11 * cair
      if (ind_BIACET      /= 0) C(ind_BIACET)      =  4.19846E-13 * cair
      if (ind_Br          /= 0) C(ind_Br)          =  3.38058E-14 * cair
      if (ind_Br2         /= 0) C(ind_Br2)         =  3.87407E-16 * cair
      if (ind_BrO         /= 0) C(ind_BrO)         =  2.39308E-13 * cair
      if (ind_HBr         /= 0) C(ind_HBr)         =  5.70085E-13 * cair
      if (ind_HOBr        /= 0) C(ind_HOBr)        =  2.86479E-13 * cair
!     if (ind_BrNO2       /= 0) C(ind_BrNO2)       =  0.          * cair
      if (ind_BrNO3       /= 0) C(ind_BrNO3)       =  6.53232E-13 * cair
      if (ind_BrCl        /= 0) C(ind_BrCl)        =  9.05211E-17 * cair
      if (ind_Cl          /= 0) C(ind_Cl)          =  7.12196E-17 * cair
      if (ind_Cl2         /= 0) C(ind_Cl2)         =  3.46324E-17 * cair
      if (ind_ClO         /= 0) C(ind_ClO)         =  8.18961E-14 * cair
      if (ind_HCl         /= 0) C(ind_HCl)         =  5.60562E-11 * cair
      if (ind_HOCl        /= 0) C(ind_HOCl)        =  2.5891E-13  * cair
      if (ind_Cl2O2       /= 0) C(ind_Cl2O2)       =  3.47767E-17 * cair
      if (ind_OClO        /= 0) C(ind_OClO)        =  1.33148E-16 * cair
      if (ind_ClNO3       /= 0) C(ind_ClNO3)       =  3.81274E-12 * cair
      if (ind_CCl4        /= 0) C(ind_CCl4)        =  8.9102E-11  * cair
      if (ind_CH3Cl       /= 0) C(ind_CH3Cl)       =  4.55034E-10 * cair
      if (ind_CH3CCl3     /= 0) C(ind_CH3CCl3)     =  1.50035E-11 * cair
      if (ind_CF2Cl2      /= 0) C(ind_CF2Cl2)      =  7.87179E-10 * cair
      if (ind_CFCl3       /= 0) C(ind_CFCl3)       =  2.42555E-10 * cair
      if (ind_CH2ClBr     /= 0) C(ind_CH2ClBr)     =  9.40689E-14 * cair
      if (ind_CHCl2Br     /= 0) C(ind_CHCl2Br)     =  5.51093E-14 * cair
      if (ind_CHClBr2     /= 0) C(ind_CHClBr2)     =  4.54419E-14 * cair
      if (ind_CH2Br2      /= 0) C(ind_CH2Br2)      =  1.02541E-12 * cair
      if (ind_CH3Br       /= 0) C(ind_CH3Br)       =  8.78744E-15 * cair
      if (ind_CHBr3       /= 0) C(ind_CHBr3)       =  4.8533E-13  * cair
      if (ind_CF3Br       /= 0) C(ind_CF3Br)       =  2.43175E-12 * cair
      if (ind_CF2ClBr     /= 0) C(ind_CF2ClBr)     =  4.20382E-12 * cair
      if (ind_CH4         /= 0) C(ind_CH4)         =  1.74813E-06 * cair
      if (ind_C2H6        /= 0) C(ind_C2H6)        =  5.04266E-10 * cair
      if (ind_C2H4        /= 0) C(ind_C2H4)        =  1.51334E-11 * cair
      if (ind_C3H8        /= 0) C(ind_C3H8)        =  4.6215E-11  * cair
      if (ind_C3H6        /= 0) C(ind_C3H6)        =  1.68475E-12 * cair
      if (ind_NC4H10      /= 0) C(ind_NC4H10)      =  7.76293E-11 * cair
      if (ind_MVK         /= 0) C(ind_MVK)         =  3.90326E-11 * cair
      if (ind_MEK         /= 0) C(ind_MEK)         =  3.98487E-11 * cair
      if (ind_C5H8        /= 0) C(ind_C5H8)        =  8.45986E-12 * cair
      if (ind_HgO         /= 0) C(ind_HgO)         =  3.57349E-16 * cair
      if (ind_HgBr2       /= 0) C(ind_HgBr2)       =  9.89811E-16 * cair
      if (ind_ClHgBr      /= 0) C(ind_ClHgBr)      =  1.13395E-16 * cair
      if (ind_BrHgOBr     /= 0) C(ind_BrHgOBr)     =  7.30999E-15 * cair
      if (ind_ClHgOBr     /= 0) C(ind_ClHgOBr)     =  1.1631E-15  * cair
      if (ind_HgCl        /= 0) C(ind_HgCl)        =  6.52374E-17 * cair
      if (ind_HgBr        /= 0) C(ind_HgBr)        =  7.25411E-16 * cair
      if (ind_Hg          /= 0) C(ind_Hg)          =  1.75258E-13 * cair
      if (ind_NACA        /= 0) C(ind_NACA)        =  2.85441E-12 * cair
      if (ind_MPAN        /= 0) C(ind_MPAN)        =  5.31725E-12 * cair
      if (ind_IC3H7NO3    /= 0) C(ind_IC3H7NO3)    =  8.94082E-13 * cair
      if (ind_LC4H9NO3    /= 0) C(ind_LC4H9NO3)    =  8.32727E-12 * cair
      if (ind_ISON        /= 0) C(ind_ISON)        =  9.0462E-12  * cair
      if (ind_NH3         /= 0) C(ind_NH3)         =  1.2068E-10  * cair
      if (ind_N2O         /= 0) C(ind_N2O)         =  3.16661E-07 * cair
      if (ind_NO          /= 0) C(ind_NO)          =  4.65026E-11 * cair
      if (ind_NO2         /= 0) C(ind_NO2)         =  1.25056E-10 * cair
      if (ind_NO3         /= 0) C(ind_NO3)         =  3.1458E-12  * cair
      if (ind_N2O5        /= 0) C(ind_N2O5)        =  4.37511E-12 * cair
      if (ind_HONO        /= 0) C(ind_HONO)        =  6.12167E-13 * cair
      if (ind_HNO3        /= 0) C(ind_HNO3)        =  1.33769E-10 * cair
      if (ind_HNO4        /= 0) C(ind_HNO4)        =  2.77194E-11 * cair
      if (ind_PAN         /= 0) C(ind_PAN)         =  3.1475E-10  * cair
      if (ind_NH2OH       /= 0) C(ind_NH2OH)       =  1.31453E-12 * cair
      if (ind_NHOH        /= 0) C(ind_NHOH)        =  1.00251E-11 * cair
      if (ind_HNO         /= 0) C(ind_HNO)         =  9.47677E-14 * cair
      if (ind_NH2         /= 0) C(ind_NH2)         =  8.9126E-18  * cair
      if (ind_O3P         /= 0) C(ind_O3P)         =  2.31275E-15 * cair
      if (ind_O3          /= 0) C(ind_O3)          =  1.60052E-07 * cair ! also lower strat in average
      if (ind_H2          /= 0) C(ind_H2)          =  5.35521E-07 * cair
      if (ind_OH          /= 0) C(ind_OH)          =  7.07567E-14 * cair
      if (ind_HO2         /= 0) C(ind_HO2)         =  9.74215E-13 * cair
      if (ind_H2O2        /= 0) C(ind_H2O2)        =  6.10601E-10 * cair
      if (ind_H2O         /= 0) C(ind_H2O)         =  0.0038177   * cair
      if (ind_CH3O2       /= 0) C(ind_CH3O2)       =  1.1287E-12  * cair
      if (ind_CH3OOH      /= 0) C(ind_CH3OOH)      =  2.37819E-10 * cair
      if (ind_C2H5O2      /= 0) C(ind_C2H5O2)      =  1.21222E-14 * cair
      if (ind_C2H5OOH     /= 0) C(ind_C2H5OOH)     =  9.14669E-12 * cair
      if (ind_CH3CO3      /= 0) C(ind_CH3CO3)      =  9.65657E-14 * cair
      if (ind_CH3CO3H     /= 0) C(ind_CH3CO3H)     =  5.72797E-11 * cair
      if (ind_IC3H7O2     /= 0) C(ind_IC3H7O2)     =  3.568E-15   * cair
      if (ind_IC3H7OOH    /= 0) C(ind_IC3H7OOH)    =  1.98726E-12 * cair
      if (ind_LHOC3H6O2   /= 0) C(ind_LHOC3H6O2)   =  2.6452E-14  * cair
      if (ind_LHOC3H6OOH  /= 0) C(ind_LHOC3H6OOH)  =  4.19966E-12 * cair
      if (ind_CH3COCH2O2  /= 0) C(ind_CH3COCH2O2)  =  4.96396E-15 * cair
      if (ind_HYPERACET   /= 0) C(ind_HYPERACET)   =  1.95516E-12 * cair
      if (ind_LC4H9O2     /= 0) C(ind_LC4H9O2)     =  1.79502E-14 * cair
      if (ind_LC4H9OOH    /= 0) C(ind_LC4H9OOH)    =  9.50258E-12 * cair
      if (ind_MVKO2       /= 0) C(ind_MVKO2)       =  7.3364E-14  * cair
      if (ind_MVKOOH      /= 0) C(ind_MVKOOH)      =  2.49326E-11 * cair
      if (ind_LMEKO2      /= 0) C(ind_LMEKO2)      =  6.33844E-15 * cair
      if (ind_LMEKOOH     /= 0) C(ind_LMEKOOH)     =  3.52573E-12 * cair
      if (ind_ISO2        /= 0) C(ind_ISO2)        =  2.85094E-14 * cair
      if (ind_ISOOH       /= 0) C(ind_ISOOH)       =  7.02518E-12 * cair
      if (ind_SO2         /= 0) C(ind_SO2)         =  6.63355E-11 * cair
      if (ind_H2SO4       /= 0) C(ind_H2SO4)       =  5.51045E-14 * cair
      if (ind_CH3SO3H     /= 0) C(ind_CH3SO3H)     =  6.24971E-11 * cair
      if (ind_DMS         /= 0) C(ind_DMS)         =  5.13875E-12 * cair
      if (ind_DMSO        /= 0) C(ind_DMSO)        =  7.64715E-14 * cair
      if (ind_CH3SO2      /= 0) C(ind_CH3SO2)      =  4.44594E-17 * cair
      if (ind_CH3SO3      /= 0) C(ind_CH3SO3)      =  1.33623E-14 * cair
      if (ind_CO2         /= 0) C(ind_CO2)         =  0.000382388 * cair
      if (ind_CH3I        /= 0) C(ind_CH3I)        =  4.39702E-14 * cair
    endif

  ! stratosphere @20hPa
    if (kibME_scenario.eq.kibME_case_s20) then
      press = 20e2
      temp = 235.
      cair = (N_A/1e6)*press/(R_gas*temp)
    ! from scout02 (ProSECCO simulation)
      if (ind_H        /= 0) C(ind_H)      =   1.E-12 * cair
      if (ind_OH       /= 0) C(ind_OH)     =   1.E-16 * cair
      if (ind_HO2      /= 0) C(ind_HO2)    =   1.E-15 * cair
      if (ind_N        /= 0) C(ind_N)      =   1.E-12 * cair
      if (ind_NO3      /= 0) C(ind_NO3)    =   1.E-12 * cair
      if (ind_N2O5     /= 0) C(ind_N2O5)   =   1.E-10 * cair
      if (ind_HNO4     /= 0) C(ind_HNO4)   =   1.E-10 * cair
      if (ind_CL       /= 0) C(ind_CL)     =   1.E-21 * cair
      if (ind_CLO      /= 0) C(ind_CLO)    =   1.E-15 * cair
      if (ind_HOCl     /= 0) C(ind_HOCl)   =  40.E-12 * cair
      if (ind_CL2O2    /= 0) C(ind_CL2O2)  =   1.E-13 * cair
      if (ind_CL2      /= 0) C(ind_CL2)    =   1.E-13 * cair
      if (ind_CH3O2    /= 0) C(ind_CH3O2)  =   1.E-12 * cair
      if (ind_N2O      /= 0) C(ind_N2O)    =  1.3E-07 * cair
      if (ind_CO       /= 0) C(ind_CO)     =  1.4E-08 * cair
      if (ind_CH3OOH   /= 0) C(ind_CH3OOH) =  12.E-12 * cair
      if (ind_ClNO3    /= 0) C(ind_ClNO3)  =   6.E-10 * cair
      if (ind_CFCl3    /= 0) C(ind_CFCl3)  =  1.4E-11 * cair
      if (ind_CF2Cl2   /= 0) C(ind_CF2Cl2) =   1.E-12 * cair
      if (ind_CH3CL    /= 0) C(ind_CH3CL)  =   1.E-12 * cair
      if (ind_CCL4     /= 0) C(ind_CCL4)   =   1.E-12 * cair
      if (ind_CH3CCL3  /= 0) C(ind_CH3CCL3)=   1.E-12 * cair
      if (ind_HNO3     /= 0) C(ind_HNO3)   =   5.E-09 * cair
      if (ind_H2O      /= 0) C(ind_H2O)    =   1.E-12 * cair
      if (ind_O3P      /= 0) C(ind_O3P)    =   9.E-34 * cair
      if (ind_O1D      /= 0) C(ind_O1D)    =   1.E-16 * cair
      if (ind_H2       /= 0) C(ind_H2)     =   5.E-07 * cair
      if (ind_O3       /= 0) C(ind_O3)     =   4.E-06 * cair
      if (ind_NO       /= 0) C(ind_NO)     =   1.E-24 * cair
      if (ind_NO2      /= 0) C(ind_NO2)    =   1.E-09 * cair
      if (ind_CH4      /= 0) C(ind_CH4)    =  1.8E-06 * cair
      if (ind_HCHO     /= 0) C(ind_HCHO)   =   7.E-11 * cair
      if (ind_CO       /= 0) C(ind_CO)     =  70.E-09 * cair
      if (ind_CO2      /= 0) C(ind_CO2)    = 350.E-06 * cair
      if (ind_H2O2     /= 0) C(ind_H2O2)   = 450.E-12 * cair
      if (ind_HCl      /= 0) C(ind_HCl)    = 400.E-12 * cair
     !if (ind_SO2      /= 0) C(ind_SO2)    =   0. * cair
    endif

    if ((kibME_scenario.lt.kibME_case_mbl).or.(kibME_scenario.gt.kibME_case_s20)) then
      print *,'kibME: unknown scenario (',kibME_scenario,'), stop.'
      stop 1
    endif

  ! make sure O2 and N2 are initialised always
    if (ind_O2 /= 0) C(ind_O2) = 21e-2 * cair
    if (ind_N2 /= 0) C(ind_N2) = 78e-2 * cair

  ! reset counters/MRs
  ! no of integrator calls (in outer loop)
    kibME_icalls = 0
  ! integrator accumulated stats
    kibME_nFC   = 0
    kibME_nJC   = 0
    kibME_nS    = 0
    kibME_nSacc = 0
    kibME_nSrej = 0
    kibME_nLUD  = 0
    kibME_nFBS  = 0
    kibME_nSMD  = 0
  ! accumulated species MRs (we average later)
    kibME_aO3  = 0._dp
    kibME_aOH  = 0._dp
    kibME_aNOx = 0._dp

#ifdef DEBUG
    print *,'kibME_x0: tries = ',aMC_tries,&
            '| autonom/method/posdef/qssa: ',icntrl((/1,3,5,6/)),&
            '| fac-min/max/rej/safe: ',rcntrl(4:7),&
#ifdef kibME_int_rosenbrock_posdef_h211b_qssa
            '| bb/kk/thres_tau/Iqssamax: ',rcntrl(8:11), &
#endif
            '| scenario/timestep:',kibME_scenario,kibME_timestep
#endif
#endif /* aMC__new_x0__code */

! ----- prepare output ---------------------------------------------------------
#ifdef    aMC__output_prepare__defs
#endif /* aMC__output_prepare__defs */
#ifdef    aMC__output_prepare__code
  ! negative MRs denote no averages were obtained (icalls = 0)
    kibME_aO3  = merge(kibME_aO3*1e9, aMC_NODATA,kibME_aO3.ge.0._dp )  ! in ppb
    kibME_aOH  = merge(kibME_aOH*1e12,aMC_NODATA,kibME_aOH.ge.0._dp )  ! in ppt
    kibME_aNOx = merge(kibME_aNOx*1e9,aMC_NODATA,kibME_aNOx.ge.0._dp)  ! in ppb
#ifdef DEBUG
      print *,'kibME_prepare_output: try = ',aMC_tries,&
              '| icalls:',kibME_icalls,&
              '| nFC/nJC/nS/nSacc/nLUD/nFBS/nSMD:',kibME_nFC,kibME_nJC,kibME_nS,kibME_nSacc,kibME_nSrej,kibME_nLUD,kibME_nFBS,kibME_nSMD,&
              '| aO3/aOH/aNOx:',kibME_aO3,kibME_aOH,kibME_aNOx,&
              '| wall_try/sim:',aMC_wall_try,aMC_wall_sim
#endif
#endif /* aMC__output_prepare__code */

! ----- find matching cases ----------------------------------------------------
#ifdef    aMC__match__defs
    logical   :: hit_scenario, hit_aO3

  ! values derived using rosenbrock_mz integrator
    real8, parameter :: hit_scenario_MRs_ref(1:3,1:3) = reshape( (/ & ! (case:mbl-ft-s20,species:O3-NOx-OH)
                          25.505e-09,   142.20e-09,       2688e-09, & ! O3 
                          8.2566e-12,    26.55e-12,       1354e-12, & ! NOx
                          22.555e-15,   265.80e-15,      104.7e-12  & ! OH
           /), (/3,3/) ) ! kibME_case_mbl kibME_case_ft kibME_case_s20
#endif /* aMC__match__defs */
! inside the loop over cases: case = aMC_match_case_from, aMC_match_case_upto
#ifdef    aMC__match__cases__code
    ! arrays of case hits
      hit_scenario = (kibME_scenario.eq.case)
      if ( .not. hit_scenario ) cycle   ! skipping further checks if scenario does not fit the case

      aMC_hit_set(kibME_set_bench)%is_hit   = hit_scenario
      aMC_hit_set(kibME_set_autonom)%is_hit = ( kibME_autonom.ne.0 )     !> if autonomous mode
      aMC_hit_set(kibME_set_posdef)%is_hit  = ( kibME_posdef.ne.0 )      !> if posdef is set
      aMC_hit_set(kibME_set_qssa)%is_hit    = ( kibME_qssa.ne.0 )        !> if qssa is used
    ! if O3 within +-1% of ref. value
      aMC_hit_set(kibME_set_O3fit)%is_hit   = (  abs(kibME_aO3/hit_scenario_MRs_ref(case,1)-1.) .le. 0.01*2. )
    ! if NOx within +-2% of ref. value
      aMC_hit_set(kibME_set_NOxfit)%is_hit  = ( abs(kibME_aNOx/hit_scenario_MRs_ref(case,2)-1.) .le. 0.02*2. )
    ! if OH within +-5% of ref. value
      aMC_hit_set(kibME_set_OHfit)%is_hit   = (  abs(kibME_aOH/hit_scenario_MRs_ref(case,3)-1.) .le. 0.05*2. )
    ! all MRs are within ref. values
      aMC_hit_set(kibME_set_Afit)%is_hit    = aMC_hit_set(kibME_set_O3fit)%is_hit  .and. &
                                              aMC_hit_set(kibME_set_NOxfit)%is_hit .and. &
                                              aMC_hit_set(kibME_set_OHfit)%is_hit
    ! if integration ended up successfully or with error
      aMC_hit_set(kibME_set_ierr)%is_hit    = ( kibME_ierrf.ne.1 )
      aMC_hit_set(kibME_set_succ)%is_hit    = ( kibME_ierrf.eq.1 )

    ! probe statistic, case-wise
      aMC_hit_set(aMC_hit_set_probestat)%is_hit = hit_scenario
#endif /* aMC__match__cases__code */

! ----- aMC_physc --------------------------------------------------------------
#ifdef    aMC__physc__defs
#endif /* aMC__physc__defs */
#ifdef    aMC__physc__code
    ! this part is executed every time integration is done, so we:
    ! update integration status & no. of calls
      kibME_ierrf = ierrf(1)
      kibME_icalls = kibME_icalls + 1
    !
    ! update integrator accumulated stats
      kibME_nFC   = kibME_nFC   + istatus(1)
      kibME_nJC   = kibME_nJC   + istatus(2)
      kibME_nS    = kibME_nS    + istatus(3)
      kibME_nSacc = kibME_nSacc + istatus(4)
      kibME_nSrej = kibME_nSrej + istatus(5)
      kibME_nLUD  = kibME_nLUD  + istatus(6)
      kibME_nFBS  = kibME_nFBS  + istatus(7)
      kibME_nSMD  = kibME_nSMD  + istatus(8)
    !
    ! accumulate species MRs (we average later)
      kibME_aO3  = kibME_aO3  + C(ind_O3)/cair
      kibME_aOH  = kibME_aOH  + C(ind_OH)/cair
      kibME_aNOx = kibME_aNOx + (C(ind_NO)+C(ind_NO2))/cair
    !
    ! shot is done when simulation end is reached or error occured
      aMC_shot_done = (model_time.ge.model_end).or.(kibME_ierrf.ne.1)

      if (aMC_shot_done) then
      ! all realisations set to successful => output
        aMC_shot_real = aMC_shot_done
      ! calculate average MRs => will be used in finding matching cases
        if (kibME_icalls.gt.0) then
          kibME_aO3  = kibME_aO3/kibME_icalls
          kibME_aOH  = kibME_aOH/kibME_icalls
          kibME_aNOx = kibME_aNOx/kibME_icalls
        endif
#ifdef DEBUG
        if (kibME_ierrf.ne.1) print *,'kibME_physc: try =',aMC_tries,' unsuccessful, ierrf(1) =',ierrf(1)
#endif
      endif

#ifdef DEEPDEBUG
      print *,'kibME_physc: try = ',aMC_tries,&
              '| icalls:',kibME_icalls,&
              '| nFC/nJC/nS/nSacc/nLUD/nFBS/nSMD:',kibME_nFC,kibME_nJC,kibME_nS,kibME_nSacc,kibME_nSrej,kibME_nLUD,kibME_nFBS,kibME_nSMD,&
!             '| istatus(1:8):',istatus(1:8),&
              '| aO3/aOH/aNOx:',kibME_aO3,kibME_aOH,kibME_aNOx,&
!             '| cO3/cOH/cNOx:',C(ind_O3),C(ind_OH),C(ind_NO)+C(ind_NO2),&
              '| wall_sim:',aMC_wall_sim
#endif
#endif /* aMC__physc__code */
! ----- aMC_physc:check_req_hits -----
! inside the loop over cases: cn = aMC_match_case_from, aMC_match_case_upto
#ifdef    aMC__physc__check_req_hits__code
!       ! stop when only 'bench' set statistic is at req. minimum (rest can be less)
!         if ( aMC_hits(0,aMC_set_bench,00).lt.aMC_req_hits ) req_hits_found4all = .false.
!         exit ! case loop
        ! stop when hit statistic is complete for all sets (except fm)
          do sn = aMC_match_set_from, aMC_match_set_upto
            if ( aMC_hits(cn,sn,00).lt.aMC_req_hits ) then
              if ( sn .ne. kibME_set_ierr ) req_hits_found4all = .false.  ! exclude cases with erroneous integration
              exit ! do-loop
            endif
          enddo
#endif /* aMC__physc__check_req_hits__code */

! ----- info output ------------------------------------------------------------
#ifdef    aMC__info__code
#ifdef kibME_int_rosenbrock_posdef_h211b_qssa
    _info_ '| - rosenbrock_posdef_h211b_qssa integrator'
#else
    _info_ '| - default integrator'
#endif
#endif /* aMC__info__code */

! --- XXX ---
#ifdef    aMC__XXX__defs
#endif /* aMC__XXX__defs */
#ifdef    aMC__XXX__code
#endif /* aMC__XXX__code */
