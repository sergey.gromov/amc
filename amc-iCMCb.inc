! ----- f90 -----

! ----- this is a template for model code simulated with aMC -------------------

! - iCMCb parameters -----------------------------------------------------------
#define iCMCb_frac_unif_mixing
#define iCMCb_hit_set_mix
!define iCMCb_hit_set_bio
#define iCMCb_HL_from_C2 7
#define iCMCb_LL_from_C2 15
!define iCMCb_hit_single 11
!define iCMCb_precribe_bio

! ----- global imports ---------------------------------------------------------
#ifdef    aMC__uses
#endif /* aMC__uses */

! ----- global definitions -----------------------------------------------------
#ifdef    aMC__defs
#endif /* aMC__defs */

! ----- custom routines --------------------------------------------------------
#ifdef    aMC__contains
#endif /* aMC__contains */

! ----- custom [de]init --------------------------------------------------------
#ifdef    aMC__init__defs
#endif /* aMC__init__defs */
#ifdef    aMC__init__code
#endif /* aMC__init__code */

#ifdef    aMC__deinit__defs
#endif /* aMC__deinit__defs */
#ifdef    aMC__deinit__code
#endif /* aMC__deinit__code */

! ----- MC parameter setup -----------------------------------------------------
#ifdef    aMC__parameter_setup__defs
#endif /* aMC__parameter_setup__defs */
#ifdef    aMC__parameter_setup__code
#endif /* aMC__parameter_setup__code */

! ----- new initial state ------------------------------------------------------
#ifdef    aMC__new_x0__defs
#endif /* aMC__new_x0__defs */
#ifdef    aMC__new_x0__code
#endif /* aMC__new_x0__code */

! ----- prepare output ---------------------------------------------------------
#ifdef    aMC__output_prepare__defs
    real8                :: ppm, ppb, ppt, f_advin
#endif /* aMC__output_prepare__defs */
#ifdef    aMC__output_prepare__code
#endif /* aMC__output_prepare__code */

! ----- find matching cases ----------------------------------------------------
#ifdef    aMC__match__defs
#endif /* aMC__match__defs */
#ifdef    aMC__match__cases__code
! inside the loop over cases: case = aMC_match_case_from, aMC_match_case_upto
#endif /* aMC__match__cases__code */

! ----- aMC_physc --------------------------------------------------------------
#ifdef    aMC__physc__defs
#endif /* aMC__physc__defs */
#ifdef    aMC__physc__code
#endif /* aMC__physc__code */
! ----- aMC_physc:check_req_hits -----
#ifdef    aMC__physc__check_req_hits__code
! inside the loop over cases: cn = aMC_match_case_from, aMC_match_case_upto
#endif /* aMC__physc__check_req_hits__code */

! ----- info output ------------------------------------------------------------
#ifdef    aMC__info__code
    _info_ '| '//aMC_setup//' options:'
    _info_ '| - hit set: ' &
#ifdef iCMCb_hit_set_mix
    //'mixing'
#endif
#ifdef iCMCb_hit_set_bio
    //'biosphere'
#endif
#ifdef iCMCb_frac_unif_mixing
    _info_ '| - probe uniformly distr. mixing fractions'
#endif
#ifdef iCMCb_HL_from_C2
    _info_ '| - iCMCb_HL_from_C2: '//num2str(i=iCMCb_HL_from_C2)
#endif
#ifdef iCMCb_LL_from_C2
    _info_ '| - iCMCb_LL_from_C2: '//num2str(i=iCMCb_LL_from_C2)
#endif
#ifdef iCMCb_hit_single
    _info_ '| - iCMCb_hit_single: '//num2str(i=iCMCb_hit_single)
#endif
#ifdef iCMCb_precribe_bio
    _info_ '| - iCMCb_prescribe_bio'
#endif
#endif /* aMC__info__code */

! --- XXX ---
#ifdef    aMC__XXX__defs
#endif /* aMC__XXX__defs */
#ifdef    aMC__XXX__code
#endif /* aMC__XXX__code */
