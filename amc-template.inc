! ----- f90 -----

! ----- this is a template for model code simulated with aMC -------------------

! - parameters -----------------------------------------------------------------
!define USE_CAABA                 // set if CAABA is used

! - aMC parameters overrides ---------------------------------------------------
!undef  aMC_no_sim_on_master      // master only serves run control & I/O
!undef  aMC_output_non_real       // output also uncuccessful realisations
!undef  aMC_output_single_case    // enable output containers for single cases
!undef  aMC_output_common_set     // enable output containers for sets of cases
!undef  aMC_output_probe_stat     // output MC probe statistic (use to check for skewed input)
!undef  aMC_output_spare_extra_rq // skip PE output requests if the local amnt. of hits is greater than ~ max_req / (#PEs)
! - tuning/debugging -----------------------------------------------------------
!define aMC_run_single_sim        // enable to run a single try simulation
#define DEBUG                     // useful runtime info
!define DEEPDEBUG                 // a lot of runtime info

! ----- global imports ---------------------------------------------------------
#ifdef    aMC__uses
    ! use <custom_module>
#endif /* aMC__uses */

! ----- global definitions -----------------------------------------------------
#ifdef    aMC__defs
    ! type1 :: var1
    ! type2 :: var2
#endif /* aMC__defs */

! ----- custom namelist entries ------------------------------------------------
#ifdef    aMC__nml
    ! <var1>, <var2>, &
#endif /* aMC__nml */

! ----- custom routines --------------------------------------------------------
#ifdef    aMC__contains
#endif /* aMC__contains */

! ----- custom [de]init --------------------------------------------------------
#ifdef    aMC__init__defs
#endif /* aMC__init__defs */
#ifdef    aMC__init__code
#endif /* aMC__init__code */

#ifdef    aMC__deinit__defs
#endif /* aMC__deinit__defs */
#ifdef    aMC__deinit__code
#endif /* aMC__deinit__code */

! ----- MC parameter setup -----------------------------------------------------
#ifdef    aMC__parameter_setup__defs
  ! already defined vars: res, ci
#endif /* aMC__parameter_setup__defs */
#ifdef    aMC__parameter_setup__code
#endif /* aMC__parameter_setup__code */

! ----- new initial state ------------------------------------------------------
#ifdef    aMC__new_x0__defs
#endif /* aMC__new_x0__defs */
#ifdef    aMC__new_x0__code
#endif /* aMC__new_x0__code */

! ----- prepare output ---------------------------------------------------------
#ifdef    aMC__output_prepare__defs
#endif /* aMC__output_prepare__defs */
#ifdef    aMC__output_prepare__code
#endif /* aMC__output_prepare__code */

! ----- find matching cases ----------------------------------------------------
#ifdef    aMC__match__defs
#endif /* aMC__match__defs */
    ! note: do not use cycle here
    ! hint: see the no. of hits using match(<case>,<set>)
#ifdef    aMC__match__cases__code
    ! inside the loop over cases: case = aMC_match_case_from, aMC_match_case_upto
      aMC_hit_set(aMC_hit_set_probestat)%is_hit = .true.
#endif /* aMC__match__cases__code */
#ifdef    aMC__match__sets__code
    ! inside the loop over sets: set = aMC_match_set_from, aMC_match_set_upto
#endif /* aMC__match__sets__code */

! ----- aMC_physc --------------------------------------------------------------
#ifdef    aMC__physc__defs
#endif /* aMC__physc__defs */
#ifdef    aMC__physc__code
     aMC_shot_done = .true.
     aMC_shot_real = .true.
#endif /* aMC__physc__code */
! ----- aMC_physc:check_req_hits -----
   ! inside the loop over cases: cn = aMC_match_case_from, aMC_match_case_upto
#ifdef    aMC__physc__check_req_hits__code
     req_hits_found4all = (aMC_tries.gt.aMC_max_hits)
#endif /* aMC__physc__check_req_hits__code */

! ----- info output ------------------------------------------------------------
#ifdef    aMC__info__code
    _info_ '| - this is a template for a model simulated in aMC'
#endif /* aMC__info__code */

! --- XXX ---
#ifdef    aMC__XXX__defs
#endif /* aMC__XXX__defs */
#ifdef    aMC__XXX__code
#endif /* aMC__XXX__code */
