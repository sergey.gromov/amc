! ----- f90 -----

! ----- rpcNS: road plum clemistry: N seen from satellite ----------------------

! - parameters -----------------------------------------------------------------
#define USE_CAABA                 // set if CAABA is used
!define rpcNS_probe_O3            /* set to probe O3 mixing ratios, too */
!define rpcNS_dilution            /* enable to simulate parcel dilution with bkg. air */

! - aMC parameters overrides ---------------------------------------------------
!undef  aMC_no_sim_on_master      // master only serves run control & I/O
!undef  aMC_output_non_real       // output also uncuccessful realisations
!undef  aMC_output_single_case    // enable output containers for single cases
!undef  aMC_output_common_set     // enable output containers for sets of cases
!undef  aMC_output_probe_stat     // output MC probe statistic (use to check for skewed input)
!undef  aMC_output_spare_extra_rq // skip PE output requests if the local amnt. of hits is greater than ~ max_req / (#PEs)
! - tuning/debugging -----------------------------------------------------------
#define aMC_run_single_sim        // enable to run a single try simulation
!define DEBUG                     // useful runtime info
!define DEEPDEBUG                 // a lot of runtime info

! ----- global imports ---------------------------------------------------------
#ifdef    aMC__uses
    use messy_main_constants_mem, only: OneDay !, R_gas, N_A
#endif /* aMC__uses */

! ----- global definitions -----------------------------------------------------
#ifdef    aMC__defs
! parameters/variables in rpcNS
  real8                :: iNO2, iO3, iVOCs, em_NO,       & !> initial NO2, O3 adn VOCs + NO emission term
#ifdef rpcNS_dilution
                          tdil,                          & !> plum dilution const [s]
#endif
                          dNO_1min, dNO2_1min, dO3_1min, & !> plume-induced changes to NO and NO2: after 1 minute
                          dNO_2min, dNO2_2min, dO3_2min, & !>                                            2 minutes
                          dNO_agep, dNO2_agep, dO3_agep, & !>                                            parcel age
                          dNO_ends, dNO2_ends, dO3_ends, & !>                                            end of simulation
                          dNO2_peak_time,                & !> NO2 peak time
                          dNO_peak, dNO2_peak, dO3_peak    !> MRs at dNO2 peak
  real8                :: em_start, em_dur,              & !> emission start [DOY] and duration [s]
                          agep, agep_sd,                 & !> average parcel age & SD [s]
                          rdNO2, rdNO2_sd                  !> dNO2/eNO2 obs. ratio
  real8, allocatable   :: eC(:)                            !> background air composition (incl. equilibrated NO2, O3, VOCs and NO before the plum)
  logical              :: rdNO2_in_agep_trig = .false. !> flag the value of dNO2/eNO2 was encountered within (agep+-agep_sd)
  integer              :: ind_VOCs                         !> index of the VOC representative species

! hit sets
  integer              :: rpcNS_set_rdNO2_agep,          & !> fitting diff. SCD ratio
                          rpcNS_set_rdNO2_agep_window

  real8, parameter     :: ppb = 1e-9_dp, ppt = 1e-12_dp, &
                          ippb = 1_dp/ppb, ippt = 1_dp/ppt
  real8                :: cair2ppb, cair2ppt               !> conversion factors for output

#endif /* aMC__defs */

! ----- custom routines --------------------------------------------------------
#ifdef    aMC__contains
#endif /* aMC__contains */

! ----- custom [de]init --------------------------------------------------------
#ifdef    aMC__init__defs
#endif /* aMC__init__defs */
#ifdef    aMC__init__code

  ! background "equilibrated" air
    allocate(eC(1:sizeof(C)))

  ! index for species representing VOCs
    ind_VOCs = ind_NC4H10  ! ind_C5H8

  ! plum emission start and duration
    em_start = (130.+15./24.)*OneDay  ! 10th of May, 15:00
    em_dur   = 10.                    ! [seconds]

  ! average parcel age [s], (1.2+-0.4) min
    agep    = 1.2*60.
    agep_sd = 0.4*60.

  ! dNO2/eNO2 obs. ratio, (50+-10)%
    rdNO2    = 0.5
    rdNO2_sd = 0.1

#endif /* aMC__init__code */

#ifdef    aMC__deinit__defs
#endif /* aMC__deinit__defs */
#ifdef    aMC__deinit__code
    deallocate(eC)
#endif /* aMC__deinit__code */

! ----- MC parameter setup -----------------------------------------------------
#ifdef    aMC__parameter_setup__defs
  ! already defined vars: res, ci
    string :: sr, sa
#endif /* aMC__parameter_setup__defs */
#ifdef    aMC__parameter_setup__code

  ! probed parameters
    res = aMC_mcpar_register(aMC_kind_unif, r=iNO2,  range=real((/   1.*ppt,        1000.*ppt /)), info = 'iNO2  | ppt   | initial NO2 MR', sf=ippt )
    res = aMC_mcpar_register(aMC_kind_unif, r=iVOCs, range=real((/  10.*ppt,        2000.*ppt /)), info = 'iVOCs | ppt   | initial VOCs MR', sf=ippt )
    res = aMC_mcpar_register(aMC_kind_unif, r=em_NO, range=real((/   0.1*ppt,          1.*ppb /)), info = 'em_NO | ppt/s | road NO emission rate', sf=ippt )
#ifdef rpcNS_dilution
    res = aMC_mcpar_register(aMC_kind_unif, r=tdil,  range=real((/  30.,             600.     /)), info = 'tdil  | s     | parcel dilution time bkg. air' )
#endif
#ifdef rpcNS_probe_O3
    res = aMC_mcpar_register(aMC_kind_unif, r=iO3,   range=real((/  15.*ppb,         125.*ppb /)), info = 'iO3   | ppb   | initial O3 MR (probed)', sf=ippb )
#else
  ! known parameters
    res = aMC_mcpar_register(aMC_kind_norm, r=iO3,   range=real((/ (42.+45.)/2.*ppb, 0.1*ppb /)), info = 'iO3   | ppb   | initial O3 MR (observed)', sf=ippb )
#endif

  ! extra output
  ! equilibrated MRs before the plum
    res = aMC_output_add('eO3       | ppb | O3 equil. MR before the plum',   r=eC(ind_O3),   sf=cair2ppb)
    res = aMC_output_add('eNO       | ppt | NO equil. MR before the plum',   r=eC(ind_NO),   sf=cair2ppt)
    res = aMC_output_add('eNO2      | ppt | NO2 equil. MR before the plum',  r=eC(ind_NO2),  sf=cair2ppt)
    res = aMC_output_add('eVOCs     | ppt | VOCs equil. MR before the plum', r=eC(ind_VOCs), sf=cair2ppt)
  ! plume-induced changes to NO and NO2: after 1 & 2 minute(s)
    res = aMC_output_add('dNO_1min  | ppt | plume-induced dNO after 1 min',   r=dNO_1min,  sf=cair2ppt)
    res = aMC_output_add('dNO_2min  | ppt | plume-induced dNO after 2 mins',  r=dNO_2min,  sf=cair2ppt)
    res = aMC_output_add('dNO_ends  | ppt | plume-induced dNO at sim. end',   r=dNO_ends,  sf=cair2ppt)
    res = aMC_output_add('dNO2_1min | ppt | plume-induced dNO2 after 1 min',  r=dNO2_1min, sf=cair2ppt)
    res = aMC_output_add('dNO2_2min | ppt | plume-induced dNO2 after 2 mins', r=dNO2_2min, sf=cair2ppt)
    res = aMC_output_add('dNO2_ends | ppt | plume-induced dNO2 at sim. end',  r=dNO2_ends, sf=cair2ppt)
    res = aMC_output_add('dO3_1min  | ppb | plume-induced dO3 after 1 min',   r=dO3_1min,  sf=cair2ppb)
    res = aMC_output_add('dO3_2min  | ppb | plume-induced dO3 after 2 mins',  r=dO3_2min,  sf=cair2ppb)
    res = aMC_output_add('dO3_ends  | ppb | plume-induced dO3 at sim. end',   r=dO3_ends,  sf=cair2ppb)
  ! + at dNO2 peak
    res = aMC_output_add('dNO2_peak_time|s | dNO2 peak time (since emission start)', r=dNO2_peak_time)
    res = aMC_output_add('dNO2_peak | ppt  | peak of plume-induced dNO2',     r=dNO2_peak, sf=cair2ppt)
    res = aMC_output_add('dNO_peak  | ppt  | plume-induced dNO at dNO2 peak', r=dNO_peak,  sf=cair2ppt)
    res = aMC_output_add('dO3_peak  | ppb  | plume-induced dNO at dNO2 peak', r=dO3_peak,  sf=cair2ppb)
  ! + at ave. parcel age
    res = aMC_output_add('dNO2_agep | ppt  | dNO2 at ave. parcel age',        r=dNO2_agep, sf=cair2ppt)
    res = aMC_output_add('dNO_agep  | ppt  | dNO at ave. parcel age',         r=dNO_agep,  sf=cair2ppt)
    res = aMC_output_add('dO3_agep  | ppb  | dO3 at ave. parcel age',         r=dO3_agep,  sf=cair2ppb)

  ! hit cases/sets
    res = aMC_hit_case_add(' mz | road plum NO emission simulation near Mainz')

    sr = num2str(r=rdNO2)//'+-'//num2str(r=rdNO2_sd)
    sa = num2str(r=agep)//'+-'//num2str(r=agep_sd)//'s'
    rpcNS_set_rdNO2_agep        = aMC_hit_set_add('rdNO2_agep        | conditions fitting dNO2/NO2 ('//sr//') at ave. parcel age ('//num2str(r=agep)//'s)')
    rpcNS_set_rdNO2_agep_window = aMC_hit_set_add('rdNO2_agep_window | conditions fitting dNO2/NO2 ('//sr//') within parcel age window ('//sa//')')
#endif /* aMC__parameter_setup__code */

! ----- new initial state ------------------------------------------------------
#ifdef    aMC__new_x0__defs
#endif /* aMC__new_x0__defs */
#ifdef    aMC__new_x0__code

#ifdef aMC_run_single_sim
    em_NO = 0.6e-9 ! 600 ppt/s NO emission
    iO3   = 45e-9  ! 45 ppb O3
    iNO2  = 5e-9   ! 5 ppb NOx
    iVOCs = 1e-9   ! 1 ppb VOCs
#endif

#if defined(DEBUG) || defined(aMC_run_single_sim)
    print *,'rpcNS_x0: try =',aMC_tries,&
            '| iO3/iNO2/iVOCs:',iO3/ppb,iNO2/ppt,iVOCs/ppt, &
#ifdef rpcNS_dilution
            '| tdil:',tdil, &
#endif
            '| em_NO:',em_NO/ppt
#endif

    C(:) = 0_dp

    cair2ppb = 1_dp/cair/ppb
    cair2ppt = 1_dp/cair/ppt

  ! photochem. smog parcel chemically degrading in the low troposphere
    C(ind_O2)        = 21e-2 !* cair
    C(ind_N2)        = 78e-2 !* cair
  ! probed tracers
    C(ind_NO2)       = iNO2
    C(ind_O3)        = iO3
    C(ind_VOCs)      = iVOCs
  ! rest
    C(ind_ACETOL)    =  250e-12
    C(ind_HYPERACET) = 0.74e-12 ! 4Igen
    C(ind_CH3CHO)    =  100e-12 ! 4gen
    C(ind_CH3COCH3)  =  100e-12 ! 4gen
    C(ind_MGLYOX)    =  230e-12 ! 4Igen
    C(ind_CH3OH)     =  500e-12 ! 4gen
    C(ind_CH3OOH)    =  590e-12 ! 3rdgen
    C(ind_CH3CO2H)   = 1.34E-09 ! 4Igen
    C(ind_CO)        =   80e-09 ! 4Igen  ! 200
    C(ind_DMS)       =  200e-12 ! 4gen
    C(ind_DMSO)      =   18e-12 ! 4Igen7
    C(ind_H2)        =    1e-06 ! def
    C(ind_H2O2)      =  220e-12 ! 4Igen
    C(ind_HCHO)      =  220e-12 ! 4Igen
    C(ind_HCOOH)     =    7e-12 ! 4Igen
    C(ind_HNO3)      = 0.15E-12 ! mod3
    C(ind_NH3)       =  170e-12 ! 4Igen
    C(ind_CH3CO3H)   =  300e-12 ! 4Igen
    C(ind_PAN)       =    2e-12 ! 4gen
    C(ind_SO2)       =  135e-12 ! 4Igen

  ! MRs -> concentration
    C(:) = C(:) * cair

  ! equilibrated (upwind) MRs before the plum
    eC(:) = C(:)

  ! simulated NOx enhancements
    dNO_1min  = aMC_NODATA
    dNO_2min  = aMC_NODATA
    dNO_peak  = aMC_NODATA
    dNO_agep  = aMC_NODATA
    dNO_ends  = aMC_NODATA

    dNO2_1min = aMC_NODATA
    dNO2_2min = aMC_NODATA
    dNO2_peak = aMC_NODATA
    dNO2_peak_time = aMC_NODATA
    dNO2_agep = aMC_NODATA
    dNO2_ends = aMC_NODATA

    dO3_1min  = aMC_NODATA
    dO3_2min  = aMC_NODATA
    dO3_peak  = aMC_NODATA
    dO3_agep  = aMC_NODATA
    dO3_ends  = aMC_NODATA

    rdNO2_in_agep_trig = .false.

#endif /* aMC__new_x0__code */

! ----- prepare output ---------------------------------------------------------
#ifdef    aMC__output_prepare__defs
#endif /* aMC__output_prepare__defs */
#ifdef    aMC__output_prepare__code

#endif /* aMC__output_prepare__code */

! ----- find matching cases ----------------------------------------------------
#ifdef    aMC__match__defs
#endif /* aMC__match__defs */
    ! inside the loop over cases: case = aMC_match_case_from, aMC_match_case_upto
    ! note: do not use cycle here
#ifdef    aMC__match__cases__code

    ! probe statistics
      aMC_hit_set(aMC_hit_set_probestat)%is_hit = .true.

    ! cases fitting diff. SCD ratio
      aMC_hit_set(rpcNS_set_rdNO2_agep)%is_hit = .false.
      if ( dNO2_agep /= aMC_NODATA ) then
        if ( abs( dNO2_agep/eC(ind_NO2)-rdNO2 ) .le. rdNO2_sd ) &  ! +(50+-10)% increase in NO2
          aMC_hit_set(rpcNS_set_rdNO2_agep)%is_hit = .true.
      endif

    ! with a trigger in a time window
      aMC_hit_set(rpcNS_set_rdNO2_agep_window)%is_hit = rdNO2_in_agep_trig

#endif /* aMC__match__cases__code */

! ----- aMC_physc --------------------------------------------------------------
#ifdef    aMC__physc__defs
    real8   :: tsl_prev
    integer :: cnt_aftermax = 0
    real8   :: dNO2              !> running dNO2 value
#endif /* aMC__physc__defs */
#ifdef    aMC__physc__code

  ! store background MRs before the emission
    if ( ( model_time < em_start ) .and. ( (model_time+timesteplen) >= em_start ) ) then
      eC(:) = C(:)
#ifdef DEEPDEBUG
    print *,'rpcNS_physc: O3/NO2/VOCs initial (',iO3/ppb,iNO2/ppt,iVOCs/ppt,') -> "equlibrated" (',eC(ind_O3)*cair2ppb,eC(ind_NO2)*cair2ppt,eC(ind_VOCs)*cair2ppt,') MRs'
#endif
    endif

  ! perform the emission
    if ( ( model_time >= em_start ) .and. ( model_time < (em_start+em_dur) ) ) then
      C(ind_NO)   = C(ind_NO)   + em_NO * cair * timesteplen
      C(ind_FNNO) = C(ind_FNNO) + em_NO * cair * timesteplen
#ifdef DEEPDEBUG
      print *,'rpcNS_physc: emitting ',em_NO/ppt,' ppt/s of NO for ',timesteplen,'s'
#endif
    endif

  ! actions since plum emission
    if ( model_time >= em_start ) then

#ifdef rpcNS_dilution
    ! dilution by background air
      C(:) = C(:) + (eC(:)-C(:))/tdil * timesteplen
#endif

    ! current dNO2 value
      dNO2 = C(ind_FNNO2)

    ! tracking MRs at peaking dNO2
      if ( dNO2 .gt. dNO2_peak ) then
        dNO2_peak = dNO2
        dNO2_peak_time = model_time - model_start
        dNO_peak = C(ind_FNNO)
        dO3_peak = C(ind_O3) - eC(ind_O3)
        cnt_aftermax = 0
#ifdef DEEPDEBUG
        print *,'rpcNS_physc: updated dNO2 peak/dNO/dO3 @ time:',dNO2_peak*cair2ppt,'/',dNO_peak*cair2ppt,'/',dO3_peak*cair2ppb,'@',dNO2_peak_time
#endif
      else
        cnt_aftermax = cnt_aftermax + 1
#ifdef DEBUG
        print *,'rpcNS_physc: cnt_aftermax =',cnt_aftermax
#endif
      endif

    ! tracking if the value of dNO2/eNO2 was encountered within (agep+-agep_sd)
      if ( ( eC(ind_NO2) > 0._dp ) .and. ( (dNO2*cair2ppt) >= 1. ) ) then
        if ( ( abs( model_time-(em_start+agep) ) .le. agep_sd ) .and. ( abs( dNO2/eC(ind_NO2)-rdNO2 ) .le. rdNO2_sd ) ) then
#ifdef DEBUG
          if ( .not.rdNO2_in_agep_trig ) print *,'rpcNS_physc: rdNO2_in_agep_trig triggered'
#endif
          rdNO2_in_agep_trig = .true.
        endif
      endif
    endif

  ! tracking 1min/2min/agep values
    if ( ( model_time >= em_start+60. ) .and. ( dNO_1min == aMC_NODATA ) ) then
      dO3_1min  = C(ind_O3) - eC(ind_O3)
      dNO2_1min = C(ind_FNNO2)
      dNO_1min  = C(ind_FNNO)
#ifdef DEEPDEBUG
      print *,'rpcNS_physc: dO3/dNO2/dNO @1min:',dO3_1min*cair2ppb,'/',dNO2_1min*cair2ppt,'/',dNO_1min*cair2ppt
#endif
    endif
    if ( ( model_time >= em_start+120. ) .and. ( dNO_2min == aMC_NODATA ) ) then
      dO3_2min  = C(ind_O3) - eC(ind_O3)
      dNO2_2min = C(ind_FNNO2)
      dNO_2min  = C(ind_FNNO)
#ifdef DEEPDEBUG
      print *,'rpcNS_physc: dO3/dNO2/dNO @2min:',dO3_2min*cair2ppb,'/',dNO2_2min*cair2ppt,'/',dNO_2min*cair2ppt
#endif
    endif
  ! at average parcel age
    if ( ( model_time >= em_start+agep ) .and. ( dNO_agep == aMC_NODATA ) ) then
      dO3_agep  = C(ind_O3) - eC(ind_O3)
      dNO2_agep = C(ind_FNNO2)
      dNO_agep  = C(ind_FNNO)
#ifdef DEEPDEBUG
      print *,'rpcNS_physc: dO3/dNO2/dNO @parcel age:',dO3_agep*cair2ppb,'/',dNO2_agep*cair2ppt,'/',dNO_agep*cair2ppt,'@',agep
#endif
    endif

  ! shot done:
!     .and. ( cnt_aftermax .gt. 10 ) ) then       ! and 10 steps after maximum are simulated
    if ( model_time >= (em_start+5.*60.) ) then   ! some minutes after the plum have passed
      aMC_shot_done = .true.
      aMC_shot_real = aMC_shot_done
      cnt_aftermax = 0
      dO3_ends  = C(ind_O3) - eC(ind_O3)
      dNO2_ends = C(ind_FNNO2)
      dNO_ends  = C(ind_FNNO)
#ifdef DEEPDEBUG
      print *,'rpcNS_physc: dO3/dNO2/dNO @sim. end:',dO3_ends*cair2ppb,'/',dNO2_ends*cair2ppt,'/',dNO_ends*cair2ppt
#endif
    endif

  ! speed up using variable timestep
    tsl_prev = timesteplen
    if ( ( model_time+max(timesteplen,60.) ) < em_start ) then
      if ( timesteplen .lt. 60. ) timesteplen = 60.
    else
    ! 1s within first 3 minutes after plum, then increasing gradually
      if ( model_time < (em_start+3.*60.) ) then
        timesteplen = 1.
      else
        timesteplen = timesteplen + 1.
      endif
    endif
#ifdef DEEPDEBUG
    if (timesteplen .ne. tsl_prev) print *,'rpcNS_physc: throttling timestep to [s]:',timesteplen
#endif

#endif /* aMC__physc__code */
! ----- aMC_physc:check_req_hits -----
   ! inside the loop over cases: cn = aMC_match_case_from, aMC_match_case_upto
#ifdef    aMC__physc__check_req_hits__code
!! earlier ver.
!! ! stop when benchmark statistic is at req. minimum
     if ( aMC_hits(0,rpcNS_set_rdNO2_agep_window,00).lt.aMC_req_hits ) req_hits_found4all = .false.
     exit ! case loop
!!  req_hits_found4all = (aMC_reals.gt.aMC_req_hits)
#endif /* aMC__physc__check_req_hits__code */

! ----- info output ------------------------------------------------------------
#ifdef    aMC__info__code
#ifdef rpcNS_probe_O3
    _info_ '| - O3 mixing ratios are probed'
#else
    _info_ '| - observed O3 mixing ratio is used'
#endif
#ifdef rpcNS_dilution
    _info_ '| - parcel dilution by background air'
#endif
#endif /* aMC__info__code */

! --- XXX ---
#ifdef    aMC__XXX__defs
#endif /* aMC__XXX__defs */
#ifdef    aMC__XXX__code
#endif /* aMC__XXX__code */
