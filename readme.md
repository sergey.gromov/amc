# `aMC`: The advanced HPC Monte Carlo engine

Welcome to the repository of `aMC`, the efficient high performance computing (HPC) **a**dvanced **M**onte **C**arlo (MC) simulation engine.

Originating as one of the base models in the [Modular Earth Submodel System](https://www.messy-interface.org) (MESSy), `aMC` is now a standalone MC core simulation engine with rapid setup and efficient execution with a lot of applications. Probably, it is the only HPC MC simulator written entirely in `F95`.

The main development repository hosted at the [DKRZ](https://www.dkrz.de) is being mirrored for public access via [GitLab](https://gitlab.com/sergey.gromov/amc). 
`aMC` is integrated with the [MESSy](https://gitlab.dkrz.de/MESSy/MESSy) and [CAABA-MECCA](https://gitlab.com/RolfSander/caaba-mecca) respositories via dedicated submodule'd branches.

### Main features of aMC

- Modular design with entry points for quick development and setup
- High performance & scalability (execution on HPC systems using `MPI`)
  - Single/parallel execution, restart facility (incl. random generators states)
  - Data aggregation via master / direct raw slave output with post-processing (for resource demanding data)
  - Cached send/output crucial for HPC simulations, anti-clog exchange/execution design
- Entire MC parameter space is probed within one simulation (until sufficient statistics is gathered)
  - [non-]independent harvesting (for slaves that do [not] deliver identical binary results for generated randoms)
  - Multiple hit targets (e.g. observed samples or experiment conditions)
  - Multiple sets of parameters (e.g. hitting desired combinations of parameters)
  - Integrated (hit/parameter) and highly customised output into `netCDF` (hit real-time information)
  - Output probe statistics/request nos. for checks
  - Latin Hypercube Sampling with optional thinning

See the [CHANGELOG](amc/CHANGELOG.md) for additional information.

### Current applications

* Simulations with generic models
  - `imbMC`: isotope CO transport/mixing in the remote SH @BHD/SCB
  - `pdpMD`: pore distribution from particle mobility equivant diameter measurements
  - `sTOFf`: search for time-of-flight (TOF) mass spectrometry fragments
  - `adGRF`: atmospheric dynamics & global radioactive fallout
  - `mssPF`: methane sources and sink dynamics from past to future
  - `vpCIO`: variations in present/past clumped O2 isotope composition

* Simulations with `MECCA`/`CAABA`
  - `iCMCb`: isotope CO2 in Indian BL @ `CARIBIC-2`
  - `iMMCb`: isotope CH4 in four-box palaeo atmosphere
  - `kibME`: benchmarking of kinetic integrators in `MECCA`
  - `rpcNS`: road plum chemistry of NOx seen from satellite
  - `moHOM`: mainz organics mechanism (`MOM`) experiments on HOM chemistry

# Integrating your code / building / executing

If you are using `aMC` submodule'd within the MESSy, CAABA-MECCA or other repository, make sure to update the upstream code using the following commands:

```
> git submodule init
> git submodule update --remote
> cd amc

# for MESSy
> git checkout messy/master

# for CAABA-MECCA
> git checkout caaba/master
```

`aMC` has a flexible auto-configuration facility that automatically detects environment (compiler, `MPI`, `netCDF` installation) as it is typically setup in an HPC environment (e.g. using `modules` or `lmod`). When integrated into MESSy, the root configuration of distribution is picked up.

The code of simulated models is located in `amc-*.inc` files, see the [template](amc/amc-template.inc) for an example of how to integrate your code.

In order to build a particular setup, invoke `make` with the name of the setup, e.g.:
```
# list all setups available in the distribution
> make list-setups
  rpcNS sTOFf pdpMD kibME moHOM iCMCb iMMCb

# build aMC setup kibME for benchmarking kinetic integrators in MECCA
> make kibME
```
`make`'s autocomplete should also display setups available in the distribution.

If your environment has changed (e.g. changed compiler of library version), use
```
# force redo of build configuration based on current enviroment
> make cfg-rebuild-force

# list actual build configuration
> make info
```
to renew/inspect the build configuration.

Several build options must be set at the time of compilation via conditional defines in the [core](amc/amc.inc) and setup (`amc-*.inc`) include files. Run-time execution parameters are set in the [control namelist](amc/amc.nml), see the sample in the distribution for more information.

`aMC` automatically detects a single/parallel execution environment based on the invocation, e.g.:
```
# execute a single-processor run
> ./amc-kibME.exe
# execute a parallel run
> mpirun -n 16 ./amc-kibME.exe
```
Refer to the [example job script](./xamc) for a deployment at a large HPC infrastructure (e.g. with support of save/restart and automatic queueing).

# Code usage, questions, further info, etc.

You are welcome to use `aMC`, contribute to it and/or utilise parts of its code according to the project [license](LICENSE).

Please contact [Sergey Gromov](https://gitlab.com/sergey.gromov) (using GitLab means) in case you have any questions/feedback.
