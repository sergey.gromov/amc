! mz_sg_200928 | f90
!===============================================================================
!
! aMC = advanced Monte-Carlo engine in MESSy
!
! this is the base/[sub]model-coupling/execution module, including
! - coupling with CAABA
! - generic aMC execution (i.e. no coupling other [sub]model)
!
!===============================================================================

#include "amc.inc"

program aMC_exec

  use aMC

#ifdef USE_CAABA
  use caaba_module,           only: caaba_init, caaba_result, caaba_physc, &
                                    caaba_finish
  use caaba_mem,              only: model_time, model_end, timesteplen, l_skipoutput
  use messy_main_control_cb,  only: messy_init, messy_result, messy_physc, &
                                    messy_finish
#endif
#ifndef NOMPI
  use mo_mpi,                 only: p_pe, p_io
#endif

  implicit none
  save

! I/O:
  integer :: iou = 99   ! I/O unit

! local:
  character(len=*), parameter :: substr = 'amc-generic'

  character(len=*), parameter :: statusfile = 'status.log'
  integer :: exitcode = 100

! write exit status 1 to statusfile:
  open(10, file=statusfile, status='UNKNOWN')
  write(10,'(A)') "1"
  close(10)

#ifdef USE_CAABA
! initialization:
  CALL caaba_init
  CALL messy_init
! turning off regular output
  l_skipoutput = .true.
#ifdef aMC_run_single_sim
#ifndef NOMPI
  if ( p_pe .eq. p_io ) &
#endif
    l_skipoutput = .false.
#endif
#endif /* USE_CAABA */

! init aMC - after CAABA (if we output dyn. allocated variables by reference)
  call aMC_init(aMC_seed)

#ifdef USE_CAABA
! time loop:
!!DO WHILE (model_time < model_end)
  do while(.not.aMC_physc())
    CALL caaba_physc
    CALL messy_physc
    model_time = model_time + timesteplen
#ifdef aMC_run_single_sim
    CALL caaba_result
    CALL messy_result
#endif
  end do
#else  /* ifdef USE_CAABA */
! generic loop
  do
    if ( aMC_physc() ) exit        ! .true. returned by aMC_physc denotes we have obtained what we need / need restart
  end do
#endif

! finish - before sim. model (if we output dyn. allocated variables by reference)
  call aMC_deinit(exitcode)

#ifdef USE_CAABA
! clean up:
  CALL messy_finish
  CALL caaba_finish
#endif

! finished, write exit status statusfile:
  open(10, file=statusfile, status='UNKNOWN')
  write(10,'(I0)') exitcode
  close(10)

end program aMC_exec

!===============================================================================
