
2015-2019: prior versions | development phases covering:
- concept of aMC, the advanced Monte-Carlo simulation engine in MESSy
- designed for MC simulations with various (any MESSy) core models
- multiple tries within one launch (until sufficient statistics is gathered)
- generation of any number of normally/uniformly distributed parameters
- multiple hit targets (e.g. observed samples or experiment conditions)
- multiple sets of parameters (e.g. hitting desired MRs or reactivities)
- single/parallel (using MESSy’s `mo_mpi`) execution
- master PE for run control/output, can only serve for large HPC runs
- [non-]independent randoms harvesting (for slaves that do [not]
  deliver identical binary results for generated randoms)
- direct raw slave output with post-processing (for resource demanding data)
- restart facility (incl. random generators states)
- common & hit-target-wise output of desired parameters/results
  + output to netCDF with hit realtime information
- cached send/output crucial for HPC simulations, anti-clog execution design
  + sort in time/fix time gaps for ferret
- output probe statistics/request nos. for checks

190425-29: new version ([1.7](../../tags/aMC_v1.7))
- added overall/setup/simulation wall cputime benchmarking
- fixed time-sorting/gap-adding for the output netCDFs (did not work properly with cached send/output)
- added generic aMC executable template with integrated CAABA example, aMC namelist is now separate and read within `aMC_init()`
- added l_skipoutput to MECCA for benchmarking/simulations with aMC

190505
- code cleanup/brushing + rearranged the inner loop (aMC working, restart, etc.)
- added kinetic integrator benchmarking with CAABA/MECCA (`kibME`) configuration

190717
- netCDF output corrects NaN/Inf data (some integrators spit out NaNs)
- info stream (simulation+hit statitics) is now redirectable to stderr (use `aMC_info_to_stderr` define)
- `kibME`: corrected output species average MRs, added O3fit hit case for simulations with O3 fitting reference values, improved run- and analysis scripts

190719-22
- improved Makefile(s) for build configuration pick-up from either MESSy distribution root Makefile or automatically from current environment
- fixed compilation issue when `aMC_cached_write` is not used

190727-29
- corrected integer uniformly distributed randoms generation (was ~0.5 of the expected freq. at the edges)
- added convenient handling of MC parameters (registration f-n with limits + automatic translation -> target variables)
- basic utilities moved to `amc_util.inc`, added macro for `*2str()` functions to avoid excessive use of `trim()`
- corrected harvesting scheme when master is not simulating (using effective p_pe and p_nproc)

190801-03: new version ([1.8](../../tags/aMC_v1.8))
- added Latin Hypercube Sampling (LHS) option
* new MC parameter facility for automatic handling of randoms/translation to variables (required for LHS)
- refactored set/case structure, obsolete FM (full match) set name is changed to 'service'
- some utilities moved to `aMC_util` module; all `*2str()` functions replaced with unified `num2str[f]()` macro-f-n

190819
- tuned main loop logic, corrected LHS harvesting counting
- `aMC_sync_n_output()`: added cached send flush to ensure that all obtained results are received by master

191028
+ new aMC application with CAABA - `adGRF`: atmospheric dynamics & global radioactive fallout study

200115
* dynamic (instead of hardcoded) output facility
  - extended info (caption, unit, extra info for MC parameters - type, limits, etc.) in netCDF output
- `aMC_init()`: changed order of init. statements (parallel env., info output etc. BEFORE MC parameter initialisation/output)
- refurbished `num2str()` f-n: returns now dynamic string, does not require `trim()` and `num2str[f]()` macros
- corrected import of messy_main_rnd::RND_L_verbose (use `aMC_purge_rnd_output` for MESSy distros before 2.54_dev)
- cosmetic changes to aMC & supporting units, `aMC_util` is now a unit
- `kibME`: fixed/improved parameter setup & output, tested with rosenbrock_posdef_h211b_qssa integrator

200205
* `aMC_parameter_setup()`: new common f-n for core models to set-up parameters, hit sets/cases and output
* dynamic (instead of hard-coded) hit case/set management facility
  - additional control for hit matching
  - extended info in netCDF output
+ probe statistics: rearranged output, added case-wise (or conditional) output
- cached writeout: restructured & improved info, fixed bugs

200207
- added cpp-macro custom types (int8, string) for code clarity
+ added LHS thinning option - so far based on sampling every of n harvests
- corrected (moved `aMC_print_info()`) and expanded (+MC parameters, output variables) info output

200211-15
+ added possibility of realisation case-wise calculated parameters output (`ri=`, `ra=`, etc. references in `aMC_output_add()`)
+ added build revision information (repo:tag/branch:hash[_mod])
+ new aMC application (generic) - `pdpMD`: pore distribution from particle mobility eq. diameter measurements
- netCDF output: support for v.4 (set to default in Makefile) for ease of data import (e.g. in Veusz)
- introduced slave send quantum (`aMC_send_cache_quantum_int`, earlier `aMC_output_quantum_int` was used - inconsistently)
- revived probe statistics bootstrapping (namelist var. `aMC_probestat_thinning`) for the case of output overflow
- improved statistics/output logic (capping) & reporting (`@`/`@@` for full sets when output rqs spared, `=` for full cases)

200330
- fixed erroneous statistic output for probes_stat set (PE-wise)
- further improved namespaces isolation (`KPP` vs. CAABA/MECCA vs. machine precision import from messy_main_constants_mem)
* modularised [sub]model code simulated in aMC
  - every submodel code is now in separate f90-include file, isolated with cpp blocks to be imported at relevant routines
  - simulated model is defined in `amc.inc` via `aMC_setup` conditional and automatically picked up/symlinked in make
  - [sub]model import of variables is done on module level only
  - executable is now named `amc-<aMC_setup>.exe` by default
  - clean-up of main parameter file `amc.inc`: [sub]model options moved to [sub]model files, aMC options can be also overriden there

200402
* refurbished build facility
  - aMC setups recognised by make and can be compiled separately using corresponding targets
  - build information is updated automatically or on demand ('rebuild-cfg-force' target)
  - 'info' target additionally lists available setups and aMC defines
  - automatic check for KP4-processed MECCA files
  - latest used target becomes default (via `amc-*.inc` access time)

200407
+ new aMC application (CAABA) - moHOM: mainz organics mechanism (MOM) experiments on HOM chemistry

200503: new version ([1.9](../../tags/aMC_v1.9))
- improved output & utilities
  - case-wise arrays ouput is fully supported (was not available in parallel calculations)
  - probestat samples include case no. in the output, 'hit_case' renamed to 'case'
  - memory allocation with checks + int. usage monitoring (cpp macro wrapper)
  - facilitated IO unit search function (uses MESSy generic f-n)
  - resizable 2D arrays
- fixes
  - bug: wrong addressing in `aMC_find_matching_cases()` that led to crashes when large cache send size was used
  - consistent array shape (/ probed data - reference data - tolerances /) in `aMC_check_pairs()`
- code brushing and cleanup
+ new generic simulated model: `sTOFf` - search for TOF fragments

200510-12
+ output: added single/common output switches to case/set infodata for flexible output control + optimisation @master
- fixed restart issue when overbusy master deadlocked in stats exchange, improved restart facility (added io_time_origin to save)

200926
- aMC core: `aMC_find_matching_cases()` is renamed to `aMC_match()`, added submodel code entry for sets-looping (in addition to cases)
+ new generic application - `mssPF`: methane sources and sink dynamics from past to future
+ added restart/stop trigger upon `SIGINT`/`SIGQUIT` signals received from the system
- bugfix: in multi-PE runs, hit cases may have not correctly correpond the case-variant output


upcoming:
- fixes & new developments
  - core: case-wise arrays output is fully supported (was not available in parallel mode, results sent sequentially)
  - util: debugged str<->num conversion functions, extended NODATA flags
  - build: rearranged file structure for `submodule`ing into MESSy & CAABA-MECCA repositories
  - io: added aMC_read_tab_data_colvar() for data import from multi-variable column text files
  - io: fixed crash in netCDF output sorting (likely not thread-safe `fn90_put_var()` routine)
+ new generic application - `vpCIO`: variations in present/past clumped O2 isotope composition


TODO:
- check system signal processing with GNU compiler (checked OK with Intel)
- update statistics on totals per set in `aMC_*_stats()` when common set output is switched off
- "round-robin in time" distributed cached send
- different types in netCDF according to associated output variables
- check logic when `aMC_l_raw_postprocess_only` is used with CAABA
- thinning of LHS sampling (tests)
- revive text-based output
- increase limits on common probe_stat output?
- iterative MC mode
