! - f90 -

! ----- mssPF: methane sources and sink dynamics from past to future -----------

! - parameters -----------------------------------------------------------------

! simulation mode
!define mssPF_mode_MEP            /* probing parameters together with MPI-ESM-P (MEP) simulation data */
#define mssPF_mode_CIO            /* probing trajectories for clumped isotope O2 (CIO) data from IMAU */

#ifdef mssPF_mode_MEP
! set to 1 to probe emission corr. factors (ECFs), set to 0 to prescribe sim. resp. MEP emission exactly
!define mssPF_ec_LN       1
#define mssPF_ec_ME       0
!define mssPF_ec_RN       1
!define mssPF_ec_RN_bycat         /* set to probe emissions correction factors by category */
!define mssPF_ec_RC       1
!define mssPF_ec_RC_bycat         /* set to probe emissions correction factors by category */
#define mssPF_ec_shift    0       /* set to 0 to disable emiss. corr. shift term probing */
#endif

! CH4 lifetime fit version
!define mssPF_fit_201212
!define mssPF_fit_210128
#define mssPF_fit_210219


! - aMC parameters overrides ---------------------------------------------------
!undef  aMC_no_sim_on_master      // master only serves run control & I/O
#undef  aMC_output_non_real       // output also uncuccessful realisations
!undef  aMC_output_single_case    // enable output containers for single cases
!undef  aMC_output_common_set     // enable output containers for sets of cases
!undef  aMC_output_probe_stat     // output MC probe statistic (use to check for skewed input)
!undef  aMC_output_spare_extra_rq // skip PE output requests if the local amnt. of hits is greater than ~ max_req / (#PEs)
! - tuning/debugging -----------------------------------------------------------
!define aMC_run_single_sim        // enable to run a single try simulation
!define DEBUG                     // useful runtime info
!define DEEPDEBUG                 // a lot of runtime info

! ----- global imports ---------------------------------------------------------
#ifdef aMC__uses
#endif

! ----- global definitions -----------------------------------------------------
#ifdef aMC__defs

! data
  integer              :: tsQ                 !> no of time slices
  strings              :: tsN(:)              !> time slices names
  type t_mssPF_data
    real8, allocatable :: r(:)
  end type t_mssPF_data
  type(t_mssPF_data), allocatable &
                       :: mssPF_data(:)       !> data array (idx)%r(ts#), indices:
  integer, parameter   :: idx_yr     = 1,   & !> year
                          idx_len    = 2,   & !< TS length
                          idx_B      = 3,   & !> observed CH4 burden [TgC]
                          idx_dB     = 4,   & !> +-
                          idx_ME     = 5,   & !> (GCM) CH4 emission [TgC/yr]
                          idx_LN     = 6,   & !> (GCM) lNOx emission [TgN/yr]
                          idx_RN_bb  = 7,   & !> (GCM) RN: BB emission [TgN/yr]
                          idx_RN_na  = 8,   & !>         - natural
                          idx_RN_an  = 9,   & !>         - antropogenic
                          idx_RC_bb  = 10,  & !> (GCM) RC: BB emission [TgC/yr]
                          idx_RC_na  = 11,  & !>         - natural
                          idx_RC_an  = 12,  & !>         - antropogenic
                          idx_tau    = 13,  & !> calculated: lifetime
                          idx_cMB    = 14,  & !>             burden
                          idx_cRN    = 15,  & !> + corrected RN
                          idx_cRC    = 16,  & !>             RC
                          idx_cLN    = 17,  & !>             LN
                          idx_cME    = 18,  & !>             E
                          idx_ecf_RN = 19,  & !>             RN ecf
                          idx_ecf_RC = 20     !>             RC ecf

! parameters/variables
! probed emission scaling factors
  real8                :: ecf_LN, ecs_LN, &
                          ecf_ME, ecs_ME, &
                          ecf_RN_bb, ecf_RN_na, ecf_RN_an, &
                          ecf_RC_bb, ecf_RC_na, ecf_RC_an, &
                          ecf_RN, ecs_RN, &     !> also used as diagnostic averages for bycat
                          ecf_RC, ecs_RC

! probed M, E, LN, RN, RC
  real8                :: pME, pLN, pRN, pRC
  string               :: mssPF_scheme            !> parameters probing scheme

! hit sets/cases
  integer              :: mssPF_set_hit,        & !> hit any time-slice
                          mssPF_set_hit_PI,     & !> - PI time-slices
                          mssPF_set_hit_I2PD,   & !> - industrial until PD (1990)
                          mssPF_set_hit_PD,     & !> - from PD (1990) and later
                          mssPF_set_hit_noLGM,  & !> - all except LGM
                          mssPF_set_hit_no10ka, & !> - all except 10ka
                          mssPF_set_hit_all       !> - all
  integer, allocatable :: mssPF_case_tsi(:)       !> time slice indices

  logical              :: flag_PI,     &          !> flags for matching time-slice selection
                          flag_I2PD,   &
                          flag_PD,     &
                          flag_noLGM,  &
                          flag_no10ka, &
                          flag_all

  character, parameter :: fmt_sep = _tab_       !> separator for records in input data
#endif


! ----- custom routines --------------------------------------------------------
#ifdef aMC__contains

! ----- read input data mssPF -----
  subroutine mssPF_read_data(ifname)

    use aMC_io, only: aMC_read_tab_data_colvar

    implicit none

    character(len=*), intent(in) :: ifname
    character(len=*), parameter  :: na = 'na'
    integer                      :: i, j, rc

#ifndef DEBUG
    if ( p_parallel_io ) &
#endif
      _info_ 'mssPF_read_data(): reading from '//ifname

  ! read time slice names and qty.
    tsQ = aMC_read_tab_data_colvar(ifname, 'ts', s=tsN)
#ifdef DEBUG
    _log_ '-> no of time slice name recs: '//num2str(tsQ)
#endif

  ! data array
    allocate(mssPF_data(1:idx_ecf_RC)) ! variables
    allocate(mssPF_case_tsi(1:tsQ))

  ! read data checking for records having the same length
    rc = 0
    if ( tsQ.ne.aMC_read_tab_data_colvar(ifname, 'yr',     na=na, r=mssPF_data(idx_yr)%r)   ) rc = rc + 1  !> ref. year
    if ( tsQ.ne.aMC_read_tab_data_colvar(ifname, 'len',    na=na, r=mssPF_data(idx_len)%r)  ) rc = rc + 1  !> TS length
  ! oserved CH4
    if ( tsQ.ne.aMC_read_tab_data_colvar(ifname, 'M_obs',  na=na, r=mssPF_data(idx_B)%r)    ) rc = rc + 1
    if ( tsQ.ne.aMC_read_tab_data_colvar(ifname, 'dM_obs', na=na, r=mssPF_data(idx_dB)%r)   ) rc = rc + 1
  ! simulated emissions
    if ( tsQ.ne.aMC_read_tab_data_colvar(ifname, 'ME_sim', na=na, r=mssPF_data(idx_ME)%r)   ) rc = rc + 1
    if ( tsQ.ne.aMC_read_tab_data_colvar(ifname, 'LN_sim', na=na, r=mssPF_data(idx_LN)%r)   ) rc = rc + 1
    if ( tsQ.ne.aMC_read_tab_data_colvar(ifname, 'RN_nat', na=na, r=mssPF_data(idx_RN_na)%r)) rc = rc + 1
    if ( tsQ.ne.aMC_read_tab_data_colvar(ifname, 'RC_nat', na=na, r=mssPF_data(idx_RC_na)%r)) rc = rc + 1
    if ( tsQ.ne.aMC_read_tab_data_colvar(ifname, 'RN_bb',  na=na, r=mssPF_data(idx_RN_bb)%r)) rc = rc + 1
    if ( tsQ.ne.aMC_read_tab_data_colvar(ifname, 'RC_bb',  na=na, r=mssPF_data(idx_RC_bb)%r)) rc = rc + 1
    if ( tsQ.ne.aMC_read_tab_data_colvar(ifname, 'RN_ant', na=na, r=mssPF_data(idx_RN_an)%r)) rc = rc + 1
    if ( tsQ.ne.aMC_read_tab_data_colvar(ifname, 'RC_ant', na=na, r=mssPF_data(idx_RC_an)%r)) rc = rc + 1

    if ( rc.ne.0 ) then
      _log_ '- problem, read no. of records is not equal for '//num2str(rc)//'variables, check the input. stop.'
      stop 144
    endif

  ! allocate other (not automatically allocated by read_*_data_*) data arrays
    do i = idx_tau, idx_ecf_RC
      allocate(mssPF_data(i)%r(1:tsQ))
    enddo

#ifndef DEBUG
    if ( p_parallel_io ) then
#endif
      _info_ 'mssPF_read_data(): read '//num2str(tsQ)//' time-slices'
      do i = 1, tsQ
        _log__ ' ts.#'//num2str(i)//'('//tsN(i)%s//'): '
        do j = 1, idx_tau-1
          _log__ _tab_//num2str(r=mssPF_data(j)%r(i))
        enddo
        _log_ ''
      enddo

#ifndef DEBUG
    endif ! if ( p_parallel_io )
#endif

  end subroutine mssPF_read_data


  real8 elemental function tau_fit(MB,LN,RN,RC)
    real8, intent(in) :: MB, LN, RN, RC
    real8, parameter  :: AS = 100._dp  ! aerosol surface (to be implemented as a variable)
  ! CH4 lifetime fit
#ifdef mssPF_fit_201212
  ! version 201212 (for TK's runs in end-2020)
  ! fitted against RN as NO (not the lump of all N-bearing compounds)
    real8, parameter :: &
      fit_a  =  7.41869_dp, &
      fit_p  =  0.42415_dp, &
      fit_q  = -0.64803_dp, &
      fit_kN =  0.70412_dp, &
      fit_kC =  2.38763_dp, &
      fit_kA =  0.00145_dp
  ! separate exponents for RC&M and RN
    tau_fit = 1._dp / ( fit_a * (LN+RN*fit_kN)**fit_p * (MB+RC*fit_kC*(1._dp+AS*fit_kA))**fit_q )
#endif
#ifdef mssPF_fit_210128
  ! version 210128
  ! fitted against RN as NO (not the lump of all N-bearing compounds)
    real8, parameter :: &
      fit_a  =  1.16001_dp, &
      fit_p  =  0.07663_dp, &
      fit_q  = -0.11687_dp, &
      fit_kN =  0.70419_dp, &
      fit_kC =  2.58246_dp, &
      fit_kA =  0.00150_dp, &
      fit_b  = -0.42915_dp
  ! separate exponents for RC&M and RN, base reactivity b
    tau_fit = 1._dp / ( fit_a * (LN+RN*fit_kN)**fit_p * (MB+RC*fit_kC*(1._dp+AS*fit_kA))**fit_q + fit_b )
#endif
#ifdef mssPF_fit_210219
  ! version 210219
  ! fitted against RN as NO (not the lump of all N-bearing compounds) including quadrupled CH4 experiments results
    real8, parameter :: &
      fit_a  =   7.45451_dp, &
      fit_p  =   0.36329_dp, &
      fit_q  =  -0.59839_dp, &
      fit_kN =   0.59549_dp, &
      fit_kC =   4.25408_dp, &
      fit_kA =   4.21_dp
  ! separate exponents for RC&M&AS and RN
    tau_fit = 1._dp / ( fit_a * ( LN + fit_kN*RN )**fit_p * (MB + fit_kC*RC + fit_kA*AS)**fit_q )
#endif

  end  function tau_fit

#endif /* aMC__contains */

! ----- custom [de]init --------------------------------------------------------
#ifdef    aMC__init__defs
#endif /* aMC__init__defs */
#ifdef    aMC__init__code

  ! read data
#ifdef mssPF_mode_MEP
    call mssPF_read_data('mssPF-MEP.dat')
  ! quick-check for LN adjustment
   !mssPF_data(idx_LN)%r(:) = mssPF_data(idx_LN)%r(:) !*0.5 !*(2.0/2.375)
#endif

#ifdef mssPF_mode_CIO
    call mssPF_read_data('mssPF-CIO.dat')
#endif

#endif /* aMC__init__code */

#ifdef    aMC__deinit__defs
#endif /* aMC__deinit__defs */
#ifdef    aMC__deinit__code

  ! cleanup
    deallocate(mssPF_data)
    deallocate(tsN)
    deallocate(mssPF_case_tsi)

#endif /* aMC__deinit__code */

! ----- MC parameter setup -----------------------------------------------------
#ifdef    aMC__parameter_setup__defs
    string  :: s
#endif /* aMC__parameter_setup__defs */
#ifdef    aMC__parameter_setup__code
  ! MC parameters

  ! probed randoms & ECFs info
    s = ''

  ! lightning NOx
    s = s//'LN'// &
#ifdef mssPF_ec_LN
    '('//num2str(mssPF_ec_LN)//')'
    res = aMC_output_add('LN     | TgN/yr      | lNOx emission (ECF-corr.)', ra=mssPF_data(idx_cLN)%r )
    res = aMC_mcpar_register(aMC_kind_unif, r=ecf_LN, range=(/ -0.99 , +6.0  /)*mssPF_ec_LN, info='ecf_LN | percent | lNOx .ECF.', sf=1e2_dp )
    res = aMC_mcpar_register(aMC_kind_unif, r=ecs_LN, range=(/ -10.0 , +10.0 /)*mssPF_ec_LN*mssPF_ec_shift, info='ecs_LN | TgN/yr  | lNOx .ECS.' )
#else
    ''
    res = aMC_mcpar_register(aMC_kind_unif, r=pLN,    range=(/ -1.   ,   12. /), info='LN | TgN/yr | LN emission (random)' )
#endif

  ! CH4 emission
    s = s//' ME'// &
#ifdef mssPF_ec_ME
    '('//num2str(mssPF_ec_ME)//')'
    res = aMC_output_add('ME     | TgC(CH4)/yr | CH4 emission (ECF-corr.)',  ra=mssPF_data(idx_cME)%r  )
    res = aMC_mcpar_register(aMC_kind_unif, r=ecf_ME, range=(/ -0.99 , +3.0  /)*mssPF_ec_ME, info='ecf_ME | percent     | CH4 .ECF.', sf=1e2_dp )
    res = aMC_mcpar_register(aMC_kind_unif, r=ecs_ME, range=(/ -100., +100.  /)*mssPF_ec_ME*mssPF_ec_shift, info='ecs_ME | TgC(CH4)/yr | CH4 .ECS.' )
#else
    ''
    res = aMC_mcpar_register(aMC_kind_unif, r=pME,    range=(/ 10.   ,  600. /), info='ME | TgC(CH4)/yr | CH4 emission (random)' )
#endif

  ! reactive C
    s = s//' RC'// &
#ifdef mssPF_ec_RC
#ifdef mssPF_ec_RC_bycat
    '('//num2str(mssPF_ec_RC)//':bycat)'
    res = aMC_mcpar_register(aMC_kind_unif, r=ecf_RC_bb, range=(/ -0.99 , +5.0  /)*mssPF_ec_RC, sf=1e2_dp, info='ecf_RC_bb | percent | RC BB .ECF.' )
    res = aMC_mcpar_register(aMC_kind_unif, r=ecf_RC_na, range=(/ -0.99 , +5.0  /)*mssPF_ec_RC, sf=1e2_dp, info='ecf_RC_na | percent | RC nat. .ECF.' )
    res = aMC_mcpar_register(aMC_kind_unif, r=ecf_RC_an, range=(/ -0.99 , +5.0  /)*mssPF_ec_RC, sf=1e2_dp, info='ecf_RC_an | percent | RC anth. .ECF.' )
    res = aMC_output_add('ecf_RC | percent | RC .ECF. (bycat diag.)', ra=mssPF_data(idx_ecf_RC)%r, sf=1e2_dp )
#else
    '('//num2str(mssPF_ec_RC)//')'
    res = aMC_mcpar_register(aMC_kind_unif, r=ecf_RC, range=(/ -0.99 , +5.0  /)*mssPF_ec_RC, info='ecf_RC | percent | RC .ECF.', sf=1e2_dp )
    res = aMC_mcpar_register(aMC_kind_unif, r=ecs_RC, range=(/ -100. , +200. /)*mssPF_ec_RC*mssPF_ec_shift, info='ecs_RC | TgC/yr  | RC .ECS.' )
#endif
    res = aMC_output_add('RC     | TgC/yr  | RC emission (ECF-corr.)', ra=mssPF_data(idx_cRC)%r )
#else
    ''
    res = aMC_mcpar_register(aMC_kind_unif, r=pRC,       range=(/ 10.   , 1100. /), info='RC | TgC/yr | RC emission (random)' )
#endif

  ! reactive N
    s = s//' RN'// &
#ifdef mssPF_ec_RN
#ifdef mssPF_ec_RN_bycat
    '('//num2str(mssPF_ec_RN)//':bycat)'
    res = aMC_mcpar_register(aMC_kind_unif, r=ecf_RN_bb, range=(/ -0.99 , +5.0  /)*mssPF_ec_RN, sf=1e2_dp, info='ecf_RN_bb | percent | RN BB .ECF.' )
    res = aMC_mcpar_register(aMC_kind_unif, r=ecf_RN_na, range=(/ -0.99 , +5.0  /)*mssPF_ec_RN, sf=1e2_dp, info='ecf_RN_na | percent | RN nat. .ECF.' )
    res = aMC_mcpar_register(aMC_kind_unif, r=ecf_RN_an, range=(/ -0.99 , +5.0  /)*mssPF_ec_RN, sf=1e2_dp, info='ecf_RN_an | percent | RN anth. .ECF.' )
    res = aMC_output_add('ecf_RN | percent | RN .ECF. (bycat diag.)',  ra=mssPF_data(idx_ecf_RN)%r, sf=1e2_dp )
#else
    '('//num2str(mssPF_ec_RN)//')'
    res = aMC_mcpar_register(aMC_kind_unif, r=ecf_RN,    range=(/ -0.99 , +5.0  /)*mssPF_ec_RN, info='ecf_RN | percent | RN .ECF.', sf=1e2_dp )
    res = aMC_mcpar_register(aMC_kind_unif, r=ecs_RN,    range=(/  -10. , +10.0 /)*mssPF_ec_RN*mssPF_ec_shift, info='ecs_RN | percent | RN .ECS.' )
#endif
    res = aMC_output_add('RN     | TgN/yr  | RN emission (ECF-corr.)', ra=mssPF_data(idx_cRN)%r )
#else
    ''
    res = aMC_mcpar_register(aMC_kind_unif, r=pRN,       range=(/  0.1  ,   50. /), info='RN | TgN/yr | RN emission (random)' )
#endif
    mssPF_scheme = s

  ! extra output
    res = aMC_output_add('yr     | AD     | year',                      ra=mssPF_data(idx_yr)%r )
    res = aMC_output_add('len    | yrs    | period length',             ra=mssPF_data(idx_len)%r )
  ! derived parameters -> M, tau, E, LN, RC, RN
    res = aMC_output_add('tau    | yrs    | CH4 lifetime (fit calc.)',  ra=mssPF_data(idx_tau)%r )
    res = aMC_output_add('MB     | TgC    | CH4 burden (=ME*tau)',      ra=mssPF_data(idx_cMB)%r )

  ! hit cases
  ! switch off probe statistics output by case
    aMC_hit_set(aMC_hit_set_probestat)%out_single = .false.

  ! cases as time-slices
    do ci = 1, tsQ
      s = tsN(ci)%s
      s = s//' | slice at '//num2str(int(mssPF_data(idx_yr)%r(ci)))//'+'//num2str(int(mssPF_data(idx_len)%r(ci)))//' yrs,' &
#ifdef mssPF_mode_MEP
           //' B:'//num2str(r=mssPF_data(idx_B)%r(ci))//'(+-'//num2str(r=mssPF_data(idx_dB)%r(ci))//')' &
           //' GCM: ME:'//num2str(r=mssPF_data(idx_ME)%r(ci))//' LN:'//num2str(r=mssPF_data(idx_LN)%r(ci)) &
           //' RN:'//num2str(r=mssPF_data(idx_RN_bb)%r(ci))//'+'//num2str(r=mssPF_data(idx_RN_na)%r(ci))//'+'//num2str(r=mssPF_data(idx_RN_an)%r(ci)) &
           //' RC:'//num2str(r=mssPF_data(idx_RC_bb)%r(ci))//'+'//num2str(r=mssPF_data(idx_RC_na)%r(ci))//'+'//num2str(r=mssPF_data(idx_RC_an)%r(ci))
#else
           //' B:'//num2str(r=mssPF_data(idx_B)%r(ci))//'(+-'//num2str(r=mssPF_data(idx_dB)%r(ci))//')'
#endif
      res = aMC_hit_case_add(s)
      mssPF_case_tsi(res) = ci
    enddo

  ! hit sets
  ! caption (incl. exceptions)
    s = ' | CH4 burden/lifetime fit ('//mssPF_scheme//')'
    mssPF_set_hit        = aMC_hit_set_add(       'hit'//s//' - single time-slices', out_single=.true. )
#ifdef mssPF_mode_CIO
  ! switch off single case output
    aMC_hit_set(mssPF_set_hit)%out_single = .false.
#endif

#ifdef mssPF_mode_MEP
    mssPF_set_hit_PI     = aMC_hit_set_add(    'hit_pi'//s//' - time-slices until industrial', out_single=.false. )
    mssPF_set_hit_I2PD   = aMC_hit_set_add(  'hit_i2pd'//s//' - time-slices from industrial until PD (1990)', out_single=.false. )
    mssPF_set_hit_PD     = aMC_hit_set_add(    'hit_pd'//s//' - time-slices from PD (1990) and later', out_single=.false. )
    mssPF_set_hit_noLGM  = aMC_hit_set_add( 'hit_nolgm'//s//' - time-slices except LGM', out_single=.false. )
    mssPF_set_hit_no10ka = aMC_hit_set_add('hit_no10ka'//s//' - time-slices except 10ka', out_single=.false. )
    mssPF_set_hit_all    = aMC_hit_set_add(   'hit_all'//s//' - all time-slices', out_single=.false. )
#endif

#endif /* aMC__parameter_setup__code */


! ----- new initial state ------------------------------------------------------
#ifdef    aMC__new_x0__defs
#endif /* aMC__new_x0__defs */
#ifdef    aMC__new_x0__code
  ! reset flags for matching time-slice selection
    flag_PI     = .true.
    flag_I2PD   = .true.
    flag_PD     = .true.
    flag_noLGM  = .true.
    flag_no10ka = .true.
    flag_all    = .true.
#endif /* aMC__new_x0__code */

! ----- prepare output ---------------------------------------------------------
#ifdef    aMC__output_prepare__defs
#endif /* aMC__output_prepare__defs */
#ifdef    aMC__output_prepare__code
#endif /* aMC__output_prepare__code */

! ----- find matching cases ----------------------------------------------------
#ifdef    aMC__match__defs
    logical   :: fit
    integer   :: tsi        ! time-slice index
#endif /* aMC__match__defs */
#ifdef    aMC__match__cases__code
   ! inside the loop over cases: case = aMC_match_case_from, aMC_match_case_upto
   ! NOTE: do not use cycle!

    ! time-slice index in data()
      tsi = mssPF_case_tsi(case)

#ifdef DEBUG
      print *,'--- comparing case: '//aMC_hit_case(case)%name,' tsi:',tsi,'-------'
      print '(A,*(f9.4))', ' M(obs.):',mssPF_data(idx_B)%r(tsi)
      print '(A,*(f9.4))', 'dM(obs.):',mssPF_data(idx_dB)%r(tsi)
      print '(A,*(f9.4))', 'MB(cal.):',mssPF_data(idx_cMB)%r(tsi)
#endif

    ! probe statistic
      aMC_hit_set(aMC_hit_set_probestat)%is_hit = aMC_shot_real !.true.

    ! hit if calc. and obs. B fall within dB
      fit = dabs( mssPF_data(idx_cMB)%r(tsi)-mssPF_data(idx_B)%r(tsi)) .le. mssPF_data(idx_dB)%r(tsi)

    ! ECF
      aMC_hit_set(mssPF_set_hit)%is_hit = fit

    ! adjust time-slice selection flags
      flag_all  = ( flag_all .and. fit )
#ifdef mssPF_mode_MEP
      if ( mssPF_data(idx_yr)%r(tsi) .gt. -18e3 ) flag_noLGM = ( flag_noLGM .and. fit )
      if ( abs( mssPF_data(idx_yr)%r(tsi)-10e3 ) .gt. 1e3 ) flag_no10ka = ( flag_no10ka .and. fit )
      select case(int(mssPF_data(idx_yr)%r(tsi)))
        case(-22000:1499) !> LGM to PI
          flag_PI   = (   flag_PI .and. fit )
        case(1500:1989)   !> ind. to PD
          flag_I2PD = ( flag_I2PD .and. fit )
        case(1990:2100)   !> PD+
          flag_PD   = (   flag_PD .and. fit )
      end select
#endif /* mssPF_mode_MEP */

#ifdef DEBUG
      print *,'  hit:',aMC_hit_set(mssPF_set_hit)%is_hit
      print *,'flags: all PI I2PD PD noLGM no10ka: ', flag_all, flag_PI, flag_I2PD, flag_PD, flag_noLGM, flag_no10ka
#endif

#endif /* aMC__match__cases__code */

#ifdef    aMC__match__sets__code
    ! inside the loop over sets: set = aMC_match_set_from, aMC_match_set_upto

    ! check if multiple time-slices were hit (via delta)
      do case = aMC_match_case_from, aMC_match_case_upto

      ! time-slice index in data()
        tsi = mssPF_case_tsi(case)  ! indices for lab samples

      ! set hits for time-slice combinations
        if ( set .eq. mssPF_set_hit_all ) match(case,set) = merge(1,0,flag_all)
#ifdef mssPF_mode_MEP
        if ( ( set .eq. mssPF_set_hit_noLGM ) .and. ( mssPF_data(idx_yr)%r(tsi) .ge. -18e3 ) ) &
          match(case,set) = merge(1,0,flag_noLGM)
        if ( ( set .eq. mssPF_set_hit_no10ka ) .and. ( abs( mssPF_data(idx_yr)%r(tsi)-10e3 ) .gt. 1e3 ) ) &
          match(case,set) = merge(1,0,flag_no10ka)
        select case(int(mssPF_data(idx_yr)%r(tsi)))
          case(-22000:1499) !> LGM to PI
            if ( set .eq. mssPF_set_hit_PI ) match(case,set) = merge(1,0,flag_PI)
          case(1500:1989)   !> ind. to PD
            if ( set .eq. mssPF_set_hit_I2PD ) match(case,set) = merge(1,0,flag_I2PD)
          case(1990:2100)   !> PD+
            if ( set .eq. mssPF_set_hit_PD ) match(case,set) = merge(1,0,flag_PD)
        end select
#endif /* mssPF_mode_MEP */
      enddo ! case = aMC_match_case_from, aMC_match_case_upto
#endif /* aMC__match__sets__code */


! ----- aMC_physc --------------------------------------------------------------
#ifdef    aMC__physc__defs
      integer :: qneg   ! no. of negative parameters for checks
#endif /* aMC__physc__defs */
#ifdef    aMC__physc__code

#define __(idx) mssPF_data(idx)%r(1:tsQ)

    ! calculating input

    ! lightning NOx
#ifdef mssPF_ec_LN
      __(idx_cLN) = __(idx_LN)*(1.+ecf_LN) + ecs_LN
#else
      __(idx_cLN) = pLN
#endif

    ! CH4 emission
#ifdef mssPF_ec_ME
      __(idx_cME) = __(idx_ME)*(1.+ecf_ME) + ecs_ME
#else
      __(idx_cME) = pME
#endif

    ! reactive C
#ifdef mssPF_ec_RC
#ifdef mssPF_ec_RC_bycat
    ! ECFs by category
      __(idx_cRC) = __(idx_RC_bb)*(1.+ecf_RC_bb) + __(idx_RC_na)*(1.+ecf_RC_na) + __(idx_RC_an)*(1.+ecf_RC_an) + ecs_RC
    ! diagnostic total ecf.
      __(idx_ecf_RC) = __(idx_cRC)/(__(idx_RC_bb)+__(idx_RC_na)+__(idx_RC_an) - ecs_RC)
#else
    ! single ECF
      __(idx_cRC) = (__(idx_RC_bb)+__(idx_RC_na)+__(idx_RC_an))*(1.+ecf_RC) + ecs_RC
#endif
#else
      __(idx_cRC) = pRC
#endif

    ! reactive N
#ifdef mssPF_ec_RN
#ifdef mssPF_ec_RN_bycat
    ! ECFs by category
      __(idx_cRN) = __(idx_RN_bb)*(1.+ecf_RN_bb) + __(idx_RN_na)*(1.+ecf_RN_na) + __(idx_RN_an)*(1.+ecf_RN_an) + ecs_RN
    ! diagnostic total ecf.
      __(idx_ecf_RN) = __(idx_cRN)/(__(idx_RN_bb)+__(idx_RN_na)+__(idx_RN_an) - ecs_RN)
#else
    ! single ECF
      __(idx_cRN) = (__(idx_RN_bb)+__(idx_RN_na)+__(idx_RN_an))*(1.+ecf_RN) + ecs_RN
#endif
#else
      __(idx_cRN) = pRN
#endif

    ! calculating fit lifetime and derivatives
    ! lifetime (using observed M)
      __(idx_tau) = tau_fit( __(idx_B) , __(idx_cLN), __(idx_cRN) , __(idx_cRC) )
    ! est. burden (using probed ME)
      __(idx_cMB) = __(idx_cME) * __(idx_tau)

#ifdef DEBUG
      print *,'mssPF_physc():'
      print *,'    ME:',__(idx_cME)
      print *,'    LN:',__(idx_cLN)
      print *,'    RN:',__(idx_cRN)
      print *,'    RC:',__(idx_cRC)
      print *,'ecf_RN:',__(idx_ecf_RN)
      print *,'ecf_RC:',__(idx_ecf_RC)
      print *,'    MB:',__(idx_cMB)
      print *,'   tau:',__(idx_tau)
#endif

    ! shot is done when simulation end is reached or error occured
      aMC_shot_done = .true.
!     aMC_shot_real = .true.

    ! check for negatives
      qneg = 0
      qneg = qneg + sum(merge(1,0,__(idx_cLN) .lt. 0.))
      qneg = qneg + sum(merge(1,0,__(idx_cME) .lt. 0.))
      qneg = qneg + sum(merge(1,0,__(idx_cRC) .lt. 0.))
      qneg = qneg + sum(merge(1,0,__(idx_cRN) .lt. 0.))
      aMC_shot_real = ( qneg.eq.0 )

#endif /* aMC__physc__code */
! ----- aMC_physc:check_req_hits -----
! inside the loop over cases: cn = aMC_match_case_from, aMC_match_case_upto
#ifdef    aMC__physc__check_req_hits__code
        ! stop when hit statistic is complete for all samples
          do sn = aMC_match_set_from, aMC_match_set_upto
            if ( aMC_hits(cn,sn,00).lt.aMC_req_hits ) then
              req_hits_found4all = .false.
              exit ! do-loop
            endif
          enddo
#endif /* aMC__physc__check_req_hits__code */


! ----- info output ------------------------------------------------------------
#ifdef    aMC__info__code
    _info_ '| - probing scheme: '//mssPF_scheme
    _info_ '| - fit version: '// &
#ifdef mssPF_fit_201212
           '201212'
#endif
#ifdef mssPF_fit_210128
           '210128'
#endif
#ifdef mssPF_fit_210219
           '210219'
#endif
#endif /* aMC__info__code */

! --- XXX ---
#ifdef    aMC__XXX__defs
#endif /* aMC__XXX__defs */
#ifdef    aMC__XXX__code
#endif /* aMC__XXX__code */
