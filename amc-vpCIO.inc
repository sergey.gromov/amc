! - f90 -

! ----- vpCIO: variations in present/past clumped O2 isotope composition -------

! - parameters -----------------------------------------------------------------

! - aMC parameters overrides ---------------------------------------------------
!undef  aMC_no_sim_on_master      // master only serves run control & I/O
!undef  aMC_output_non_real       // output also uncuccessful realisations
!undef  aMC_output_single_case    // enable output containers for single cases
!undef  aMC_output_common_set     // enable output containers for sets of cases
!undef  aMC_output_probe_stat     // output MC probe statistic (use to check for skewed input)
!undef  aMC_output_spare_extra_rq // skip PE output requests if the local amnt. of hits is greater than ~ max_req / (#PEs)
! - tuning/debugging -----------------------------------------------------------
!define aMC_run_single_sim        // enable to run a single try simulation
!define DEBUG                     // useful runtime info
!define DEEPDEBUG                 // a lot of runtime info

! - vpCIO parameters -----------------------------------------------------------
#define vpCIO_MCJ 0               /* set to 1 to enable inp. data norm. dist. simulation */
#define vpCIO_sfe_R 18            /* exponent of iex. rate (mol/yr) scaling factor */
!define vpCIO_probeULT            /* probe upper/lower trop. domains instead of whole troposphere */
#define vpCIO_mode_dPD            /* delta-vs-PD fitting mode */

! ----- global imports ---------------------------------------------------------
#ifdef aMC__uses
#endif

! ----- global definitions -----------------------------------------------------
#ifdef aMC__defs

! input data
  integer              :: vpCIO_q,              &   !> no of model exp. samples
                          vpCIO_n_rPD               !> idx for ref. data in delta-PD mode

#define __ 1:vpCIO_q
  integer, parameter   :: vpCIO_ci_LGM = 1,     &   !> given climates indices
                          vpCIO_ci_MH  = 2,     &
                          vpCIO_ci_PI  = 3,     &
                          vpCIO_ci_PD  = 4,     &
                          vpCIO_ci_rPD = 0          !> flag for ref. data in delta-PD mode

  real8, dimension(:), &                            !> input/derived values for each experiment (1:val,2:err)
    allocatable        :: iv_D_T,    ie_D_T,    &   !> D36 in troposphere
                          iv_D_BA,   ie_D_BA,   &   !> D36 in BL Arctic
                          iv_dD_BAT, ie_dD_BAT, &   !> diff. betw. D36 in BL Arctic and troposphere
                          iv_Re_T,   ie_Re_T,   &   !> equilibration rate - troposphere
                          iv_Re_U,   ie_Re_U,   &   !>                    - upper (UT or FT)
                          iv_Re_L,   ie_Re_L,   &   !>                    - lower (LT or BL)
                          iv_De_T,   ie_De_T,   &   !> D36 equilibration end-member - troposphere
                          iv_De_U,   ie_De_U,   &   !>                              - upper (UT or FT)
                          iv_De_L,   ie_De_L        !>                              - lower (LT or BL)
  strings              :: vpCIO_exp(:)              !> experiment name
  integer, allocatable :: vpCIO_ci(:)               !> climate/condition flag

! operational/derived variables
  real8, dimension(:), allocatable &
                       :: vpCIO_D_T, mcj_D_T,     & !> [delta] D36 trop.
                          vpCIO_D_BA, mcj_D_BA,   & !> [delta] D36 BL Arctic
                          vpCIO_dD_BAT,           & !> BL Arctic vs. trop. D36 difference
                          vpCIO_S, vpCIO_dSa,     & !> [delta] STT flux & dev. for comp.
                          vpCIO_Re_T, mcj_Re_T,   & !> eq. rate in T
                          vpCIO_Re_U, mcj_Re_U,   & !>                  UT/FT
                          vpCIO_Re_L, mcj_Re_L,   & !>                  LT/BL
                          vpCIO_De_T, mcj_De_T,   & !> eq. signature in T
                          vpCIO_De_U, mcj_De_U,   & !>                  UT/FT
                          vpCIO_De_L, mcj_De_L      !>                  LT/BL

  real8, allocatable   :: regr(:,:)                 !> regr. analysis of F [k-b-R2+5interm,1:vpCIO_q]

! probed parameters
  real8                :: vpCIO_Dr_OW,            & !> over-world D36 reset end-member
                          vpCIO_S2R,              & !> ratio of STE flux / TR equilibration rate
                          vpCIO_L2R,              & !> ratio of (LT or BL)/TR equilibration rates
                          vpCIO_dDr_OW,           & !> [delta] over-world reset end-member
                          vpCIO_dS,               & !> [delta] of STE flux
                          vpCIO_dRe_T,            & !> [delta] eq. rate in trop.
                          vpCIO_dRe_U,            & !>                     UT/FT
                          vpCIO_dRe_L,            & !>                     LT/BL
                          vpCIO_dDe_T,            & !> [delta] eq. signature in trop.
                          vpCIO_dDe_U,            & !>                          UT/FT
                          vpCIO_dDe_L               !>                          LT/BL

! MC hit sets/cases
  integer              :: vpCIO_set_fitS,         & !> same derived STE flux
                          vpCIO_set_fitD,         & !> fitting observed D
                          vpCIO_set_fitB,         & !> fitting both
                          vpCIO_case_LGM,         &
                          vpCIO_case_MH,          &
                          vpCIO_case_PDNA,        &
                          vpCIO_case_PD
  integer, allocatable :: vpCIO_case_exp(:)

! vpCIO runtime parameters (defaults)
  character(len=5)     :: vpCIO_T_def, &            !> def. for tropospheric domain
                          vpCIO_U_def, &            !> + upper domain
                          vpCIO_L_def               !> + lower domain
  real8                :: vpCIO_dPD__Dr_OW          !> PD over-world end-member D36 for delta-PD mode
#endif

! ----- custom namelist entries ------------------------------------------------
#ifdef    aMC__nml
    ! <var1>, <var2>, &
      vpCIO_T_def,      &  !> TR/TO3/T100
      vpCIO_U_def,      &  !> UT/FT
      vpCIO_L_def,      &  !> LT/BL
      vpCIO_dPD__Dr_OW, &
#endif /* aMC__nml */

! ----- custom routines --------------------------------------------------------
#ifdef aMC__contains
! ----- read lab data for vpCIO -----
  subroutine vpCIO_read_exp_data()

    use aMC_io, only: aMC_read_tab_data_colvar

    implicit none

    character(len=*), parameter :: na = 'nan', &
#ifdef vpCIO_mode_dPD
                                   ifname = 'vpCIO_dPD.dat'
#else
                                   ifname = 'vpCIO.dat'
#endif
    integer                     :: ri, ref = 0 !> read error flag
    string                      :: T_def, U_def, L_def

#ifdef DEBUG
    print *,'vpCIO_read_exp_data(): reading from '//ifname
#endif

  ! read exp. names and no. of samples
    vpCIO_q = aMC_read_tab_data_colvar(ifname, 'exp', s=vpCIO_exp)

  ! read checking for records having the same length
    T_def = trim(vpCIO_T_def)
    U_def = trim(vpCIO_U_def)
    L_def = trim(vpCIO_L_def)
    if ( vpCIO_q.ne.aMC_read_tab_data_colvar(ifname, 'ci',                   na=na, i=vpCIO_ci               ) ) ref = ref + 1  !> climate idx/flag
    if ( vpCIO_q.ne.aMC_read_tab_data_colvar(ifname, 'd36__'//T_def//'_GL',  na=na, r=iv_D_T,    re=ie_D_T   ) ) ref = ref + 1  !> D36 in troposphere
    if ( vpCIO_q.ne.aMC_read_tab_data_colvar(ifname, 'd36__BL_ARC',          na=na, r=iv_D_BA,   re=ie_D_BA  ) ) ref = ref + 1  !> D36 in BL Arctic
    if ( vpCIO_q.ne.aMC_read_tab_data_colvar(ifname, 'dd36__BL_ARC-'//T_def, na=na, r=iv_dD_BAT, re=ie_dD_BAT) ) ref = ref + 1  !> diff. betw. D36 in BL Arctic and troposphere
  ! D36 equil. end-member & equilibration rate / in 10^vpCIO_sfe_R mol/s /
    if ( vpCIO_q.ne.aMC_read_tab_data_colvar(ifname, 'ieqD__'//T_def//'_GL', na=na, r=iv_De_T,   re=ie_De_T  ) ) ref = ref + 1  !> - troposphere
    if ( vpCIO_q.ne.aMC_read_tab_data_colvar(ifname, 'ieqR__'//T_def//'_GL', na=na, r=iv_Re_T,   re=ie_Re_T, sf=real(10.**(-vpCIO_sfe_R),dp) ) ) ref = ref + 1  !> - troposphere
#ifdef vpCIO_probeULT
      if ( vpCIO_q.ne.aMC_read_tab_data_colvar(ifname, 'ieqD__'//U_def//'_GL', na=na, r=iv_De_U, re=ie_De_U  ) ) ref = ref + 1  !> - upper
      if ( vpCIO_q.ne.aMC_read_tab_data_colvar(ifname, 'ieqD__'//L_def//'_GL', na=na, r=iv_De_L, re=ie_De_L  ) ) ref = ref + 1  !> - lower trop.
      if ( vpCIO_q.ne.aMC_read_tab_data_colvar(ifname, 'ieqR__'//U_def//'_GL', na=na, r=iv_Re_U, re=ie_Re_U, sf=real(10.**(-vpCIO_sfe_R),dp) ) ) ref = ref + 1  !> - upper
      if ( vpCIO_q.ne.aMC_read_tab_data_colvar(ifname, 'ieqR__'//L_def//'_GL', na=na, r=iv_Re_L, re=ie_Re_L, sf=real(10.**(-vpCIO_sfe_R),dp) ) ) ref = ref + 1  !> - lower
#endif
    if ( ref.ne.0 ) then
      _log_ 'vpCIO_read_exp_data(): error reading data, ref = '//num2str(ref)
      stop 157
    endif

  ! info
    if ( p_parallel_io ) then
      _info__ _aMC_setup_//': read '//num2str(i=vpCIO_q)//' experiments:'
      do ref = 1, vpCIO_q
        _info__ ' '//vpCIO_exp(ref)%s//':'//num2str(vpCIO_ci(ref))
      enddo
      _info_
    endif

  end subroutine vpCIO_read_exp_data
#endif /* aMC__contains */

! ----- custom [de]init --------------------------------------------------------
#ifdef    aMC__init__defs
#endif /* aMC__init__defs */
#ifdef    aMC__init__code
  ! read in exp. data
    call vpCIO_read_exp_data()

  ! init oper. arrays
    call resize(vpCIO_S,    vpCIO_q,init=aMC_NODATA)
    call resize(vpCIO_dSa,   vpCIO_q,init=aMC_NODATA)
    allocate(regr(1:3,vpCIO_q))  !> regr. analysis of F [k-b-R2+5interm,1:vpCIO_q]

  ! input + jitter
    call resize(vpCIO_D_T,    vpCIO_q, init=aMC_NODATA); call resize(mcj_D_T,  vpCIO_q, init=aMC_NODATA)
    call resize(vpCIO_D_BA,   vpCIO_q, init=aMC_NODATA); call resize(mcj_D_BA, vpCIO_q, init=aMC_NODATA)
    call resize(vpCIO_dD_BAT, vpCIO_q, init=aMC_NODATA);
    call resize(vpCIO_Re_T,   vpCIO_q, init=aMC_NODATA); call resize(mcj_Re_T, vpCIO_q, init=aMC_NODATA)
    call resize(vpCIO_De_T,   vpCIO_q, init=aMC_NODATA); call resize(mcj_De_T, vpCIO_q, init=aMC_NODATA)
#ifdef vpCIO_probeULT
    call resize(vpCIO_Re_U,   vpCIO_q, init=aMC_NODATA); call resize(mcj_Re_U, vpCIO_q, init=aMC_NODATA)
    call resize(vpCIO_Re_L,   vpCIO_q, init=aMC_NODATA); call resize(mcj_Re_L, vpCIO_q, init=aMC_NODATA)
    call resize(vpCIO_De_U,   vpCIO_q, init=aMC_NODATA); call resize(mcj_De_U, vpCIO_q, init=aMC_NODATA)
    call resize(vpCIO_De_L,   vpCIO_q, init=aMC_NODATA); call resize(mcj_De_L, vpCIO_q, init=aMC_NODATA)
#endif

  ! case nos.
    call resize(vpCIO_case_exp,vpCIO_q)
#endif /* aMC__init__code */

#ifdef    aMC__deinit__defs
#endif /* aMC__deinit__defs */
#ifdef    aMC__deinit__code
    deallocate(vpCIO_S)
    deallocate(vpCIO_dSa)
    deallocate(regr)
    deallocate(vpCIO_D_T)
    deallocate(vpCIO_D_BA)
    deallocate(vpCIO_dD_BAT)
    deallocate(vpCIO_Re_T)
    deallocate(vpCIO_De_T)
#ifdef vpCIO_probeULT
    deallocate(vpCIO_Re_U)
    deallocate(vpCIO_Re_L)
    deallocate(vpCIO_De_U)
    deallocate(vpCIO_De_L)
#endif
    deallocate(vpCIO_case_exp)
#endif /* aMC__deinit__code */

! ----- MC parameter setup -----------------------------------------------------
#ifdef    aMC__parameter_setup__defs
    character(len=*), parameter :: SR='@SR', SG='@SG', D=SG//'D'//SR, D36=D//'_3_6', &
                                   TR='^T', BL='^B^L', FT='^F^T', LT='^L^T', UT='^U^T', OW='^O^W', &
                                   EQ='_E_Q', dPD=D//'_P_D', Re='R'//EQ, De=SG//'D'//SR//EQ, &
                                   er='equilibration rate', ee='equilibration end-member'
    string                      :: se, fu
#endif /* aMC__parameter_setup__defs */
#ifdef    aMC__parameter_setup__code

  ! MC parameters
#ifdef vpCIO_mode_dPD
#define ldPD ' (vs. PD)'
  ! probed: deltas vs. PD
    res = aMC_mcpar_register(aMC_kind_unif, r=vpCIO_dDr_OW,  range=(/ -0.1, +0.1 /), info='dD_OW | permil  | '//dPD//OW//D36//' / over-world '//ee//ldPD )
    res = aMC_mcpar_register(aMC_kind_unif, r=vpCIO_dS,      range=(/ -0.2, +0.2 /), info='dS    | percent | '//dPD//'S / STE flux'//ldPD, sf=1e2_dp )
#ifdef vpCIO_probeULT
      res = aMC_mcpar_register(aMC_kind_unif, r=vpCIO_dRe_U, range=(/ -0.5, +0.5 /), info='dRe_U | percent | '//dPD//UT//Re//' / UT '//er//ldPD, sf=1e2_dp )
      res = aMC_mcpar_register(aMC_kind_unif, r=vpCIO_dRe_L, range=(/ -0.5, +0.5 /), info='dRe_L | percent | '//dPD//LT//Re//' / LT '//er//ldPD, sf=1e2_dp )
      res = aMC_mcpar_register(aMC_kind_unif, r=vpCIO_dDe_U, range=(/ -0.5, +0.5 /), info='dDe_U | permil  | '//dPD//UT//De//' / UT '//ee//ldPD )
      res = aMC_mcpar_register(aMC_kind_unif, r=vpCIO_dDe_L, range=(/ -0.5, +0.5 /), info='dDe_L | permil  | '//dPD//LT//De//' / LT '//ee//ldPD )
#else
      res = aMC_mcpar_register(aMC_kind_unif, r=vpCIO_dRe_T, range=(/ -0.5, +0.5 /), info='dRe_T | percent | '//dPD//TR//Re//' / tropospheric '//er//ldPD, sf=1e2_dp )
      res = aMC_mcpar_register(aMC_kind_unif, r=vpCIO_dDe_T, range=(/ -0.5, +0.5 /), info='dDe_T | permil  | '//dPD//TR//Re//' / tropospheric '//ee//ldPD )
#endif /* vpCIO_probeULT */
#else
  ! probed parameters
    res = aMC_mcpar_register(aMC_kind_unif, r=vpCIO_Dr_OW, range=(/ 2.0,  3.0 /), info='D_OW | permil | '//OW//De//' / over-world '//ee )
    res = aMC_mcpar_register(aMC_kind_unif, r=vpCIO_S2R,   range=(/ 0.01, 2.0 /), info='S2R  | -      | '//SR//'S/R / ratio of STE to tropospheric '//ee )
    res = aMC_mcpar_register(aMC_kind_unif, r=vpCIO_L2R,   range=(/ 0.01, 0.5 /), info='L2R  | -      | '//SR//LT//'R/'//TR//'R / ratio of LT to tropospheric '//ee )
  ! MC jitters
    do ci = 1, vpCIO_q
      res = aMC_mcpar_register(aMC_kind_norm, r=mcj_Re_T(ci),  range=(/ 0.0, 1.0 /), info='mcj_Re_T_'//num2str(ci)//' | - | jitter', out=.false. )
      res = aMC_mcpar_register(aMC_kind_norm, r=mcj_D_T(ci),   range=(/ 0.0, 1.0 /), info='mcj_D_T_'//num2str(ci)//'  | - | jitter', out=.false. )
      res = aMC_mcpar_register(aMC_kind_norm, r=mcj_De_T(ci),  range=(/ 0.0, 1.0 /), info='mcj_De_T_'//num2str(ci)//' | - | jitter', out=.false. )
      res = aMC_mcpar_register(aMC_kind_norm, r=mcj_De_U(ci),  range=(/ 0.0, 1.0 /), info='mcj_De_U_'//num2str(ci)//' | - | jitter', out=.false. )
      res = aMC_mcpar_register(aMC_kind_norm, r=mcj_De_LT(ci), range=(/ 0.0, 1.0 /), info='mcj_De_LT_'//num2str(ci)//'| - | jitter', out=.false. )
      res = aMC_mcpar_register(aMC_kind_norm, r=mcj_D_BA(ci),  range=(/ 0.0, 1.0 /), info='mcj_D_BA_'//num2str(ci)//' | - | jitter', out=.false. )
    enddo
#endif /* vpCIO_mode_dPD */

  ! output
  ! fluxes units
    fu = '10**'//num2str(i=vpCIO_sfe_R)//' mol/yr'
  ! derived parameters -> sample-dependent (use case-wise array support, ar= ai= )
    res = aMC_output_add('ci   | flag   | climate/condition flag', ia=vpCIO_ci)
    res = aMC_output_add('D_T  | permil | '//SR//TR//D36//' / tropospheric D36', ra=vpCIO_D_T)
    res = aMC_output_add('D_BA | permil | '//SR//BL//'^A'//D36//' / Arctic boundary layer D36', ra=vpCIO_D_BA)
    res = aMC_output_add('Re_T |'//fu//'| '//SR//TR//Re//' / tropospheric '//er, ra=vpCIO_Re_T)
    res = aMC_output_add('De_T | permil | '//SR//TR//De//' / tropospheric '//ee, ra=vpCIO_De_T)
#ifdef vpCIO_probeULT
    res = aMC_output_add('Re_U |'//fu//'| '//SR//UT//Re//' / UT '//er, ra=vpCIO_Re_U)
    res = aMC_output_add('Re_L |'//fu//'| '//SR//LT//Re//' / LT '//er, ra=vpCIO_Re_L)
    res = aMC_output_add('De_U | permil | '//SR//UT//De//' / UT '//ee, ra=vpCIO_De_U)
    res = aMC_output_add('De_L | permil | '//SR//BL//De//' / LT '//ee, ra=vpCIO_De_L)
#endif

  ! STE flux
    res = aMC_output_add('S    |'//fu//'| STE flux',                      ra=vpCIO_S )
#ifndef vpCIO_mode_dPD
    res = aMC_output_add('dSa  |'//fu//'| STE flux deviation from climatic average', ra=vpCIO_dSa )
  ! STE regression analysis
    res = aMC_output_add('regr_k | -/-    | S regression slope', ra=regr(1,:))
    res = aMC_output_add('regr_b |'//fu//'| S regression bias', ra=regr(2,:))
    res = aMC_output_add('regr_r | -1+1   | S regression coefficient of determination', ra=regr(3,:))
#endif

  ! hit cases corr. entries
    do ci = 1, vpCIO_q
      se = vpCIO_exp(ci)%s
      vpCIO_case_exp(ci) = aMC_hit_case_add(se//' | '//se//' experiment/conditions')
    enddo
#ifdef vpCIO_mode_dPD
    vpCIO_set_fitD  = aMC_hit_set_add('delta | fitting D36 difference vs. PD')
#else
  ! generic
    vpCIO_set_fitS  = aMC_hit_set_add('S | fitting STE flux in same climate')
    vpCIO_set_fitD  = aMC_hit_set_add('D | fitting simulated/observed '//D36)
    vpCIO_set_fitB  = aMC_hit_set_add('B | fitting STE flux and simulated/observed '//D36)
!!  do ci = 1, vpCIO_smpQ
!!    res = int(vpCIO_lab(vpCIO_idx_sn,ci)) ! sample no.
!!    vpCIO_case_smp(ci) = aMC_hit_case_add(num2str(i=res,fmt='(I2.2)')//' | sample #'//num2str(i=res)//' at'// &
!!                    ' s(ds)='//num2str(r=vpCIO_lab(vpCIO_idx_s,ci))//'('//num2str(r=vpCIO_lab(vpCIO_idx_ds,ci))//')'// &
!!                  ' Db(dDb)='//num2str(r=vpCIO_lab(vpCIO_idx_Db,ci))//'('//num2str(r=vpCIO_lab(vpCIO_idx_dDb,ci))//')')
!!  enddo
#endif /*  vpCIO_mode_dPD */

#endif /* aMC__parameter_setup__code */


! ----- new initial state ------------------------------------------------------
#ifdef    aMC__new_x0__defs
      integer :: ci
#endif /* aMC__new_x0__defs */
#ifdef    aMC__new_x0__code

    ! preparing work arrays
      vpCIO_dD_BAT(__) = iv_dD_BAT(__)

    ! distributions around simulated averages (input is 2*sigma)
      vpCIO_Re_T(__) = iv_Re_T(__) + ie_Re_T(__)/2._dp * mcj_Re_T(__) * vpCIO_MCJ
      vpCIO_De_T(__) = iv_De_T(__) + ie_De_T(__)/2._dp * mcj_De_T(__) * vpCIO_MCJ
#ifdef vpCIO_probeULT
      vpCIO_Re_U(__) = iv_Re_U(__) + ie_Re_U(__)/2._dp * mcj_Re_U(__) * vpCIO_MCJ
      vpCIO_Re_L(__) = iv_Re_L(__) + ie_Re_L(__)/2._dp * mcj_Re_L(__) * vpCIO_MCJ
      vpCIO_De_U(__) = iv_De_U(__) + ie_De_U(__)/2._dp * mcj_De_U(__) * vpCIO_MCJ
      vpCIO_De_L(__) = iv_De_L(__) + ie_De_L(__)/2._dp * mcj_De_L(__) * vpCIO_MCJ
#endif
      vpCIO_D_T(__)  = iv_D_T(__)  + ie_D_BA(__)/2._dp * mcj_D_BA(__) * vpCIO_MCJ
      vpCIO_D_BA(__) = iv_D_BA(__) + ie_D_BA(__)/2._dp * mcj_D_BA(__) * vpCIO_MCJ

#ifdef vpCIO_mode_dPD
    ! delta-PD mode
    ! assigning OW end-member value derived in non delta-PD mode
      vpCIO_Dr_OW = vpCIO_dPD__Dr_OW
    ! detect the reference PD sim. idx & calculate S
      do ci = 1, vpCIO_q
        if ( vpCIO_ci(ci).eq.vpCIO_ci_rPD ) then
          vpCIO_n_rPD = ci
          vpCIO_S(ci) = vpCIO_Re_T(ci) * ( vpCIO_D_T(ci)-vpCIO_De_T(ci) ) / (vpCIO_Dr_OW-vpCIO_D_T(ci))
        endif
      enddo
    ! adjust the OW signature
      vpCIO_Dr_OW = vpCIO_Dr_OW + vpCIO_dDr_OW
#endif /* vpCIO_mode_dPD */


#endif /* aMC__new_x0__code */

! ----- prepare output ---------------------------------------------------------
#ifdef    aMC__output_prepare__defs
#endif /* aMC__output_prepare__defs */
#ifdef    aMC__output_prepare__code
#endif /* aMC__output_prepare__code */

! ----- aMC_physc --------------------------------------------------------------
#ifdef    aMC__physc__defs
      integer :: si
      real8   :: Sa, adSa
      real8   :: sQ, sX, sXX, sXY, sY, sYY
#endif /* aMC__physc__defs */
#ifdef    aMC__physc__code

      where ( vpCIO_ci(__).gt.0 )  ! --- data from performed simulations ---

      ! calculating STE flux based on Re_T, De_T, Dr_OW & D_T
       !vpCIO_S(__) =    iv_Re_T(__) * (    iv_D_T(__)-   iv_De_T(__) ) / (vpCIO_Dr_OW-   iv_D_T(__))
        vpCIO_S(__) = vpCIO_Re_T(__) * ( vpCIO_D_T(__)-vpCIO_De_T(__) ) / (vpCIO_Dr_OW-vpCIO_D_T(__))

      elsewhere ( vpCIO_ci(__).lt.0 )  ! --- generic climate estimates ---
#ifdef vpCIO_mode_dPD
      ! delta-PD mode

      ! perturbed terms
        vpCIO_S(__) = vpCIO_S(vpCIO_n_rPD) * (1.0_dp + vpCIO_dS)
#ifdef vpCIO_probeULT
        vpCIO_Re_U(__) = vpCIO_Re_U(vpCIO_n_rPD) * (1.0_dp + vpCIO_dRe_U)
        vpCIO_Re_L(__) = vpCIO_Re_L(vpCIO_n_rPD) * (1.0_dp + vpCIO_dRe_L)
        vpCIO_De_U(__) = vpCIO_De_U(vpCIO_n_rPD) + vpCIO_dDe_U
        vpCIO_De_L(__) = vpCIO_De_L(vpCIO_n_rPD) + vpCIO_dDe_L

        vpCIO_Re_T(__) = vpCIO_Re_L(__) + vpCIO_Re_U(__)
        vpCIO_De_T(__) = ( vpCIO_Re_L(__)*vpCIO_De_L(__) + vpCIO_Re_U(__)*vpCIO_De_U(__) ) / vpCIO_Re_T(__)
#else
        vpCIO_Re_T(__) = vpCIO_Re_T(vpCIO_n_rPD) * (1.0_dp + vpCIO_dRe_T)
        vpCIO_De_T(__) = vpCIO_De_T(vpCIO_n_rPD) + vpCIO_dDe_T
#endif /* vpCIO_probeULT */

      ! tropospheric D36
        vpCIO_D_T(__) = ( vpCIO_Re_T(__)*vpCIO_De_T(__) + vpCIO_S(__)*vpCIO_Dr_OW ) / ( vpCIO_Re_T(__) + vpCIO_S(__) )
      ! Arctic BL D36 (see the regression of EMAC data in vpCIO.xls::vpCIO-data_dPD)
        vpCIO_D_BA(__) = 1.14759*vpCIO_D_T(__)-0.38764  ! T100
      ! => delta for comparison (vs. ref. PD)
        vpCIO_D_BA(__) = vpCIO_D_BA(__) - vpCIO_D_BA(vpCIO_n_rPD)

#else /* vpCIO_mode_dPD */
      ! absolute flux & signatures mode

      ! equilibration D36
      ! using LT/R ratio and LT/UT eq. D36 signatures
#ifdef vpCIO_mode_dPD
        vpCIO_De_T(__) = ( 1._dp*iv_De_U(__) + vpCIO_L2R*iv_De_L(__) ) / ( 1._dp + vpCIO_L2R )
#endif

      ! D36 in the troposphere
      ! using S, R
       !vpCIO_D_T(__) = ( vpCIO_Re_T(__)*vpCIO_De_T(__) + vpCIO_F(__)*vpCIO_Dr_OW ) / ( vpCIO_Re_T(__) + vpCIO_S(__) )
      ! using the S/R ratio
        vpCIO_D_T(__) = ( vpCIO_De_T(__) + vpCIO_S2R*vpCIO_Dr_OW ) / ( 1._dp + vpCIO_S2R ) ! reduced to use S2R only
      ! D36 in the BL Arctic
        vpCIO_D_BA(__) = vpCIO_D_T(__) + vpCIO_dD_BAT(__)

#endif /* vpCIO_mode_dPD */
      endwhere


#ifndef vpCIO_mode_dPD

    ! derived STEs analysis
      regr(:,:) = 0.
      do ci = vpCIO_ci_LGM, vpCIO_ci_PD
      ! calculating ave./dev. of S
      ! calculate
        Sa = sum( merge( vpCIO_S(__), 0._dp, vpCIO_ci(__).eq.(ci) ))/sum( merge( 1._dp, 0._dp, vpCIO_ci(__).eq.(ci) ) )
      ! put Sa to generic
        where ( vpCIO_ci(__).eq.(-ci) )
          vpCIO_S(__) = Sa
        endwhere
      ! put dSa of act-gen -> act
        where ( vpCIO_ci(__).eq.(ci) )
          vpCIO_dSa(__) = vpCIO_S(__) - Sa
        endwhere
      ! calc. average abs. dSa
        adSa = sum( merge( dabs( vpCIO_dSa(__) ), 0._dp, vpCIO_ci(__).eq.(ci) ) ) / &
               sum( merge(                 1._dp, 0._dp, vpCIO_ci(__).eq.(ci) ) )
      ! put dSa to generic
        where ( vpCIO_ci(__).eq.(-ci) )
          vpCIO_dSa(__) = adSa
        endwhere
#ifdef DEBUG
        _log_ 'Sa(',ci,')=',Sa,'adSa(',ci,')=',adSa
#endif
      ! calculate F regression across sims. (per climate)
      ! data from act. sims.
        sQ  = sum( merge( 1., 0., vpCIO_ci(__).eq.(ci) ) )  ! qty
      ! sumX, sumX^2
        sX  = sum( merge( vpCIO_Re_T(__),    0., vpCIO_ci(__).eq.(ci) ) )
        sXX = sum( merge( vpCIO_Re_T(__)**2, 0., vpCIO_ci(__).eq.(ci) ) )
      ! sumXY
        sXY = sum( merge( vpCIO_Re_T(__)*vpCIO_S(__), 0., vpCIO_ci(__).eq.(ci) ) )
      ! sumY, sumY^2
        sY  = sum( merge( vpCIO_S(__),    0., vpCIO_ci(__).eq.(ci) ) )
        sYY = sum( merge( vpCIO_S(__)**2, 0., vpCIO_ci(__).eq.(ci) ) )

      ! put results in actual & generic
        where ( abs(vpCIO_ci(__)).eq.(ci) )
          regr(1,__) = (sQ*sXY-sX*sY) / (sQ*sXX-sX**2)                      ! slope
          regr(2,__) = (sY*sXX-sX*sXY) / (sQ*sXX-sX**2)                     ! bias
          regr(3,__) = (sXY-sX*sY/sQ) / sqrt((sXX-sX**2/sQ)*(sYY-sY**2/sQ)) ! corr. coeff.
        endwhere
#ifdef DEBUG
        do si = 1, vpCIO_q
          if ( vpCIO_ci(si).eq.(ci) ) _log_ 'x=',vpCIO_Re_T(si),'y=',vpCIO_S(si)
          if ( vpCIO_ci(si).eq.(-ci) )_log_ 'regr(',ci,')=',regr(1:3,si)
        enddo
#endif
      enddo

#endif /* vpCIO_mode_dPD */


#ifdef DEBUG
    _log_ 'vpCIO_physc('//_pe_//'):'
    _log_ ' Dr_OW:',vpCIO_Dr_OW,' S2R:',vpCIO_S2R
    _log_ '     S:',vpCIO_S(__)
    _log_ '   dSa:',vpCIO_dSa(__)
    _log_ '  De_T:',vpCIO_De_T(__)
    _log_ '   D_T:',vpCIO_D_T(__)
    stop
#endif

    ! shot is done when simulation end is reached or error occured
      aMC_shot_done = .true.
      aMC_shot_real = .true.
#endif /* aMC__physc__code */

! ----- find matching cases ----------------------------------------------------
#ifdef    aMC__match__defs
    integer :: ci
    logical :: allF
#endif /* aMC__match__defs */
    ! note: do not use cycle here
    ! hint: see the no. of hits using match(<case>,<set>)
#ifdef    aMC__match__cases__code
    ! inside the loop over cases: case = aMC_match_case_from, aMC_match_case_upto

#ifdef vpCIO_mode_dPD

    ! hit if observed delta-PD BL ARC D36 is reproduced
      if ( vpCIO_ci(case).lt.0  ) &
        aMC_hit_set(vpCIO_set_fitD)%is_hit = aMC_check_pair( vpCIO_D_BA(case), iv_D_BA(case), ie_D_BA(case) )

#else
      if ( vpCIO_ci(case).gt.0  ) then
        aMC_hit_set(vpCIO_set_fitS)%is_hit = dabs(regr(1,case)) .lt. 0.001_dp
!( dabs(vpCIO_dSa(case)) .lt. 0.01_dp*ie_Re_T(case) ) .and. &
!                                             ( dabs(regr(1,case)) .lt. (0.01_dp*vpCIO_S(case)) )
      else
      ! compare F in sims. from a given climate, hit if all fluxes are regressed well
        aMC_hit_set(vpCIO_set_fitS)%is_hit = .true.
        do ci = 1, vpCIO_q
          if ( vpCIO_ci(ci).eq.(-vpCIO_ci(case)) ) then
            if ( match(ci,vpCIO_set_fitS).lt.1 ) aMC_hit_set(vpCIO_set_fitS)%is_hit = .false.
          endif
        enddo

      ! hit if observed D36 is reproduced
        aMC_hit_set(vpCIO_set_fitD)%is_hit = aMC_check_pair( vpCIO_D_BA(case), iv_D_BA(case), ie_D_BA(case) )
      endif

      aMC_hit_set(vpCIO_set_fitB)%is_hit = aMC_hit_set(vpCIO_set_fitS)%is_hit .and. aMC_hit_set(vpCIO_set_fitD)%is_hit
#endif /* vpCIO_mode_dPD */

    ! probe statistic (all)
      aMC_hit_set(aMC_hit_set_probestat)%is_hit = .true.
#endif /* aMC__match__cases__code */
#ifdef    aMC__match__sets__code
    ! inside the loop over sets: set = aMC_match_set_from, aMC_match_set_upto

#endif /* aMC__match__sets__code */

! ----- aMC_physc:check_req_hits -----
! inside the loop over cases: cn = aMC_match_case_from, aMC_match_case_upto
#ifdef    aMC__physc__check_req_hits__code
        ! stop when hit statistic is complete for all samples
          do sn = aMC_match_set_from, aMC_match_set_upto
#ifdef vpCIO_mode_dPD
          ! stop when all delta-cases for generic climates are found
            if ( ( vpCIO_ci(cn).lt.0 ) .and. &
                 ( aMC_hits(cn,vpCIO_set_fitD,00).lt.aMC_max_hits ) ) then
#else
            if ( aMC_hits(cn,sn,00).lt.aMC_req_hits ) then
#endif
              req_hits_found4all = .false.
              exit ! do-loop
            endif
          enddo
#ifdef DEBUG
          if ( aMC_tries.gt.10 ) req_hits_found4all = .true.
#endif
#endif /* aMC__physc__check_req_hits__code */


! ----- info output ------------------------------------------------------------
#ifdef    aMC__info__code
    _info_ '| - troposphere definition: '//trim(vpCIO_T_def)
#ifdef vpCIO_mode_dPD
    _info_ '| - delta-PD mode: probing perturbation vs. PD conditions'
    _info_ '|   - OW end-member value: '//num2str(r=vpCIO_dPD__Dr_OW,fmt='(F7.2)')//' permil'
#endif
#ifdef vpCIO_probeULT
    _info_ '| - probing upper/lower tropospheric domains: '//trim(vpCIO_U_def)//' & '//trim(vpCIO_L_def)
#endif
#endif /* aMC__info__code */

! --- XXX ---
#ifdef    aMC__XXX__defs
#endif /* aMC__XXX__defs */
#ifdef    aMC__XXX__code
#endif /* aMC__XXX__code */
